package shardmaster

import "net"
import "fmt"
import "net/rpc"
import "log"
import "benchmarks/paxos"
import "sync"
import "os"
import "syscall"
import "encoding/gob"
import "math/rand"
import "time"
import "sort"

const Debug=0

func DPrintf(format string, a ...interface{}) (n int, err error) {
  if Debug > 0 {
    log.Printf(format, a...)
  }
  return
}

type ShardMaster struct {
  mu sync.Mutex
  l net.Listener
  me int
  dead bool // for testing
  unreliable bool // for testing
  px *paxos.Paxos

  configs []Config // indexed by config num
  processed int // processedInstance
  availableGIDs int64slice
  sn int
}


type Op struct {
  // Your data here.
  Cmd string
  GID int64
  Args *Args
  Sn int // for Query cmd
}

type Args struct {
  JoinArgs *JoinArgs
  LeaveArgs *LeaveArgs
  MoveArgs *MoveArgs
  QueryArgs *QueryArgs
}

func (sm *ShardMaster) Join(args *JoinArgs, reply *JoinReply) error {
  DPrintf("%d Join GID %d ", sm.me, args.GID)
  sm.mu.Lock()
  defer sm.mu.Unlock()
  var instance int
  instance = sm.px.Max() + 1
  var op Op
  op = Op{Cmd:"Join", GID: args.GID, Args: &Args{JoinArgs:args}}
  sm.px.Start(instance, op)
  currentOp := sm.WaitForAgreement(instance)
  for !sm.dead && !(currentOp.Cmd == "Join" && currentOp.GID == op.GID){
      instance = sm.px.Max() + 1
      sm.px.Start(instance, op)
      currentOp = sm.WaitForAgreement(instance)
  }
  // agreement reached
  return nil
}
func (sm *ShardMaster) Leave(args *LeaveArgs, reply *LeaveReply) error {
  DPrintf("%d Leavei GID %d", sm.me, args.GID)
  sm.mu.Lock()
  defer sm.mu.Unlock()
  var instance int
  instance = sm.px.Max() + 1
  var op Op
  op = Op{Cmd:"Leave", GID: args.GID, Args: &Args{LeaveArgs:args}}
  sm.px.Start(instance, op)
  currentOp := sm.WaitForAgreement(instance)
  for !sm.dead && !(currentOp.Cmd == "Leave" && currentOp.GID == op.GID){
      instance = sm.px.Max() + 1
      sm.px.Start(instance, op)
      currentOp = sm.WaitForAgreement(instance)
  }
  // agreement reached
  return nil
}

func (sm *ShardMaster) Move(args *MoveArgs, reply *MoveReply) error {
  DPrintf("%d Move GID %d SHARD %d", sm.me, args.GID, args.Shard)
  sm.mu.Lock()
  defer sm.mu.Unlock()
  var instance int
  instance = sm.px.Max() + 1
  var op Op
  op = Op{Cmd:"Move", GID: args.GID, Args: &Args{MoveArgs:args}}
  sm.px.Start(instance, op)
  currentOp := sm.WaitForAgreement(instance)
  for !sm.dead && !(currentOp.Cmd == "Move" && currentOp.GID == op.GID){
      instance = sm.px.Max() + 1
      sm.px.Start(instance, op)
      currentOp = sm.WaitForAgreement(instance)
  }
  // agreement reached
  return nil
}

func (sm *ShardMaster) Query(args *QueryArgs, reply *QueryReply) error {

  //log.Printf("%d Query", sm.me)
  sm.mu.Lock()
  defer sm.mu.Unlock()
  sn := sm.sn + 1
  sm.sn = sn
  var instance int
  instance = sm.px.Max() + 1
  var op Op
  op = Op{Cmd:"Query", Sn: sn, Args: &Args{QueryArgs:args}}
  sm.px.Start(instance, op)
  currentOp := sm.WaitForAgreement(instance)
  for !sm.dead && !(currentOp.Cmd == "Query" && currentOp.Sn == op.Sn){
      instance = sm.px.Max() + 1
      sm.px.Start(instance, op)
      currentOp = sm.WaitForAgreement(instance)
  }
  // agreement reached
  sm.mu.Unlock()
  for !sm.dead && sm.processed < instance {
      time.Sleep(10 * time.Millisecond)
  }
  sm.mu.Lock()
  if args.Num == -1 || args.Num >= len(sm.configs) {
     reply.Config = sm.configs[len(sm.configs)-1]
  } else {
     reply.Config = sm.configs[args.Num]
  }
  return nil
}

// return Op decided on that instance
func (sm *ShardMaster) WaitForAgreement(instance int) *Op {
  to := 10 * time.Millisecond
  for !sm.dead {
    decided, v := sm.px.Status(instance)
    if decided {
       currentOp, ok := v.(Op)
       if !ok {
          log.Printf("Type conversion failed")
          return nil
       }
       return &currentOp
    }
    time.Sleep(to)
    if to < 10 * time.Second {
      to *= 2
    }
  }
  return nil
}

func (sm *ShardMaster) applyJoin(args *JoinArgs) {
  //log.Printf("%d Processing JOIN Job %d", sm.me, job)
  currentConfig := sm.configs[len(sm.configs)-1]
  dup := false // whether GID already exists
  for _, g := range sm.availableGIDs {
    if g == args.GID {
     dup = true
    }
  }
  if dup {
     return
  }

  var shards [NShards]int64
  groups := make (map [int64] []string)
  if len(sm.availableGIDs) > 0 {
    // count each gid owns how many shards
    shardcountmap := make (map[int64] int)
    for _, gid := range currentConfig.Shards {
       _, existed := shardcountmap[gid]
       if !existed {
          shardcountmap[gid] = 1
       } else {
          shardcountmap[gid] += 1
       }
    }
    var maxgid int64
    maxcount := 0
    for _, gid := range sm.availableGIDs {
       shardcount := shardcountmap[gid]
       if shardcount > maxcount {
          maxgid = gid
          maxcount = shardcount
       }
    }
    newcount := maxcount/2
    for i, gid := range currentConfig.Shards {
       if gid == maxgid && newcount > 0 {
          shards[i] = args.GID
          newcount--
       } else {
          shards[i] = currentConfig.Shards[i]
       }
    }

    for mapkey,mapvalue := range currentConfig.Groups {
       groups[mapkey] = mapvalue
    }
  } else {
    for i, _ := range currentConfig.Shards {
        shards[i] = args.GID
    }
  }
  groups[args.GID] = args.Servers
  sm.availableGIDs = append(sm.availableGIDs,args.GID)
  sort.Sort(sm.availableGIDs)
  sm.configs = append(sm.configs, Config{len(sm.configs), shards, groups})

  DPrintf("%d Join new shards %s", sm.me, shards)
}

func (sm *ShardMaster) applyLeave(args *LeaveArgs) {
  // log.Printf("%d Processing LEAVE Job %d GID %d", sm.me, job, args.GID)
  currentConfig := sm.configs[len(sm.configs)-1]
  var newavailableGIDs int64slice // need to confirm
  existed := false // whether GID already exists
  for _, g := range sm.availableGIDs {
     if g == args.GID {
       existed = true
     } else {
       newavailableGIDs = append(newavailableGIDs,g)
     }
  }
  // newavailableGIDs should be sorted at this point
  if !existed {
     return
  }

  sm.availableGIDs = newavailableGIDs
  // log.Printf("LEAVE availableGIDs %s",sm.availableGIDs)
  groups := make (map [int64] []string)
  var shards [NShards]int64
  // log.Printf("Leave shards %s", shards)
  // count how many shards each gid owns
  shardcountmap := make (map[int64] int)
  for _, gid := range currentConfig.Shards {
     _, existed := shardcountmap[gid]
     if !existed {
        shardcountmap[gid] = 1
     } else {
        shardcountmap[gid] += 1
     }
  }
  var leastgid int64
  leastcount := NShards
  for _, gid := range sm.availableGIDs {
     if gid != args.GID {
       shardcount := shardcountmap[gid]
       if shardcount < leastcount {
          leastgid = gid
          leastcount = shardcount
       }
     }
  }
  DPrintf("%d LEAVE leastgid %d leastcount %d", sm.me, leastgid, leastcount)
  for i, gid := range currentConfig.Shards {
     if gid == args.GID {
        shards[i] = leastgid
     } else {
        shards[i] = currentConfig.Shards[i]
     }
  }

  for mapkey,mapvalue := range currentConfig.Groups {
     if mapkey != args.GID {
        groups[mapkey] = mapvalue
     }
  }

  sm.configs = append(sm.configs, Config{len(sm.configs), shards, groups})
  DPrintf("%d Leave new shards %s", sm.me, shards)
}

type int64slice []int64
func (a int64slice) Len() int { return len(a) }
func (a int64slice) Swap(i, j int) { a[i], a[j] = a[j], a[i] }
func (a int64slice) Less(i, j int) bool { return a[i] < a[j] }

func (sm *ShardMaster) StartBackground() {
  for !sm.dead {
    job := 1 + sm.processed
    to := 10 * time.Millisecond
    for !sm.dead {
      decided, v := sm.px.Status(job)
      if decided {
        //log.Printf("%d Processing Job %d", sm.me, job)
         op, ok := v.(Op)
         if !ok {
            log.Printf("Type conversion failed")
            break
         }
         sm.mu.Lock()
         if op.Cmd == "Join" {
            args := op.Args.JoinArgs
            sm.applyJoin(args)
         } else if op.Cmd == "Leave" {
            args := op.Args.LeaveArgs
            sm.applyLeave(args)
         } else if op.Cmd == "Move" {
            //log.Printf("%d Processing MOVE Job %d", sm.me, job)
            currentConfig := sm.configs[len(sm.configs)-1]
            args := op.Args.MoveArgs
            var shards [NShards]int64
            for i:=0; i < NShards; i++ {
                if i == args.Shard {
                  shards[i] = args.GID
                } else {
                  shards[i] = currentConfig.Shards[i]
                }
            }
            groups := make(map[int64][]string)
            for mapkey,mapvalue := range currentConfig.Groups {
                groups[mapkey] = mapvalue
            }
            sm.configs = append(sm.configs, Config{len(sm.configs), shards, groups})
         } else if op.Cmd == "Query" {
           // do anything??
           // args := op.Args.QueryArgs
            //log.Printf("%d Processing QUERY Job %d ConfigNum %d", sm.me, job, args.Num)
         }
         sm.processed = job
         sm.px.Done(job)
         sm.mu.Unlock()
         //log.Printf("%d background releases lock", sm.me)
         break
      } else {
        // try to propose nop
        if sm.px.Max() > job {
           sm.px.Start(job, Op{})
        }
      }
      time.Sleep(to)
      if to < 10 * time.Second {
        to *= 2
      }
     }
  }
}


// please don't change this function.
func (sm *ShardMaster) Kill() {
  sm.dead = true
  sm.l.Close()
  sm.px.Kill()
}

//
// servers[] contains the ports of the set of
// servers that will cooperate via Paxos to
// form the fault-tolerant shardmaster service.
// me is the index of the current server in servers[].
// 
func StartServer(servers []string, me int) *ShardMaster {
  gob.Register(Op{})

  sm := new(ShardMaster)
  sm.me = me

  sm.configs = make([]Config, 1)
  sm.configs[0].Groups = map[int64][]string{}
  sm.processed = -1
  sm.availableGIDs = int64slice{}
  sm.sn = -1 // used for query
  rpcs := rpc.NewServer()
  rpcs.Register(sm)

  sm.px = paxos.Make(servers, me, rpcs)
  go sm.StartBackground()
  os.Remove(servers[me])
  l, e := net.Listen("unix", servers[me]);
  if e != nil {
    log.Fatal("listen error: ", e);
  }
  sm.l = l

  // please do not change any of the following code,
  // or do anything to subvert it.

  go func() {
    for sm.dead == false {
      conn, err := sm.l.Accept()
      if err == nil && sm.dead == false {
        if sm.unreliable && (rand.Int63() % 1000) < 100 {
          // discard the request.
          conn.Close()
        } else if sm.unreliable && (rand.Int63() % 1000) < 200 {
          // process the request but force discard of reply.
          c1 := conn.(*net.UnixConn)
          f, _ := c1.File()
          err := syscall.Shutdown(int(f.Fd()), syscall.SHUT_WR)
          if err != nil {
            fmt.Printf("shutdown: %v\n", err)
          }
          go rpcs.ServeConn(conn)
        } else {
          go rpcs.ServeConn(conn)
        }
      } else if err == nil {
        conn.Close()
      }
      if err != nil && sm.dead == false {
        fmt.Printf("ShardMaster(%v) accept: %v\n", me, err.Error())
        sm.Kill()
      }
    }
  }()

  return sm
}
