package benchmarks

import "testing"
import "runtime"
import "strconv"
import "os"
import "time"
import "fmt"
import "math/rand"
import "math"
import "common"
import mshardkv "shardkv"
import mshardmaster "shardmaster"
import pshardkv "benchmarks/shardkv"
import pshardmaster "benchmarks/shardmaster"

const db_path = "./db/"
const ngroups = 3 // replica groups
const nreplicas = 3 // servers per group
const inst_max = 1024 // max # of instances

//
// Run with "go test -bench=."
//

func BenchmarkShardKVPaxosRPCs(b *testing.B) {
  p_smh, p_gids, p_ha, p_sa, p_clean := p_setup("bench-skv-paxos")
  defer p_clean()

  p_mck := pshardmaster.MakeClerk(p_smh)
  for i := 0; i < len(p_gids); i++ {
    p_mck.Join(p_gids[i], p_ha[i])
  }
  p_ck := pshardkv.MakeClerk(p_smh)

  rand.Seed(time.Now().Unix())
  rand_group := rand.Intn(ngroups)

  p_cfg := p_mck.Query(-1)
  p_shrds := make([]int64, 10)
  p_count := 0
  for i, g := range p_cfg.Shards {
    if g == p_gids[rand_group] {
      p_shrds[p_count] = int64(i)
      p_count++
    }
  }
  p_shrds = p_shrds[:p_count]

  p_inst := 1
  for {
    for j := 0; j < p_inst; j++ {
      arr := []byte(strconv.Itoa(rand.Intn(math.MaxInt32)))
      arr[0] = byte(p_shrds[0] + 100)
      key := string(arr[:len(arr)])
      val := string(arr[:])
      p_ck.Put(key, val)
    }
    p_total := 0
    for j := 0; j < nreplicas; j++ {
      p_total += p_sa[rand_group][j].GetRPCCount()
    }
    fmt.Printf("PAXOS (ShardKV): total RPCs for %+v instances: %+v\n",p_inst,p_total)
    for j := 0; j < nreplicas; j++ {
      p_sa[rand_group][j].ResetRPCCount()
    }
    p_inst *= 2
    if p_inst > inst_max {
      break
    }
  }
}

func BenchmarkShardKVPaxosFailureRPCs(b *testing.B) {
  p_smh, p_gids, p_ha, p_sa, p_clean := p_setup("bench-skv-paxos")
  defer p_clean()

  p_mck := pshardmaster.MakeClerk(p_smh)
  for i := 0; i < len(p_gids); i++ {
    p_mck.Join(p_gids[i], p_ha[i])
  }
  p_ck := pshardkv.MakeClerk(p_smh)

  rand.Seed(time.Now().Unix())
  rand_group := rand.Intn(ngroups)

  p_cfg := p_mck.Query(-1)
  p_shrds := make([]int64, 10)
  p_count := 0
  for i, g := range p_cfg.Shards {
    if g == p_gids[rand_group] {
      p_shrds[p_count] = int64(i)
      p_count++
    }
  }
  p_shrds = p_shrds[:p_count]

  rand_server := rand.Intn(nreplicas)
  fmt.Printf("kill replica %+v\n",rand_server)
  p_sa[rand_group][rand_server].BenchKill()

  p_inst := 1
  for {
    for j := 0; j < p_inst; j++ {
      arr := []byte(strconv.Itoa(rand.Intn(math.MaxInt32)))
      arr[0] = byte(p_shrds[0] + 100)
      key := string(arr[:len(arr)])
      val := string(arr[:])
      p_ck.Put(key, val)
    }
    p_total := 0
    for j := 0; j < nreplicas; j++ {
      p_total += p_sa[rand_group][j].GetRPCCount()
    }
    fmt.Printf("PAXOS (ShardKV) (Failure): total RPCs for %+v instances: %+v\n",p_inst,p_total)
    for j := 0; j < nreplicas; j++ {
      p_sa[rand_group][j].ResetRPCCount()
    }
    p_inst *= 2
    if p_inst > inst_max {
      break
    }
  }
}

func BenchmarkShardKVMenciusRPCs(b *testing.B) {
  m_smh, m_gids, m_ha, m_sa, m_clean, _ := m_setup("bench-skv-mencius")
  defer m_clean()

  m_mck := mshardmaster.MakeClerk(m_smh)
  for i := 0; i < len(m_gids); i++ {
    m_mck.Join(m_gids[i], m_ha[i])
  }
  m_ck := mshardkv.MakeClerk(m_smh)

  rand.Seed(time.Now().Unix())
  rand_group := rand.Intn(ngroups)

  m_cfg := m_mck.Query(-1)
  m_shrds := make([]int64, 10)
  m_count := 0
  for i, g := range m_cfg.Shards {
    if g == m_gids[rand_group] {
      m_shrds[m_count] = int64(i)
      m_count++
    }
  }
  m_shrds = m_shrds[:m_count]

  m_inst := 1
  for {
    for j := 0; j < m_inst; j++ {
      arr := []byte(strconv.Itoa(rand.Intn(math.MaxInt32)))
      arr[0] = byte(m_shrds[0] + 100)
      key := string(arr[:len(arr)])
      val := string(arr[:])
      m_ck.Put(key, val)
    }
    m_total := 0
    for j := 0; j < nreplicas; j++ {
      //fmt.Printf("replica %+v rpccount %+v\n",j,m_sa[rand_group][j].GetRPCCount())
      m_total += m_sa[rand_group][j].GetRPCCount()
    }
    fmt.Printf("MENCIUS (ShardKV): total RPCs for %+v instances: %+v\n",m_inst,m_total)
    for j := 0; j < nreplicas; j++ {
      m_sa[rand_group][j].ResetRPCCount()
    }
    m_inst *= 2
    if m_inst > inst_max {
      break
    }
  }
}

func BenchmarkShardKVMenciusFailureRPCs(b *testing.B) {
  m_smh, m_gids, m_ha, m_sa, m_clean, _ := m_setup("bench-skv-mencius-failure")
  defer m_clean()

  m_mck := mshardmaster.MakeClerk(m_smh)
  for i := 0; i < len(m_gids); i++ {
    m_mck.Join(m_gids[i], m_ha[i])
  }
  m_ck := mshardkv.MakeClerk(m_smh)

  rand.Seed(time.Now().Unix())
  rand_group := rand.Intn(ngroups)

  m_cfg := m_mck.Query(-1)
  m_shrds := make([]int64, 10)
  m_count := 0
  for i, g := range m_cfg.Shards {
    if g == m_gids[rand_group] {
      m_shrds[m_count] = int64(i)
      m_count++
    }
  }
  m_shrds = m_shrds[:m_count]

  rand_server := 0 // for bench. reliability
  fmt.Printf("kill replica %+v\n",rand_server)
  m_sa[rand_group][rand_server].BenchKill()

  m_inst := 1
  for {
    for j := 0; j < m_inst; j++ {
      arr := []byte(strconv.Itoa(rand.Intn(math.MaxInt32)))
      arr[0] = byte(m_shrds[0] + 100)
      key := string(arr[:len(arr)])
      val := string(arr[:])
      m_ck.Put(key, val)
    }
    m_total := 0
    for j := 0; j < nreplicas; j++ {
      //fmt.Printf("replica %+v rpccount %+v\n",j,m_sa[rand_group][j].GetRPCCount())
      m_total += m_sa[rand_group][j].GetRPCCount()
    }
    fmt.Printf("MENCIUS (ShardKV) (Failure): total RPCs for %+v instances: %+v\n",m_inst,m_total)
    for j := 0; j < nreplicas; j++ {
      m_sa[rand_group][j].ResetRPCCount()
    }
    m_inst *= 2
    if m_inst > inst_max {
      break
    }
  }
}


func m_setup(tag string) ([]string, []int64, [][]string, [][]*mshardkv.ShardKV, func(), [][]int) {
  runtime.GOMAXPROCS(4)
  os.Mkdir("db", 0777)
  const nmasters = 3
  var sma []*mshardmaster.ShardMaster = make([]*mshardmaster.ShardMaster, nmasters)
  var smh []string = make([]string, nmasters)
  // defer mcleanup(sma)
  for i := 0; i < nmasters; i++ {
    smh[i] = m_port(tag+"m", i)
  }
  for i := 0; i < nmasters; i++ {
    sma[i] = mshardmaster.StartServer(smh, i)
  }

  gids := make([]int64, ngroups)    // each group ID
  ha := make([][]string, ngroups)   // ShardKV ports, [group][replica]
  sa := make([][]*mshardkv.ShardKV, ngroups) // ShardKVs
  uids := make([][]int, ngroups) // uids
  // defer cleanup(sa)
  for i := 0; i < ngroups; i++ {
    gids[i] = int64(i + 100)
    sa[i] = make([]*mshardkv.ShardKV, nreplicas)
    ha[i] = make([]string, nreplicas)
    uids[i] = make([]int, nreplicas)
    for j := 0; j < nreplicas; j++ {
      uids[i][j] = int(common.Nrand())
      ha[i][j] = m_port(tag+"s", (i*nreplicas)+j)
    }
    for j := 0; j < nreplicas; j++ {
      sa[i][j] = mshardkv.StartServer(gids[i], smh, ha[i], uids[i][j], j, false /**normal start **/)
    }
  }

  clean := func() { m_cleanup(sa) ; m_mcleanup(sma) }
  return smh, gids, ha, sa, clean, uids
}

func m_port(tag string, host int) string {
  s := "/var/tmp/824-"
  s += strconv.Itoa(os.Getuid()) + "/"
  os.Mkdir(s, 0777)
  s += "skv-"
  s += strconv.Itoa(os.Getpid()) + "-"
  s += tag + "-"
  s += strconv.Itoa(host)
  return s
}

func m_mcleanup(sma []*mshardmaster.ShardMaster) {
  for i := 0; i < len(sma); i++ {
    if sma[i] != nil {
      sma[i].Kill()
    }
  }
}

func m_cleanup(sa [][]*mshardkv.ShardKV) {

  for i := 0; i < len(sa); i++ {
    for j := 0; j < len(sa[i]); j++ {
      sa[i][j].BenchKill()
    }
  }
  os.RemoveAll(fmt.Sprintf(db_path))
}

func p_setup(tag string) ([]string, []int64, [][]string, [][]*pshardkv.ShardKV, func()) {
  runtime.GOMAXPROCS(4)
  
  const nmasters = 3
  var sma []*pshardmaster.ShardMaster = make([]*pshardmaster.ShardMaster, nmasters)
  var smh []string = make([]string, nmasters)
  // defer mcleanup(sma)
  for i := 0; i < nmasters; i++ {
    smh[i] = port(tag+"m", i)
  }
  for i := 0; i < nmasters; i++ {
    sma[i] = pshardmaster.StartServer(smh, i)
  }

  const ngroups = 3   // replica groups
  const nreplicas = 3 // servers per group
  gids := make([]int64, ngroups)    // each group ID
  ha := make([][]string, ngroups)   // ShardKV ports, [group][replica]
  sa := make([][]*pshardkv.ShardKV, ngroups) // ShardKVs
  // defer cleanup(sa)
  for i := 0; i < ngroups; i++ {
    gids[i] = int64(i + 100)
    sa[i] = make([]*pshardkv.ShardKV, nreplicas)
    ha[i] = make([]string, nreplicas)
    for j := 0; j < nreplicas; j++ {
      ha[i][j] = port(tag+"s", (i*nreplicas)+j)
    }
    for j := 0; j < nreplicas; j++ {
      sa[i][j] = pshardkv.StartServer(gids[i], smh, ha[i], j)
    }
  }

  clean := func() { p_cleanup(sa) ; p_mcleanup(sma) }
  return smh, gids, ha, sa, clean
}

func p_mcleanup(sma []*pshardmaster.ShardMaster) {
  for i := 0; i < len(sma); i++ {
    if sma[i] != nil {
      sma[i].Kill()
    }
  }
}

func p_cleanup(sa [][]*pshardkv.ShardKV) {
  for i := 0; i < len(sa); i++ {
    for j := 0; j < len(sa[i]); j++ {
      sa[i][j].BenchKill()
    }
  }
}

func key2shard(key string) int {
  shard := 0
  if len(key) > 0 {
    shard = int(key[0])
  }
  shard %= common.NShards
  return shard
}