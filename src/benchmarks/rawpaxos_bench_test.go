package benchmarks

import "testing"
import "runtime"
import "strconv"
import "os"
import "time"
import "fmt"
import "math/rand"
import "common"
import "mencius"
import "benchmarks/paxos"

//
// Run with "go test -bench=."
//

var curr_group_size int = 0
var needPersistence bool = false
var match_value_flag bool = false
func Owner(instance int) (int) {
  return instance % curr_group_size
}

func BenchmarkRPCs(b *testing.B) {
  runtime.GOMAXPROCS(4)
  rand.Seed(time.Now().Unix())

  curr_group_size = 3
  const npaxos = 3

  // Mencius
  var r_m_pxa []*mencius.Paxos = make([]*mencius.Paxos, npaxos)
  var r_m_pxh []string = make([]string, npaxos)
  var r_m_uids[]int = make([]int, npaxos)
  defer r_m_cleanup(r_m_pxa)
  for i := 0; i < npaxos; i++ {
    r_m_pxh[i] = port("mcount", i)
  }
  for i := 0; i < npaxos; i++ {
    r_m_uids[i] = int(common.Nrand())
    r_m_pxa[i] = mencius.Make(common.StringPaxos, r_m_pxh, i, nil, r_m_uids[i], needPersistence, false)
  }

  // Paxos
  var r_p_pxa []*paxos.Paxos = make([]*paxos.Paxos, npaxos)
  var r_p_pxh []string = make([]string, npaxos)
  defer r_p_cleanup(r_p_pxa)
  for i := 0; i < npaxos; i++ {
    r_p_pxh[i] = port("pcount", i)
  }
  for i := 0; i < npaxos; i++ {
    r_p_pxa[i] = paxos.Make(r_p_pxh, i, nil)
  }

  r_m_seq := 0
  r_m_inst := 1
  for {
    for i := 0; i < r_m_inst; i++ {
      r_m_pxa[Owner(r_m_seq)].Start(r_m_seq, "x")
      r_m_waitn(b, r_m_pxa, r_m_seq, npaxos)
      r_m_seq++
    }
    r_m_total := 0
    for j := 0; j < npaxos; j++ {
      r_m_total += r_m_pxa[j].GetRPCCount()
    }
    fmt.Printf("MENCIUS: total RPCs for %+v instances: %+v\n",r_m_inst,r_m_total)
    for j := 0; j < npaxos; j++ {
      r_m_pxa[j].ResetRPCCount()
    }
    r_m_inst *= 2
    if r_m_inst > 1024 {
      break
    }
  }
  
  r_p_seq := 0
  r_p_inst := 1
  for {
    for i := 0; i < r_p_inst; i++ {
      r_p_pxa[rand.Intn(npaxos)].Start(r_p_seq, "x")
      r_p_waitn(b, r_p_pxa, r_p_seq, npaxos)
      r_p_seq++
    }
    r_p_total := 0
    for j := 0; j < npaxos; j++ {
      r_p_total += r_p_pxa[j].GetRPCCount()
    }
    fmt.Printf("PAXOS: total RPCs for %+v instances: %+v\n",r_p_inst,r_p_total)
    for j := 0; j < npaxos; j++ {
      r_p_pxa[j].ResetRPCCount()
    }
    r_p_inst *= 2
    if r_p_inst > 1024 {
      break
    }
  }
}

func BenchmarkTestBasicMencius(b *testing.B) {
  for i := 0; i < b.N; i++ {
    runtime.GOMAXPROCS(4)

    curr_group_size = 3
    const npaxos = 3
    var r_m_pxa []*mencius.Paxos = make([]*mencius.Paxos, npaxos)
    var r_m_pxh []string = make([]string, npaxos)
    var r_m_uids[]int = make([]int, npaxos)
    defer r_m_cleanup(r_m_pxa)

    for i := 0; i < npaxos; i++ {
      r_m_pxh[i] = port("mbasic", i)
    }
    for i := 0; i < npaxos; i++ {
      r_m_uids[i] = int(common.Nrand())
      r_m_pxa[i] = mencius.Make(common.StringPaxos, r_m_pxh, i, nil, r_m_uids[i], needPersistence, false)
    }

    fmt.Printf("Test: Single proposer ...\n")

    r_m_pxa[Owner(0)].Start(0, "hello")
    r_m_waitn(b, r_m_pxa, 0, npaxos)

    fmt.Printf("  ... Passed\n")

    fmt.Printf("Test: Many proposers, same value ...\n")

    for i := 0; i < npaxos; i++ {
      r_m_pxa[Owner(1)].Start(1, 77)
    }
    r_m_waitn(b, r_m_pxa, 1, npaxos)

    fmt.Printf("  ... Passed\n")

    fmt.Printf("Test: Many proposers, different values ...\n")

    r_m_pxa[Owner(2)].Start(2, 100)
    r_m_pxa[Owner(2)].Start(2, 101)
    r_m_pxa[Owner(2)].Start(2, 102)
    r_m_waitn(b, r_m_pxa, 2, npaxos)

    fmt.Printf("  ... Passed\n")

    fmt.Printf("Test: Out-of-order instances ...\n")

    r_m_pxa[Owner(7)].Start(7, 700)
    r_m_pxa[Owner(6)].Start(6, 600)
    r_m_pxa[Owner(5)].Start(5, 500)
    r_m_waitn(b, r_m_pxa, 7, npaxos)
    r_m_pxa[Owner(4)].Start(4, 400)
    r_m_pxa[Owner(3)].Start(3, 300)
    r_m_waitn(b, r_m_pxa, 6, npaxos)
    r_m_waitn(b, r_m_pxa, 5, npaxos)
    r_m_waitn(b, r_m_pxa, 4, npaxos)
    r_m_waitn(b, r_m_pxa, 3, npaxos)

    if r_m_pxa[0].Max() != 7 {
      b.Fatalf("wrong Max()")
    }

    fmt.Printf("  ... Passed\n")
  }
}

func BenchmarkTestBasicPaxos(b *testing.B) {
  for i := 0; i < b.N; i++ {
    runtime.GOMAXPROCS(4)

    const npaxos = 3
    var r_p_pxa []*paxos.Paxos = make([]*paxos.Paxos, npaxos)
    var r_p_pxh []string = make([]string, npaxos)
    defer r_p_cleanup(r_p_pxa)

    for i := 0; i < npaxos; i++ {
      r_p_pxh[i] = port("basic", i)
    }
    for i := 0; i < npaxos; i++ {
      r_p_pxa[i] = paxos.Make(r_p_pxh, i, nil)
    }

    fmt.Printf("Test: Single proposer ...\n")

    r_p_pxa[0].Start(0, "hello")
    r_p_waitn(b, r_p_pxa, 0, npaxos)

    fmt.Printf("  ... Passed\n")

    fmt.Printf("Test: Many proposers, same value ...\n")

    for i := 0; i < npaxos; i++ {
      r_p_pxa[i].Start(1, 77)
    }
    r_p_waitn(b, r_p_pxa, 1, npaxos)

    fmt.Printf("  ... Passed\n")

    fmt.Printf("Test: Many proposers, different values ...\n")

    r_p_pxa[0].Start(2, 100)
    r_p_pxa[1].Start(2, 101)
    r_p_pxa[2].Start(2, 102)
    r_p_waitn(b, r_p_pxa, 2, npaxos)

    fmt.Printf("  ... Passed\n")

    fmt.Printf("Test: Out-of-order instances ...\n")

    r_p_pxa[0].Start(7, 700)
    r_p_pxa[0].Start(6, 600)
    r_p_pxa[1].Start(5, 500)
    r_p_waitn(b, r_p_pxa, 7, npaxos)
    r_p_pxa[0].Start(4, 400)
    r_p_pxa[1].Start(3, 300)
    r_p_waitn(b, r_p_pxa, 6, npaxos)
    r_p_waitn(b, r_p_pxa, 5, npaxos)
    r_p_waitn(b, r_p_pxa, 4, npaxos)
    r_p_waitn(b, r_p_pxa, 3, npaxos)

    if r_p_pxa[0].Max() != 7 {
      b.Fatalf("wrong Max()")
    }

    fmt.Printf("  ... Passed\n")
  }
}

func r_m_waitn(b *testing.B, r_m_pxa[]*mencius.Paxos, seq int, wanted int) {
  to := 10 * time.Millisecond
  for iters := 0; iters < 30; iters++ {
    if r_m_ndecided(b, r_m_pxa, seq) >= wanted {
      break
    }
    time.Sleep(to)
    if to < time.Second {
      to *= 2
    }
  }
  nd := r_m_ndecided(b, r_m_pxa, seq)
  if nd < wanted {
    b.Fatalf("too few decided; seq=%v ndecided=%v wanted=%v", seq, nd, wanted)
  }
}

func r_p_waitn(b *testing.B, r_p_pxa[]*paxos.Paxos, seq int, wanted int) {
  to := 10 * time.Millisecond
  for iters := 0; iters < 30; iters++ {
    if r_p_ndecided(b, r_p_pxa, seq) >= wanted {
      break
    }
    time.Sleep(to)
    if to < time.Second {
      to *= 2
    }
  }
  nd := r_p_ndecided(b, r_p_pxa, seq)
  if nd < wanted {
    b.Fatalf("too few decided; seq=%v ndecided=%v wanted=%v", seq, nd, wanted)
  }
}

func r_m_cleanup(r_m_pxa []*mencius.Paxos) {
  for i := 0; i < len(r_m_pxa); i++ {
    if r_m_pxa[i] != nil {
      r_m_pxa[i].Kill()
    }
  }
}

func r_p_cleanup(r_p_pxa []*paxos.Paxos) {
  for i := 0; i < len(r_p_pxa); i++ {
    if r_p_pxa[i] != nil {
      r_p_pxa[i].Kill()
    }
  }
}

func port(tag string, host int) string {
  s := "/var/tmp/824-"
  s += strconv.Itoa(os.Getuid()) + "/"
  os.Mkdir(s, 0777)
  s += "px-"
  s += strconv.Itoa(os.Getpid()) + "-"
  s += tag + "-"
  s += strconv.Itoa(host)
  return s
}

func r_m_ndecided(b *testing.B, r_m_pxa []*mencius.Paxos, seq int) int {
  count := 0
  var v interface{}
  for i := 0; i < len(r_m_pxa); i++ {
    if r_m_pxa[i] != nil {
      decided, v1 := r_m_pxa[i].Status(seq)
      if decided {
        if count > 0 && v != v1 {
          if match_value_flag && v != v1 {
            // ignore, we're expecting different valuse
          } else {
          b.Fatalf("decided values do not match; seq=%v i=%v v=%v v1=%v",
            seq, i, v, v1)
          }
        }
        count++
        v = v1
      }
    }
  }
  return count
}

func r_p_ndecided(b *testing.B, r_p_pxa []*paxos.Paxos, seq int) int {
  count := 0
  var v interface{}
  for i := 0; i < len(r_p_pxa); i++ {
    if r_p_pxa[i] != nil {
      decided, v1 := r_p_pxa[i].Status(seq)
      if decided {
        if count > 0 && v != v1 {
          if match_value_flag && v != v1 {
            // ignore, we're expecting different valuse
          } else {
          b.Fatalf("decided values do not match; seq=%v i=%v v=%v v1=%v",
            seq, i, v, v1)
          }
        }
        count++
        v = v1
      }
    }
  }
  return count
}

//
// Lab3A Paxos Benchmarks
//

func BenchmarkTestForgetPaxos(b *testing.B) {
  for i := 0; i < b.N; i++ {
    runtime.GOMAXPROCS(4)

    const npaxos = 6
    var pxa []*paxos.Paxos = make([]*paxos.Paxos, npaxos)
    var pxh []string = make([]string, npaxos)
    defer r_p_cleanup(pxa)
    
    for i := 0; i < npaxos; i++ {
      pxh[i] = port("gc", i)
    }
    for i := 0; i < npaxos; i++ {
      pxa[i] = paxos.Make(pxh, i, nil)
    }

    fmt.Printf("Test: Forgetting ...\n")

    // initial Min() correct?
    for i := 0; i < npaxos; i++ {
      m := pxa[i].Min()
      if m > 0 {
        b.Fatalf("wrong initial Min() %v", m)
      }
    }

    pxa[0].Start(0, "00")
    pxa[1].Start(1, "11")
    pxa[2].Start(2, "22")
    pxa[0].Start(6, "66")
    pxa[1].Start(7, "77")

    r_p_waitn(b, pxa, 0, npaxos)

    // Min() correct?
    for i := 0; i < npaxos; i++ {
      m := pxa[i].Min()
      if m != 0 {
        b.Fatalf("wrong Min() %v; expected 0", m)
      }
    }

    r_p_waitn(b, pxa, 1, npaxos)

    // Min() correct?
    for i := 0; i < npaxos; i++ {
      m := pxa[i].Min()
      if m != 0 {
        b.Fatalf("wrong Min() %v; expected 0", m)
      }
    }

    // everyone Done() -> Min() changes?
    for i := 0; i < npaxos; i++ {
      pxa[i].Done(0)
    }
    for i := 1; i < npaxos; i++ {
      pxa[i].Done(1)
    }
    for i := 0; i < npaxos; i++ {
      pxa[i].Start(8 + i, "xx")
    }
    allok := false
    for iters := 0; iters < 12; iters++ {
      allok = true
      for i := 0; i < npaxos; i++ {
        s := pxa[i].Min()
        if s != 1 {
          allok = false
        }
      }
      if allok {
        break
      }
      time.Sleep(1 * time.Second)
    }
    if allok != true {
      b.Fatalf("Min() did not advance after Done()")
    }

    fmt.Printf("  ... Passed\n")
  }
}

func BenchmarkTestManyForgetPaxos(b *testing.B) {
  for i := 0; i < b.N; i++ {
    runtime.GOMAXPROCS(4)

    const npaxos = 3
    var pxa []*paxos.Paxos = make([]*paxos.Paxos, npaxos)
    var pxh []string = make([]string, npaxos)
    defer r_p_cleanup(pxa)
    
    for i := 0; i < npaxos; i++ {
      pxh[i] = port("manygc", i)
    }
    for i := 0; i < npaxos; i++ {
      pxa[i] = paxos.Make(pxh, i, nil)
      pxa[i].SetUnreliable(true)
    }

    fmt.Printf("Test: Lots of forgetting ...\n")

    const maxseq = 20
    done := false

    go func() {
      na := rand.Perm(maxseq)
      for i := 0; i < len(na); i++ {
        seq := na[i]
        j := (rand.Int() % npaxos)
        v := rand.Int() 
        pxa[j].Start(seq, v)
        runtime.Gosched()
      }
    }()

    go func() {
      for done == false {
        seq := (rand.Int() % maxseq)
        i := (rand.Int() % npaxos)
        if seq >= pxa[i].Min() {
          decided, _ := pxa[i].Status(seq)
          if decided {
            pxa[i].Done(seq)
          }
        }
        runtime.Gosched()
      }
    }()

    time.Sleep(5 * time.Second)
    done = true
    for i := 0; i < npaxos; i++ {
      pxa[i].SetUnreliable(false)
    }
    time.Sleep(2 * time.Second)

    for seq := 0; seq < maxseq; seq++ {
      for i := 0; i < npaxos; i++ {
        if seq >= pxa[i].Min() {
          pxa[i].Status(seq)
        }
      }
    }

    fmt.Printf("  ... Passed\n")
  }
}

//
// does paxos forgetting actually free the memory?
//
func BenchmarkTestForgetMemPaxos(b *testing.B) {
  for i := 0; i < b.N; i++ {
    runtime.GOMAXPROCS(4)

    fmt.Printf("Test: Paxos frees forgotten instance memory ...\n")

    const npaxos = 3
    var pxa []*paxos.Paxos = make([]*paxos.Paxos, npaxos)
    var pxh []string = make([]string, npaxos)
    defer r_p_cleanup(pxa)
    
    for i := 0; i < npaxos; i++ {
      pxh[i] = port("gcmem", i)
    }
    for i := 0; i < npaxos; i++ {
      pxa[i] = paxos.Make(pxh, i, nil)
    }

    pxa[0].Start(0, "x")
    r_p_waitn(b, pxa, 0, npaxos)

    runtime.GC()
    var m0 runtime.MemStats
    runtime.ReadMemStats(&m0)
    // m0.Alloc about a megabyte

    for i := 1; i <= 10; i++ {
      big := make([]byte, 1000000)
      for j := 0; j < len(big); j++ {
        big[j] = byte('a' + rand.Int() % 26)
      }
      pxa[0].Start(i, string(big))
      r_p_waitn(b, pxa, i, npaxos)
    }

    runtime.GC()
    var m1 runtime.MemStats
    runtime.ReadMemStats(&m1)
    // m1.Alloc about 90 megabytes

    for i := 0; i < npaxos; i++ {
      pxa[i].Done(10)
    }
    for i := 0; i < npaxos; i++ {
      pxa[i].Start(11 + i, "z")
    }
    time.Sleep(3 * time.Second)
    for i := 0; i < npaxos; i++ {
      if pxa[i].Min() != 11 {
        b.Fatalf("expected Min() %v, got %v\n", 11, pxa[i].Min())
      }
    }

    runtime.GC()
    var m2 runtime.MemStats
    runtime.ReadMemStats(&m2)
    // m2.Alloc about 10 megabytes

    if m2.Alloc > (m1.Alloc / 2) {
      b.Fatalf("memory use did not shrink enough")
    }

    fmt.Printf("  ... Passed\n")
  }
}

func BenchmarkTestRPCCountPaxos(b *testing.B) {
  for i := 0; i < b.N; i++ {
    runtime.GOMAXPROCS(4)

    fmt.Printf("Test: RPC counts aren't too high ...\n")

    const npaxos = 3
    var pxa []*paxos.Paxos = make([]*paxos.Paxos, npaxos)
    var pxh []string = make([]string, npaxos)
    defer r_p_cleanup(pxa)

    for i := 0; i < npaxos; i++ {
      pxh[i] = port("count", i)
    }
    for i := 0; i < npaxos; i++ {
      pxa[i] = paxos.Make(pxh, i, nil)
    }

    ninst1 := 5
    seq := 0
    for i := 0; i < ninst1; i++ {
      pxa[0].Start(seq, "x")
      r_p_waitn(b, pxa, seq, npaxos)
      seq++
    }

    time.Sleep(2 * time.Second)

    total1 := 0
    for j := 0; j < npaxos; j++ {
      total1 += pxa[j].GetRPCCount()
    }

    // per agreement:
    // 3 prepares
    // 3 accepts
    // 3 decides
    expected1 := ninst1 * npaxos * npaxos
    if total1 > expected1 {
      b.Fatalf("too many RPCs for serial Start()s; %v instances, got %v, expected %v",
        ninst1, total1, expected1)
    }

    ninst2 := 5
    for i := 0; i < ninst2; i++ {
      for j := 0; j < npaxos; j++ {
        go pxa[j].Start(seq, j + (i * 10))
      }
      r_p_waitn(b, pxa, seq, npaxos)
      seq++
    }

    time.Sleep(2 * time.Second)

    total2 := 0
    for j := 0; j < npaxos; j++ {
      total2 += pxa[j].GetRPCCount()
    }
    total2 -= total1

    // worst case per agreement:
    // Proposer 1: 3 prep, 3 acc, 3 decides.
    // Proposer 2: 3 prep, 3 acc, 3 prep, 3 acc, 3 decides.
    // Proposer 3: 3 prep, 3 acc, 3 prep, 3 acc, 3 prep, 3 acc, 3 decides.
    expected2 := ninst2 * npaxos * 15
    if total2 > expected2 {
      b.Fatalf("too many RPCs for concurrent Start()s; %v instances, got %v, expected %v",
        ninst2, total2, expected2)
    }

    fmt.Printf("  ... Passed\n")
  }
}

//
// many agreements (without failures)
//
func BenchmarkTestManyPaxos(b *testing.B) {
  for i := 0; i < b.N; i++ {
    runtime.GOMAXPROCS(4)

    fmt.Printf("Test: Many instances ...\n")

    const npaxos = 3
    var pxa []*paxos.Paxos = make([]*paxos.Paxos, npaxos)
    var pxh []string = make([]string, npaxos)
    defer r_p_cleanup(pxa)

    for i := 0; i < npaxos; i++ {
      pxh[i] = port("many", i)
    }
    for i := 0; i < npaxos; i++ {
      pxa[i] = paxos.Make(pxh, i, nil)
      pxa[i].Start(0, 0)
    }

    const ninst = 50
    for seq := 1; seq < ninst; seq++ {
      // only 5 active instances, to limit the
      // number of file descriptors.
      for seq >= 5 && r_p_ndecided(b, pxa, seq - 5) < npaxos {
        time.Sleep(20 * time.Millisecond)
      }
      for i := 0; i < npaxos; i++ {
        pxa[i].Start(seq, (seq * 10) + i)
      }
    }

    for {
      done := true
      for seq := 1; seq < ninst; seq++ {
        if r_p_ndecided(b, pxa, seq) < npaxos {
          done = false
        }
      }
      if done {
        break
      }
      time.Sleep(100 * time.Millisecond)
    }

    fmt.Printf("  ... Passed\n")
  }
}

//
// a peer starts up, with proposal, after others decide.
// then another peer starts, without a proposal.
// 
func BenchmarkTestOldPaxos(b *testing.B) {
  for i := 0; i < b.N; i++ {
    runtime.GOMAXPROCS(4)

    fmt.Printf("Test: Minority proposal ignored ...\n")

    const npaxos = 5
    var pxa []*paxos.Paxos = make([]*paxos.Paxos, npaxos)
    var pxh []string = make([]string, npaxos)
    defer r_p_cleanup(pxa)

    for i := 0; i < npaxos; i++ {
      pxh[i] = port("old", i)
    }

    pxa[1] = paxos.Make(pxh, 1, nil)
    pxa[2] = paxos.Make(pxh, 2, nil)
    pxa[3] = paxos.Make(pxh, 3, nil)
    pxa[1].Start(1, 111)

    r_p_waitmajority(b, pxa, 1)

    pxa[0] = paxos.Make(pxh, 0, nil)
    pxa[0].Start(1, 222)

    r_p_waitn(b, pxa, 1, 4)

    if false {
      pxa[4] = paxos.Make(pxh, 4, nil)
      r_p_waitn(b, pxa, 1, npaxos)
    }

    fmt.Printf("  ... Passed\n")
  }
}

func r_p_part(b *testing.B, tag string, npaxos int, p1 []int, p2 []int, p3 []int) {
  r_p_cleanpp(tag, npaxos)

  pa := [][]int{p1, p2, p3}
  for pi := 0; pi < len(pa); pi++ {
    p := pa[pi]
    for i := 0; i < len(p); i++ {
      for j := 0; j < len(p); j++ {
        ij := r_p_pp(tag, p[i], p[j])
        pj := port(tag, p[j])
        err := os.Link(pj, ij)
        if err != nil {
          // one reason this link can fail is if the
          // corresponding Paxos peer has prematurely quit and
          // deleted its socket file (e.g., called px.Kill()).
          b.Fatalf("os.Link(%v, %v): %v\n", pj, ij, err)
        }
      }
    }
  }
}

func BenchmarkTestPartitionPaxos(b *testing.B) {
  for i := 0; i < b.N; i++ {
    runtime.GOMAXPROCS(4)

    tag := "partition"
    const npaxos = 5
    var pxa []*paxos.Paxos = make([]*paxos.Paxos, npaxos)
    defer r_p_cleanup(pxa)
    defer r_p_cleanpp(tag, npaxos)

    for i := 0; i < npaxos; i++ {
      var pxh []string = make([]string, npaxos)
      for j := 0; j < npaxos; j++ {
        if j == i {
          pxh[j] = port(tag, i)
        } else {
          pxh[j] = r_p_pp(tag, i, j)
        }
      }
      pxa[i] = paxos.Make(pxh, i, nil)
    }
    defer r_p_part(b, tag, npaxos, []int{}, []int{}, []int{})

    seq := 0

    fmt.Printf("Test: No decision if partitioned ...\n")

    r_p_part(b, tag, npaxos, []int{0,2}, []int{1,3}, []int{4})
    pxa[1].Start(seq, 111)
    r_p_checkmax(b, pxa, seq, 0)
    
    fmt.Printf("  ... Passed\n")

    fmt.Printf("Test: Decision in majority partition ...\n")

    r_p_part(b, tag, npaxos, []int{0}, []int{1,2,3}, []int{4})
    time.Sleep(2 * time.Second)
    r_p_waitmajority(b, pxa, seq)

    fmt.Printf("  ... Passed\n")

    fmt.Printf("Test: All agree after full heal ...\n")

    pxa[0].Start(seq, 1000) // poke them
    pxa[4].Start(seq, 1004)
    r_p_part(b, tag, npaxos, []int{0,1,2,3,4}, []int{}, []int{})

    r_p_waitn(b, pxa, seq, npaxos)

    fmt.Printf("  ... Passed\n")

    fmt.Printf("Test: One peer switches partitions ...\n")

    for iters := 0; iters < 20; iters++ {
      seq++

      r_p_part(b, tag, npaxos, []int{0,1,2}, []int{3,4}, []int{})
      pxa[0].Start(seq, seq * 10)
      pxa[3].Start(seq, (seq * 10) + 1)
      r_p_waitmajority(b, pxa, seq)
      if r_p_ndecided(b, pxa, seq) > 3 {
        b.Fatalf("too many decided")
      }
      
      r_p_part(b, tag, npaxos, []int{0,1}, []int{2,3,4}, []int{})
      r_p_waitn(b, pxa, seq, npaxos)
    }

    fmt.Printf("  ... Passed\n")

    fmt.Printf("Test: One peer switches partitions, unreliable ...\n")


    for iters := 0; iters < 20; iters++ {
      seq++

      for i := 0; i < npaxos; i++ {
        pxa[i].SetUnreliable(true)
      }

      r_p_part(b, tag, npaxos, []int{0,1,2}, []int{3,4}, []int{})
      for i := 0; i < npaxos; i++ {
        pxa[i].Start(seq, (seq * 10) + i)
      }
      r_p_waitn(b, pxa, seq, 3)
      if r_p_ndecided(b, pxa, seq) > 3 {
        b.Fatalf("too many decided")
      }
      
      r_p_part(b, tag, npaxos, []int{0,1}, []int{2,3,4}, []int{})

      for i := 0; i < npaxos; i++ {
        pxa[i].SetUnreliable(false)
      }

      r_p_waitn(b, pxa, seq, 5)
    }

    fmt.Printf("  ... Passed\n")
  }
}

func BenchmarkTestLotsPaxos(b *testing.B) {
  for i := 0; i < b.N; i++ {
    runtime.GOMAXPROCS(4)

    fmt.Printf("Test: Many requests, changing partitions ...\n")

    tag := "lots"
    const npaxos = 5
    var pxa []*paxos.Paxos = make([]*paxos.Paxos, npaxos)
    defer r_p_cleanup(pxa)
    defer r_p_cleanpp(tag, npaxos)

    for i := 0; i < npaxos; i++ {
      var pxh []string = make([]string, npaxos)
      for j := 0; j < npaxos; j++ {
        if j == i {
          pxh[j] = port(tag, i)
        } else {
          pxh[j] = r_p_pp(tag, i, j)
        }
      }
      pxa[i] = paxos.Make(pxh, i, nil)
      pxa[i].SetUnreliable(true)
    }
    defer r_p_part(b, tag, npaxos, []int{}, []int{}, []int{})

    done := false

    // re-partition periodically
    ch1 := make(chan bool)
    go func() {
      defer func(){ ch1 <- true }()
      for done == false {
        var a [npaxos]int
        for i := 0; i < npaxos; i++ {
          a[i] = (rand.Int() % 3)
        }
        pa := make([][]int, 3)
        for i := 0; i < 3; i++ {
          pa[i] = make([]int, 0)
          for j := 0; j < npaxos; j++ {
            if a[j] == i {
              pa[i] = append(pa[i], j)
            }
          }
        }
        r_p_part(b, tag, npaxos, pa[0], pa[1], pa[2])
        time.Sleep(time.Duration(rand.Int63() % 200) * time.Millisecond)
      }
    }()

    seq := 0

    // periodically start a new instance
    ch2 := make(chan bool)
    go func () {
      defer func() { ch2 <- true } ()
      for done == false {
        // how many instances are in progress?
        nd := 0
        for i := 0; i < seq; i++ {
          if r_p_ndecided(b, pxa, i) == npaxos {
            nd++
          }
        }
        if seq - nd < 10 {
          for i := 0; i < npaxos; i++ {
            pxa[i].Start(seq, rand.Int() % 10)
          }
          seq++
        }
        time.Sleep(time.Duration(rand.Int63() % 300) * time.Millisecond)
      }
    }()

    // periodically check that decisions are consistent
    ch3 := make(chan bool)
    go func() {
      defer func() { ch3 <- true }()
      for done == false {
        for i := 0; i < seq; i++ {
          r_p_ndecided(b, pxa, i)
        }
        time.Sleep(time.Duration(rand.Int63() % 300) * time.Millisecond)
      }
    }()

    time.Sleep(20 * time.Second)
    done = true
    <- ch1
    <- ch2
    <- ch3

    // repair, then check that all instances decided.
    for i := 0; i < npaxos; i++ {
      pxa[i].SetUnreliable(false)
    }
    r_p_part(b, tag, npaxos, []int{0,1,2,3,4}, []int{}, []int{})
    time.Sleep(5 * time.Second)

    for i := 0; i < seq; i++ {
      r_p_waitmajority(b, pxa, i)
    }

    fmt.Printf("  ... Passed\n")
  }
}

func r_p_waitmajority(b *testing.B, pxa[]*paxos.Paxos, seq int) {
  r_p_waitn(b, pxa, seq, (len(pxa) / 2) + 1)
}

func r_p_cleanpp(tag string, n int) {
  for i := 0; i < n; i++ {
    for j := 0; j < n; j++ {
      ij := r_p_pp(tag, i, j)
      os.Remove(ij)
    }
  }
}

func r_p_pp(tag string, src int, dst int) string {
  s := "/var/tmp/824-"
  s += strconv.Itoa(os.Getuid()) + "/"
  s += "px-" + tag + "-"
  s += strconv.Itoa(os.Getpid()) + "-"
  s += strconv.Itoa(src) + "-"
  s += strconv.Itoa(dst)
  return s
}

func r_p_checkmax(b *testing.B, pxa[]*paxos.Paxos, seq int, max int) {
  time.Sleep(3 * time.Second)
  nd := r_p_ndecided(b, pxa, seq)
  if nd > max {
    b.Fatalf("too many decided; seq=%v ndecided=%v max=%v", seq, nd, max)
  }
}

//
// Lab3A Mencius Benchmarks
//

func BenchmarkTestForgetMencius(b *testing.B) {
  for i := 0; i < b.N; i++ {
    runtime.GOMAXPROCS(4)

    curr_group_size = 6
    const npaxos = 6
    var pxa []*mencius.Paxos = make([]*mencius.Paxos, npaxos)
    var pxh []string = make([]string, npaxos)
    var uids[]int = make([]int, npaxos)


    defer r_m_cleanup(pxa)

    for i := 0; i < npaxos; i++ {
      pxh[i] = port("gc", i)
    }
    for i := 0; i < npaxos; i++ {
      uids[i] = int(common.Nrand())
      pxa[i] = mencius.Make(common.StringPaxos, pxh, i, nil, uids[i], needPersistence, false)
    }

    fmt.Printf("Test: Forgetting ...\n")

    // initial Min() correct?
    for i := 0; i < npaxos; i++ {
      m := pxa[i].Min()
      if m > 0 {
        b.Fatalf("wrong initial Min() %v", m)
      }
    }

    pxa[Owner(0)].Start(0, "00")
    pxa[Owner(1)].Start(1, "11")
    pxa[Owner(2)].Start(2, "22")
    pxa[Owner(6)].Start(6, "66")
    pxa[Owner(7)].Start(7, "77")

    r_m_waitn(b, pxa, 0, npaxos)

    // Min() correct?
    for i := 0; i < npaxos; i++ {
      m := pxa[i].Min()
      if m != 0 {
        b.Fatalf("wrong Min() %v; expected 0", m)
      }
    }

    r_m_waitn(b, pxa, 1, npaxos)

    // Min() correct?
    for i := 0; i < npaxos; i++ {
      m := pxa[i].Min()
      if m != 0 {
        b.Fatalf("wrong Min() %v; expected 0", m)
      }
    }

    // everyone Done() -> Min() changes?
    for i := 0; i < npaxos; i++ {
      pxa[i].Done(0)
    }
    for i := 1; i < npaxos; i++ {
      pxa[i].Done(1)
    }
    for i := 0; i < npaxos; i++ {
      pxa[Owner(8+i)].Start(8 + i, "xx")
      // these need to be done sequentially in Mencius,
      // otherwise a higher sequence that happens first
      // might preempt a skip which will delays Done propagation
      r_m_waitmajority(b, pxa, 8+i)
    }
    allok := false
    for iters := 0; iters < 12; iters++ {
      allok = true
      for i := 0; i < npaxos; i++ {
        s := pxa[i].Min()
        if s != 1 {
          allok = false
        }
      }
      if allok {
        break
      }
      time.Sleep(1 * time.Second)
    }
    if allok != true {
      b.Fatalf("Min() did not advance after Done()")
    }

    fmt.Printf("  ... Passed\n")
  }
}

func BenchmarkTestManyForgetMencius(b *testing.B) {
  for i := 0; i < b.N; i++ {
    runtime.GOMAXPROCS(4)

    curr_group_size = 3
    const npaxos = 3
    var pxa []*mencius.Paxos = make([]*mencius.Paxos, npaxos)
    var pxh []string = make([]string, npaxos)
    var uids []int = make([]int, npaxos)
    defer r_m_cleanup(pxa)

    for i := 0; i < npaxos; i++ {
      pxh[i] = port("manygc", i)
    }
    for i := 0; i < npaxos; i++ {
      uids[i] = int(common.Nrand())
      pxa[i] = mencius.Make(common.StringPaxos, pxh, i, nil, uids[i],  needPersistence, false)
      pxa[i].SetUnreliable(true)
    }

    fmt.Printf("Test: Lots of forgetting ...\n")

    const maxseq = 20
    done := false

    go func() {
      na := rand.Perm(maxseq)
      for i := 0; i < len(na); i++ {
        seq := na[i]
        //j := (rand.Int() % npaxos)
        v := rand.Int()
        pxa[Owner(seq)].Start(seq, v)
        runtime.Gosched()
      }
    }()

    go func() {
      for done == false {
        seq := (rand.Int() % maxseq)
        i := (rand.Int() % npaxos)
        if seq >= pxa[i].Min() {
          decided, _ := pxa[i].Status(seq)
          if decided {
            pxa[i].Done(seq)
          }
        }
        runtime.Gosched()
      }
    }()

    time.Sleep(5 * time.Second)
    done = true
    for i := 0; i < npaxos; i++ {
      pxa[i].SetUnreliable(false)
    }
    time.Sleep(2 * time.Second)

    for seq := 0; seq < maxseq; seq++ {
      for i := 0; i < npaxos; i++ {
        if seq >= pxa[i].Min() {
          pxa[i].Status(seq)
        }
      }
    }

    fmt.Printf("  ... Passed\n")
  }
}

//
// does paxos forgetting actually free the memory?
//
func BenchmarkTestForgetMemMencius(b *testing.B) {
  for i := 0; i < b.N; i++ {
    runtime.GOMAXPROCS(4)

    fmt.Printf("Test: Paxos frees forgotten instance memory ...\n")

    curr_group_size = 3
    const npaxos = 3
    var pxa []*mencius.Paxos = make([]*mencius.Paxos, npaxos)
    var pxh []string = make([]string, npaxos)
    var uids[]int = make([]int, npaxos)
    defer r_m_cleanup(pxa)

    for i := 0; i < npaxos; i++ {
      pxh[i] = port("gcmem", i)
    }
    for i := 0; i < npaxos; i++ {
      uids[i] = int(common.Nrand())
      pxa[i] = mencius.Make(common.StringPaxos, pxh, i, nil, uids[i], needPersistence, false)
    }

    pxa[Owner(0)].Start(0, "x")
    r_m_waitn(b, pxa, 0, npaxos)

    runtime.GC()
    var m0 runtime.MemStats
    runtime.ReadMemStats(&m0)
    // m0.Alloc about a megabyte

    for i := 1; i <= 10; i++ {
      big := make([]byte, 1000000)
      for j := 0; j < len(big); j++ {
        big[j] = byte('a' + rand.Int() % 26)
      }
      pxa[Owner(i)].Start(i, string(big))
      r_m_waitn(b, pxa, i, npaxos)
    }

    runtime.GC()
    var m1 runtime.MemStats
    runtime.ReadMemStats(&m1)
    // m1.Alloc about 90 megabytes

    for i := 0; i < npaxos; i++ {
      pxa[i].Done(10)
    }
    for i := 0; i < npaxos; i++ {
      pxa[Owner(11+i)].Start(11 + i, "z")
    }
    time.Sleep(3 * time.Second)
    for i := 0; i < npaxos; i++ {
      if pxa[i].Min() != 11 {
        b.Fatalf("expected Min() %v, got %v\n", 11, pxa[i].Min())
      }
    }

    runtime.GC()
    var m2 runtime.MemStats
    runtime.ReadMemStats(&m2)
    // m2.Alloc about 10 megabytes

    if m2.Alloc > (m1.Alloc / 2) {
      b.Fatalf("memory use did not shrink enough")
    }

    fmt.Printf("  ... Passed\n")
  }
}

func BenchmarkTestRPCCountMencius(b *testing.B) {
  for i := 0; i < b.N; i++ {
    runtime.GOMAXPROCS(4)

    fmt.Printf("Test: RPC counts aren't too high ...\n")

    curr_group_size = 3
    const npaxos = 3
    var pxa []*mencius.Paxos = make([]*mencius.Paxos, npaxos)
    var pxh []string = make([]string, npaxos)
    var uids[]int = make([]int, npaxos)
    defer r_m_cleanup(pxa)

    for i := 0; i < npaxos; i++ {
      pxh[i] = port("count", i)
    }
    for i := 0; i < npaxos; i++ {
      uids[i] = int(common.Nrand())
      pxa[i] = mencius.Make(common.StringPaxos, pxh, i, nil, uids[i], needPersistence, false)
    }

    ninst1 := 5
    seq := 0
    for i := 0; i < ninst1; i++ {
      pxa[Owner(seq)].Start(seq, "x")
      r_m_waitn(b, pxa, seq, npaxos)
      seq++
    }

    time.Sleep(2 * time.Second)

    total1 := 0
    for j := 0; j < npaxos; j++ {
      total1 += pxa[j].GetRPCCount()
    }

    // per agreement:
    // 3 prepares
    // 3 accepts
    // 3 decides
    expected1 := ninst1 * npaxos * npaxos
    if total1 > expected1 {
      b.Fatalf("too many RPCs for serial Start()s; %v instances, got %v, expected %v",
        ninst1, total1, expected1)
    }

    ninst2 := 5
    for i := 0; i < ninst2; i++ {
      for j := 0; j < npaxos; j++ {
        go pxa[Owner(seq)].Start(seq, j + (i * 10))
      }
      r_m_waitn(b, pxa, seq, npaxos)
      seq++
    }

    time.Sleep(2 * time.Second)

    total2 := 0
    for j := 0; j < npaxos; j++ {
      total2 += pxa[j].GetRPCCount()
    }
    total2 -= total1

    // worst case per agreement:
    // Proposer 1: 3 prep, 3 acc, 3 decides.
    // Proposer 2: 3 prep, 3 acc, 3 prep, 3 acc, 3 decides.
    // Proposer 3: 3 prep, 3 acc, 3 prep, 3 acc, 3 prep, 3 acc, 3 decides.
    expected2 := ninst2 * npaxos * 15
    if total2 > expected2 {
      b.Fatalf("too many RPCs for concurrent Start()s; %v instances, got %v, expected %v",
        ninst2, total2, expected2)
    }

    fmt.Printf("  ... Passed\n")
  }
}

//
// many agreements (without failures)
//
func BenchmarkTestManyMencius(b *testing.B) {
  for i := 0; i < b.N; i++ {
    runtime.GOMAXPROCS(4)

    fmt.Printf("Test: Many instances ...\n")

    curr_group_size = 3
    const npaxos = 3
    var pxa []*mencius.Paxos = make([]*mencius.Paxos, npaxos)
    var pxh []string = make([]string, npaxos)
    var uids[]int = make([]int, npaxos)
    defer r_m_cleanup(pxa)

    for i := 0; i < npaxos; i++ {
      pxh[i] = port("many", i)
    }
    for i := 0; i < npaxos; i++ {
      uids[i] = int(common.Nrand())
      pxa[i] = mencius.Make(common.StringPaxos, pxh, i, nil, uids[i], needPersistence, false)
    }
    pxa[Owner(0)].Start(0, 0)

    const ninst = 50
    for seq := 1; seq < ninst; seq++ {
      // only 5 active instances, to limit the
      // number of file descriptors.
      for seq >= 5 && r_m_ndecided(b, pxa, seq - 5) < npaxos {
        time.Sleep(20 * time.Millisecond)
      }
      for i := 0; i < npaxos; i++ {
        pxa[Owner(seq)].Start(seq, (seq * 10) + i)
      }
    }

    for {
      done := true
      for seq := 1; seq < ninst; seq++ {
        if r_m_ndecided(b, pxa, seq) < npaxos {
          done = false
        }
      }
      if done {
        break
      }
      time.Sleep(100 * time.Millisecond)
    }

    fmt.Printf("  ... Passed\n")
  }
}

//
// a peer starts up, with proposal, after others decide.
// then another peer starts, without a proposal.
// 
func BenchmarkTestOldMencius(b *testing.B) {
  for i := 0; i < b.N; i++ {
    runtime.GOMAXPROCS(4)

    fmt.Printf("Test: Minority proposal ignored ...\n")

    curr_group_size = 5
    const npaxos = 5
    var pxa []*mencius.Paxos = make([]*mencius.Paxos, npaxos)
    var pxh []string = make([]string, npaxos)
    var uids[]int = make([]int, npaxos)
    defer r_m_cleanup(pxa)

    for i := 0; i < npaxos; i++ {
      uids[i] = int(common.Nrand())
      pxh[i] = port("old", i)
    }

    pxa[1] = mencius.Make(common.StringPaxos, pxh, 1, nil, uids[1], needPersistence, false)
    pxa[2] = mencius.Make(common.StringPaxos, pxh, 2, nil, uids[2], needPersistence, false)
    pxa[3] = mencius.Make(common.StringPaxos, pxh, 3, nil, uids[3], needPersistence, false)
    pxa[Owner(1)].Start(1, 111)

    r_m_waitmajority(b, pxa, 1)

    pxa[0] = mencius.Make(common.StringPaxos, pxh, 0, nil, uids[0], needPersistence,false)
    // in Mencius, you call Revoke if it's on an instance
    // you don't own and you want to find out about
    //pxa[0].Start(1, 222)
    pxa[0].Revoke(1)

    r_m_waitn(b, pxa, 1, 4)

    if false {
      pxa[4] = mencius.Make(common.StringPaxos, pxh, 4, nil, uids[4], needPersistence,false)
      r_m_waitn(b, pxa, 1, npaxos)
    }

    fmt.Printf("  ... Passed\n")
  }
}

func r_m_part(b *testing.B, tag string, npaxos int, p1 []int, p2 []int, p3 []int) {
  r_m_cleanpp(tag, npaxos)

  pa := [][]int{p1, p2, p3}
  for pi := 0; pi < len(pa); pi++ {
    p := pa[pi]
    for i := 0; i < len(p); i++ {
      for j := 0; j < len(p); j++ {
        ij := r_m_pp(tag, p[i], p[j])
        pj := port(tag, p[j])
        err := os.Link(pj, ij)
        if err != nil {
          // one reason this link can fail is if the
          // corresponding Paxos peer has prematurely quit and
          // deleted its socket file (e.g., called px.Kill()).
          b.Fatalf("os.Link(%v, %v): %v\n", pj, ij, err)
        }
      }
    }
  }
}

func BenchmarkTestPartitionMencius(b *testing.B) {
  for i := 0; i < b.N; i++ {
    runtime.GOMAXPROCS(4)

    tag := "partition"
    curr_group_size = 5
    const npaxos = 5
    var pxa []*mencius.Paxos = make([]*mencius.Paxos, npaxos)
    defer r_m_cleanup(pxa)
    defer r_m_cleanpp(tag, npaxos)
    var uids[]int = make([]int, npaxos)

    for i := 0; i < npaxos; i++ {
      var pxh []string = make([]string, npaxos)
      uids[i] = int(common.Nrand())
      for j := 0; j < npaxos; j++ {
        if j == i {
          pxh[j] = port(tag, i)
        } else {
          pxh[j] = r_m_pp(tag, i, j)
        }
      }
      pxa[i] = mencius.Make(common.StringPaxos, pxh, i, nil,  uids[i],needPersistence, false)
    }
    defer r_m_part(b, tag, npaxos, []int{}, []int{}, []int{})

    seq := 1

    fmt.Printf("Test: No decision if partitioned ...\n")

    r_m_part(b, tag, npaxos, []int{0,2}, []int{1,3}, []int{4})
    pxa[Owner(seq)].Start(seq, 111)
    r_m_checkmax(b, pxa, seq, 0)
    
    fmt.Printf("  ... Passed\n")

    fmt.Printf("Test: Decision in majority partition ...\n")

    r_m_part(b, tag, npaxos, []int{0}, []int{1,2,3}, []int{4})
    time.Sleep(2 * time.Second)
    r_m_waitmajority(b, pxa, seq)

    fmt.Printf("  ... Passed\n")

    fmt.Printf("Test: All agree after full heal ...\n")
    // "poking" is calling revoke in Mencius
    pxa[0].Revoke(seq)
    pxa[4].Revoke(seq)
    //pxa[0].Start(seq, 1000) // poke them
    //pxa[4].Start(seq, 1004)
    r_m_part(b, tag, npaxos, []int{0,1,2,3,4}, []int{}, []int{})

    r_m_waitn(b, pxa, seq, npaxos)

    fmt.Printf("  ... Passed\n")

    fmt.Printf("Test: One peer switches partitions ...\n")

    // To test this in Mencius, the sequence numbers
    // for server 0 to start must be ones it owns
    seq = npaxos
    for iters := 0; iters < 20; iters++ {
      seq = seq + npaxos

      r_m_part(b, tag, npaxos, []int{0,1,2}, []int{3,4}, []int{})
      pxa[0].Start(seq, seq * 10)
      // Both 3/4 need to be poked, since there is no decision
      // propagation if a decision has already been reached
      pxa[3].Revoke(seq)
      pxa[4].Revoke(seq)
      //pxa[0].Start(seq, seq * 10)
      //pxa[3].Start(seq, (seq * 10) + 1)
      r_m_waitmajority(b, pxa, seq)
      if r_m_ndecided(b, pxa, seq) > 3 {
        b.Fatalf("too many decided")
      }
      r_m_part(b, tag, npaxos, []int{0,1}, []int{2,3,4}, []int{})
      r_m_waitn(b, pxa, seq, npaxos)
    }

    fmt.Printf("  ... Passed\n")

    fmt.Printf("Test: One peer switches partitions, unreliable ...\n")
    
    // Since the revocations might win duels (thereby setting an
    // instance's value to no-op) we must ignore value checks
    match_value_flag = true
    for iters := 0; iters < 20; iters++ {
      seq++

      for i := 0; i < npaxos; i++ {
        pxa[i].SetUnreliable(true)
      }

      r_m_part(b, tag, npaxos, []int{0,1,2}, []int{3,4}, []int{})
      for i := 0; i < npaxos; i++ {
        if i == Owner(seq) {
          pxa[i].Start(seq, (seq * 10) + i)
        } else {
          pxa[i].Revoke(seq)
        }
      }
      r_m_waitn(b, pxa, seq, 3)
      if r_m_ndecided(b, pxa, seq) > 3 {
        b.Fatalf("too many decided")
      }
      
      r_m_part(b, tag, npaxos, []int{0,1}, []int{2,3,4}, []int{})

      for i := 0; i < npaxos; i++ {
        pxa[i].SetUnreliable(false)
      }

      r_m_waitn(b, pxa, seq, 5)
    }
    match_value_flag = false

    fmt.Printf("  ... Passed\n")
  }
}

func BenchmarkTestLotsMencius(b *testing.B) {
  for i := 0; i < b.N; i++ {
    runtime.GOMAXPROCS(4)

    fmt.Printf("Test: Many requests, changing partitions ...\n")

    // for revocation calls
    match_value_flag = true

    tag := "lots"
    curr_group_size = 5
    const npaxos = 5
    var pxa []*mencius.Paxos = make([]*mencius.Paxos, npaxos)
    var uids[]int = make([]int, npaxos)
    defer r_m_cleanup(pxa)
    defer r_m_cleanpp(tag, npaxos)

    for i := 0; i < npaxos; i++ {
      var pxh []string = make([]string, npaxos)
      uids[i] = int(common.Nrand())
      for j := 0; j < npaxos; j++ {
        if j == i {
          pxh[j] = port(tag, i)
        } else {
          pxh[j] = r_m_pp(tag, i, j)
        }
      }
      pxa[i] = mencius.Make(common.StringPaxos, pxh, i, nil,  uids[i], needPersistence, false)
      pxa[i].SetUnreliable(true)
    }
    defer r_m_part(b, tag, npaxos, []int{}, []int{}, []int{})

    done := false

    // re-partition periodically
    ch1 := make(chan bool)
    go func() {
      defer func(){ ch1 <- true }()
      for done == false {
        var a [npaxos]int
        for i := 0; i < npaxos; i++ {
          a[i] = (rand.Int() % 3)
        }
        pa := make([][]int, 3)
        for i := 0; i < 3; i++ {
          pa[i] = make([]int, 0)
          for j := 0; j < npaxos; j++ {
            if a[j] == i {
              pa[i] = append(pa[i], j)
            }
          }
        }
        r_m_part(b, tag, npaxos, pa[0], pa[1], pa[2])
        time.Sleep(time.Duration(rand.Int63() % 200) * time.Millisecond)
      }
    }()

    seq := 0

    // periodically start a new instance
    ch2 := make(chan bool)
    go func () {
      defer func() { ch2 <- true } ()
      for done == false {
        // how many instances are in progress?
        nd := 0
        for i := 0; i < seq; i++ {
          if r_m_ndecided(b, pxa, i) == npaxos {
            nd++
          }
        }
        if seq - nd < 10 {
          for i := 0; i < npaxos; i++ {
            if i == Owner(seq) {
              pxa[i].Start(seq, rand.Int() % 10)
            } else {
              pxa[i].Revoke(seq)
            }
          }
          seq++
        }
        time.Sleep(time.Duration(rand.Int63() % 300) * time.Millisecond)
      }
    }()

    // periodically check that decisions are consistent
    ch3 := make(chan bool)
    go func() {
      defer func() { ch3 <- true }()
      for done == false {
        for i := 0; i < seq; i++ {
          r_m_ndecided(b, pxa, i)
        }
        time.Sleep(time.Duration(rand.Int63() % 300) * time.Millisecond)
      }
    }()

    time.Sleep(20 * time.Second)
    done = true
    <- ch1
    <- ch2
    <- ch3

    // repair, then check that all instances decided.
    for i := 0; i < npaxos; i++ {
      pxa[i].SetUnreliable(false)
    }
    r_m_part(b, tag, npaxos, []int{0,1,2,3,4}, []int{}, []int{})
    time.Sleep(5 * time.Second)

    for i := 0; i < seq; i++ {
      r_m_waitmajority(b, pxa, i)
    }
    match_value_flag = false

    fmt.Printf("  ... Passed\n")
  }
}

func r_m_waitmajority(b *testing.B, pxa[]*mencius.Paxos, seq int) {
  r_m_waitn(b, pxa, seq, (len(pxa) / 2) + 1)
}

func r_m_cleanpp(tag string, n int) {
  for i := 0; i < n; i++ {
    for j := 0; j < n; j++ {
      ij := r_m_pp(tag, i, j)
      os.Remove(ij)
    }
  }
}

func r_m_pp(tag string, src int, dst int) string {
  s := "/var/tmp/824-"
  s += strconv.Itoa(os.Getuid()) + "/"
  s += "px-" + tag + "-"
  s += strconv.Itoa(os.Getpid()) + "-"
  s += strconv.Itoa(src) + "-"
  s += strconv.Itoa(dst)
  return s
}

func r_m_checkmax(b *testing.B, pxa[]*mencius.Paxos, seq int, max int) {
  time.Sleep(3 * time.Second)
  nd := r_m_ndecided(b, pxa, seq)
  if nd > max {
    b.Fatalf("too many decided; seq=%v r_m_ndecided=%v max=%v", seq, nd, max)
  }
}

