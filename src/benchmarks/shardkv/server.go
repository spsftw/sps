package shardkv

import "net"
import "fmt"
import "net/rpc"
import "log"
import "time"
import "benchmarks/paxos"
import "sync"
import "os"
import "syscall"
import "encoding/gob"
import "math/rand"
import "benchmarks/shardmaster"
import "strconv"

const Debug=0

func DPrintf(format string, a ...interface{}) (n int, err error) {
  if Debug > 0 {
     log.Printf(format, a...)
  }
  return
}


type Op struct {
  // Your definitions here.
  Cmd string // Get, Put, PutHash, Reconfig
  Args *Args
  Sn int64 // Serial number of request
  Config *shardmaster.Config // Optional, for cmd Reconfig
  ConfigNum int
  Key string
}

type Args struct {
  GetArgs *GetArgs
  PutArgs *PutArgs
}

type TransferArgs struct {
  Shards [shardmaster.NShards]int
  ConfigNum int
}

type TransferReply struct {
  Err Err
  KVMap map[int] map[string]string
  ReplyMap map[int] map[int64]*Reply
}

type Reply struct {
  Cmd string // Get, Put, PutHash
  Sn int64 // Serial number of request
  Err Err
  GetReply *GetReply
  PutReply *PutReply
}

type ShardKV struct {
  mu sync.Mutex
  l net.Listener
  me int
  dead bool // for testing
  unreliable bool // for testing
  sm *shardmaster.Clerk
  px *paxos.Paxos
  rpcCount int

  gid int64 // my replica group ID

  // Your definitions here.
  config *shardmaster.Config  // current configuration
  processed int // processed instance number
  kvmap [shardmaster.NShards] map[string]string // shard to kvmap
  replymap [shardmaster.NShards] map[int64] *Reply // shard to replymap
  proposed int
}


func (kv *ShardKV) Get(args *GetArgs, reply *GetReply) error {
  // DPrintf("gid: %d GET KEY %s", kv.gid, args.Key)
  kv.mu.Lock()
  defer kv.mu.Unlock()

  var instance int
  instance = kv.px.Max() + 1
  var op Op
  op = Op{Cmd:"Get", Sn: args.Sn, Args: &Args{GetArgs: args}, Key: args.Key}
  kv.px.Start(instance, op)
  // wait for agreement
  currentOp := kv.WaitForAgreement(instance)
  if currentOp == nil {  // paxos dead
     reply.Err = ErrWrongGroup
     return nil
  }
  for currentOp.Sn != op.Sn {
     instance = kv.px.Max() + 1
     kv.px.Start(instance, op)
     currentOp = kv.WaitForAgreement(instance)
     if currentOp == nil {
        reply.Err = ErrWrongGroup
        return nil
     }
  }
  kv.mu.Unlock()
  // check whether instances before that has been processed
  for kv.processed < instance {
      time.Sleep(10 * time.Millisecond)
  }

  kv.mu.Lock()
  shard := key2shard(args.Key)
  processedReply, processedReplyExisted := kv.replymap[shard][args.Sn]
  if !processedReplyExisted {
     reply.Err = ErrWrongGroup
     DPrintf("me %d gid %d GET Sn %d returning ErrWrongGroup", kv.me, kv.gid, args.Sn)
  } else {
     reply.Value = processedReply.GetReply.Value
     reply.Err = processedReply.Err
  }
  return nil
}

func (kv *ShardKV) Transfer(args *TransferArgs, reply *TransferReply) error {
  kv.mu.Lock()
  defer kv.mu.Unlock()
  if kv.config.Num >= args.ConfigNum {
     reply.KVMap = make(map[int] map[string] string)
     reply.ReplyMap = make (map[int] map[int64] *Reply)
     var logShards [shardmaster.NShards] int
     for i, s := range args.Shards {
        if s == 1 {
           reply.KVMap[i] = make(map[string] string)
           reply.ReplyMap[i] = make(map[int64] *Reply)
           for mapkey, mapvalue := range kv.kvmap[i] {
               reply.KVMap[i][mapkey] = mapvalue
           }

           for sn, storedreply := range kv.replymap[i] {
               reply.ReplyMap[i][sn] = storedreply
           }
           logShards[i] = i
        }
    }
    DPrintf("OK me %d gid %d ok want shard %d config %d. now %d", kv.me, kv.gid, logShards, args.ConfigNum, kv.config.Num)
    reply.Err = OK
  } else {
    DPrintf("NO me %d dead %v gid %d  want %d. now %d", kv.me, kv.dead, kv.gid, args.ConfigNum, kv.config.Num)
    reply.Err = ErrWait
  }
  return nil
}

func (kv *ShardKV) checkServer(shard int) bool {
  return kv.config.Shards[shard] == kv.gid
}

func (kv *ShardKV) requestShards(config *shardmaster.Config, transferreply *TransferReply) {
  DPrintf("RequestShards me %d gid %d New config %d old config %d", kv.me, kv.gid, config.Num, kv.config.Num)
  if kv.config.Num > 0 { // if kv.config.Num == 0, no need for state transfer
    var oldguy int64
    var oldguyshards [shardmaster.NShards]int
    for i, gid := range kv.config.Shards {
        newgid := config.Shards[i]
        if gid != kv.gid && newgid == kv.gid {
           if oldguy != 0 && oldguy != gid { log.Printf("Error in assumption about shardmaster") }
           if oldguy == 0 { oldguy = gid }
           oldguyshards[i] = 1
        }
    }
    if oldguy != 0 {
       kv.getShards(oldguy, oldguyshards, config.Num, transferreply)
    }
  }
}

func (kv *ShardKV) getShards(gid int64, shards [shardmaster.NShards]int, configNum int, transferreply *TransferReply) {
  DPrintf("GetShards me %d gid %d New config %d old config %d", kv.me, kv.gid, configNum, kv.config.Num)
  transferargs := TransferArgs{Shards: shards, ConfigNum: configNum}
  okToBreak := false
  for !okToBreak && !kv.dead {
    for i, s := range kv.config.Groups[gid] {
      DPrintf("me %d gid %d New config %d old config %d asking for %d %d shard %d", kv.me, kv.gid, configNum, kv.config.Num, gid, i, shards)
      ok := call(s, "ShardKV.Transfer", &transferargs, transferreply)
      okToBreak = (ok && transferreply.Err == OK)
      if okToBreak {
         break
      }
      if transferreply.Err == ErrWait {
         time.Sleep(10 * time.Millisecond)
      }
     }
   }
}

func (kv *ShardKV) updateStates(transferreply *TransferReply) {
     // log.Printf("me %d gid %d update shard %d ", kv.me, kv.gid, shard) 
     var logShards [shardmaster.NShards] int
     for shard, shardkvmap := range transferreply.KVMap {
       for mapkey, mapvalue := range shardkvmap {
           kv.kvmap[shard][mapkey] = mapvalue
           // log.Printf("me %d gid %d shard %d key %s value %s", kv.me, kv.gid, shard, mapkey, mapvalue)
       }
       logShards[shard] = shard
     }
     for shard, shardreplymap := range transferreply.ReplyMap {
       for sn, reply := range shardreplymap {
           kv.replymap[shard][sn] = reply
       }
     }
     DPrintf("me %d gid %d finish update shards %d", kv.me, kv.gid, logShards) 
}

func (kv *ShardKV) Put(args *PutArgs, reply *PutReply) error {
  kv.mu.Lock()
  defer  kv.mu.Unlock()

  // log.Printf("me %d gid: %d PUT KEY %s VALUE %s Sn: %d", kv.me, kv.gid, args.Key, args.Value, args.Sn)
  var instance int
  instance = kv.px.Max() + 1
  var op Op
  if args.DoHash {
     op = Op{Cmd:"PutHash", Key: args.Key, Sn: args.Sn, Args: &Args{PutArgs: args}}
  } else {
     op = Op{Cmd:"Put", Key: args.Key, Sn: args.Sn, Args: &Args{PutArgs: args}}
  }
  kv.px.Start(instance, op)
  // wait for agreement
  currentOp := kv.WaitForAgreement(instance)
  if currentOp == nil { // paxos dead 
     reply.Err = ErrWrongGroup
     return nil
  }

  for currentOp.Sn != op.Sn {
     instance = kv.px.Max() + 1
     kv.px.Start(instance, op)
     currentOp = kv.WaitForAgreement(instance)
     if currentOp == nil {
        reply.Err = ErrWrongGroup
        return nil
     }
  }
  // need to wait for processing
  kv.mu.Unlock()
  for kv.processed < instance {
      time.Sleep(10 * time.Millisecond)
  }
  kv.mu.Lock()

  shard := key2shard(args.Key)
  processedReply, processedReplyExisted := kv.replymap[shard][args.Sn]
  if !processedReplyExisted {
     reply.Err = ErrWrongGroup
  } else {
     reply.PreviousValue = processedReply.PutReply.PreviousValue
     reply.Err = processedReply.Err
  }
  return nil
}

//
// Ask the shardmaster if there's a new configuration;
// if so, re-configure.
//
func (kv *ShardKV) tick() {
  kv.mu.Lock()
  defer kv.mu.Unlock()
  var c shardmaster.Config
  c = kv.sm.Query(kv.config.Num + 1)
  if c.Num > kv.config.Num {
     if c.Num == kv.proposed {
        return
     }
     DPrintf("me %d gid %d Update config now %d to %d", kv.me, kv.gid, kv.config.Num, c.Num)
     var instance int
     instance = kv.px.Max() + 1
     var op Op
     op = Op{Cmd:"Reconfig", ConfigNum: c.Num, Config: &c}
     kv.px.Start(instance, op)
     // wait for agreement
     currentOp := kv.WaitForAgreement(instance)
     if currentOp == nil {
        DPrintf("me %d gid %d tick killed", kv.me, kv.gid)
        return
     }
     for !(currentOp.Cmd == "Reconfig" && currentOp.ConfigNum == c.Num) {
         DPrintf("me %d gid %d Proposing NEW CONFIG %d", kv.me, kv.gid, c.Num)
         instance = kv.px.Max() + 1
         kv.px.Start(instance, op)
         currentOp = kv.WaitForAgreement(instance)
         if currentOp == nil { 
            DPrintf("me %d gid %d tick 2 killed", kv.me, kv.gid)
            return
         }
     }
     DPrintf("me %d gid %d NEW CONFIG %d agreement reached JOB %d", kv.me, kv.gid, c.Num, instance)
     kv.proposed = c.Num
  }
}

// return Op decided on that instance
func (kv *ShardKV) WaitForAgreement(instance int) *Op {
  to := 10 * time.Millisecond
  for !kv.dead {
    decided, v := kv.px.Status(instance)
    if decided {
       currentOp, ok := v.(Op)
       if !ok {
          log.Printf("Type conversion failed")
          return nil
       }
       return &currentOp
    }
    time.Sleep(to)
    if to < 10 * time.Second {
      to *= 2
    }
  }
  return nil
}

// tell the server to shut itself down.
func (kv *ShardKV) kill() {
  kv.dead = true
  kv.l.Close()
  kv.px.Kill()
}

func (kv *ShardKV) StartBackground() {
  for !kv.dead {
    job := 1 + kv.processed
    to := 10 * time.Millisecond
    for !kv.dead {
      decided, v := kv.px.Status(job)
      if decided {
         op, ok := v.(Op)
         if !ok {
            log.Printf("Type conversion failed")
            break
         }
         kv.mu.Lock()
         dup := false
         // filter dup
         if op.Cmd != "Reconfig" {
            _, hasPreviousReply := kv.replymap[key2shard(op.Key)][op.Sn]
            if hasPreviousReply {
               dup = true
            }
         } else if op.Cmd == "Reconfig" {
           if op.Config.Num != kv.config.Num + 1 {
              DPrintf("me %d gid %d DUP RECONFIG configNum %d NOW %d", kv.me, kv.gid, op.ConfigNum, kv.config.Num)
              dup = true
           }
         }

         // end with filter dup
         if !dup {
            DPrintf("me %d gid %d Processing cmd %s sn %d configNum %d NOW %d", kv.me, kv.gid, op.Cmd, op.Sn, op.ConfigNum, kv.config.Num)
            reply := Reply{GetReply: &GetReply{}, PutReply: &PutReply{}, Sn:op.Sn}
            if op.Cmd == "Put" || op.Cmd == "Get" || op.Cmd == "PutHash" {
                shard := key2shard(op.Key)
                if kv.checkServer(shard) {
                  if op.Cmd == "Put" {
                      putArgs := op.Args.PutArgs
                      kv.kvmap[shard][putArgs.Key] = putArgs.Value
                  } else if op.Cmd == "PutHash" {
                      putArgs := op.Args.PutArgs
                      previousValue := kv.kvmap[shard][putArgs.Key]
                      h := hash(previousValue + putArgs.Value)
                      kv.kvmap[shard][putArgs.Key] = strconv.Itoa(int(h))
                      reply.PutReply.PreviousValue = previousValue
                  } else if op.Cmd == "Get" {
                      getArgs := op.Args.GetArgs
                      value := kv.kvmap[shard][getArgs.Key]
                      reply.GetReply.Value = value
                  }
                  reply.Err = OK
                  kv.replymap[shard][op.Sn] = &reply
                }
            } // end if op.Cmd == "Put" || "Get" || "PutHash"
            if op.Cmd == "Reconfig" {
                kv.mu.Unlock()
                transferreply := TransferReply{}
                kv.requestShards(op.Config, &transferreply)
                DPrintf("me %d gid %d DONE with requestshard for config %d", kv.me, kv.gid, op.Config.Num)
                kv.mu.Lock()
                if kv.config.Num > 0 { kv.updateStates(&transferreply)}
                kv.config = op.Config
            } // Reconfig
        } // end dup
        kv.processed = job
        kv.px.Done(job)
        kv.mu.Unlock()
        break
      } else {  // if not decided
        // try to propose nop
        if kv.px.Max() > job {
           kv.px.Start(job, Op{})
        }
      }
      time.Sleep(to)
      // Exponential backoff doesn't work quite well for TestLimp, coz the group becomes dead too quickly
      /**
      if to < 10 * time.Second {
        to *= 2
      }
      **/
     }
  }
}

// for the benchmarking package
func(kv *ShardKV) BenchKill() {
  kv.kill()
}
func(kv *ShardKV) GetRPCCount() int {
  return kv.rpcCount
}
func(kv *ShardKV) ResetRPCCount() {
  kv.rpcCount = 0
}

//
// Start a shardkv server.
// gid is the ID of the server's replica group.
// shardmasters[] contains the ports of the
//   servers that implement the shardmaster.
// servers[] contains the ports of the servers
//   in this replica group.
// Me is the index of this server in servers[].
//
func StartServer(gid int64, shardmasters []string,
                 servers []string, me int) *ShardKV {
  gob.Register(Op{})
  gob.Register(shardmaster.Config{})
  gob.Register(TransferArgs{})
  gob.Register(TransferReply{})
  gob.Register(Reply{})
  gob.Register(GetReply{})
  gob.Register(PutReply{})

  kv := new(ShardKV)
  kv.me = me
  kv.gid = gid
  kv.config = &shardmaster.Config{Num:0}
  kv.processed = -1
  kv.proposed = 0
  kv.rpcCount = 0
  for i, _ := range kv.kvmap {
      kv.kvmap[i] = make(map[string]string)
  }
  for i, _ := range kv.replymap {
      kv.replymap[i] = make(map[int64]*Reply)
  }

  kv.sm = shardmaster.MakeClerk(shardmasters)

  // Your initialization code here.
  // Don't call Join().

  rpcs := rpc.NewServer()
  rpcs.Register(kv)

  kv.px = paxos.Make(servers, me, rpcs)

  //start bg thread
  go kv.StartBackground()

  os.Remove(servers[me])
  l, e := net.Listen("unix", servers[me]);
  if e != nil {
    log.Fatal("listen error: ", e);
  }
  kv.l = l

  // please do not change any of the following code,
  // or do anything to subvert it.

  go func() {
    for kv.dead == false {
      conn, err := kv.l.Accept()
      if err == nil && kv.dead == false {
        if kv.unreliable && (rand.Int63() % 1000) < 100 {
          // discard the request.
          conn.Close()
        } else if kv.unreliable && (rand.Int63() % 1000) < 200 {
          // process the request but force discard of reply.
          c1 := conn.(*net.UnixConn)
          f, _ := c1.File()
          err := syscall.Shutdown(int(f.Fd()), syscall.SHUT_WR)
          if err != nil {
            fmt.Printf("shutdown: %v\n", err)
          }
          kv.rpcCount++
          go rpcs.ServeConn(conn)
        } else {
          kv.rpcCount++
          go rpcs.ServeConn(conn)
        }
      } else if err == nil {
        conn.Close()
      }
      if err != nil && kv.dead == false {
        fmt.Printf("ShardKV(%v) accept: %v\n", me, err.Error())
        kv.kill()
      }
    }
  }()

  go func() {
    for kv.dead == false {
      kv.tick()
      time.Sleep(250 * time.Millisecond)
    }
  }()

  return kv
}
