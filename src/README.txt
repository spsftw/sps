6.824 Final Project
[dkao, xunjieli, ruwenliu, psuriana]

There are two branches of our code repo. One is made for execution on a single machine, while the other is geared for deployment on a real distributed system (AWS). 

Please look at both branches for the complete project.

///////////////////////
/// master branch   ///
//////////////////////
- used to test correctedness of our code.
- communication is through unix sockets.

//////////////////////
/// aws branch    ///
/////////////////////
- used to deploy/scale our code on AWS.
- communication is through tcp.

Setup Instructions
- install sqlite3
- install sqlite3 driver: go get github.com/mattn/go-sqlite3
- use fabfile.py to deploy aws instances. You will need to 
change machine addresses
