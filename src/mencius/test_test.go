package mencius

import "testing"
import "runtime"
import "strconv"
import "os"
import "time"
import "fmt"
import "math/rand"
import "common"

//
// This testing package is a revamped version of Lab 3A tests.
// Tests have all been modified for testing Mencius (e.g., it no 
// longer makes sense to call pxa[i].Start on an instance that i
// does not own, instead in this case we should call pxa[i].Revoke
// as a way to "prod" a server that isn't sure what happened).
// In some cases, tests have been modified substantially, and
// in others tests have been added for targeting Mencius-specific
// cases, for instance TestRevoke.
//

// Variables and functions for testing Mencius
var curr_group_size int = 0
var match_value_flag bool = false
var needPersistence bool = true
var db_path = "./db"
func Owner(instance int) (int) {
  return instance % curr_group_size
}

func port(tag string, host int) string {
  s := "/var/tmp/824-"
  s += strconv.Itoa(os.Getuid()) + "/"
  os.Mkdir(s, 0777)
  s += "px-"
  s += strconv.Itoa(os.Getpid()) + "-"
  s += tag + "-"
  s += strconv.Itoa(host)
  return s
}

func ndecided(t *testing.T, pxa []*Paxos, seq int) int {
  count := 0
  var v interface{}
  for i := 0; i < len(pxa); i++ {
    if pxa[i] != nil {
      decided, v1 := pxa[i].Status(seq)
      if decided {
        if count > 0 && v != v1 {
          if match_value_flag && v != v1 {
            // ignore, we're expecting different valuse
          } else {
          t.Fatalf("decided values do not match; seq=%v i=%v v=%v v1=%v",
            seq, i, v, v1)
          }
        }
        count++
        v = v1
      }
    }
  }
  return count
}

func waitn(t *testing.T, pxa[]*Paxos, seq int, wanted int) {
  to := 10 * time.Millisecond
  for iters := 0; iters < 30; iters++ {
    if ndecided(t, pxa, seq) >= wanted {
      break
    }
    time.Sleep(to)
    if to < time.Second {
      to *= 2
    }
  }
  nd := ndecided(t, pxa, seq)
  if nd < wanted {
    t.Fatalf("too few decided; seq=%v ndecided=%v wanted=%v", seq, nd, wanted)
  }
}

func waitmajority(t *testing.T, pxa[]*Paxos, seq int) {
  waitn(t, pxa, seq, (len(pxa) / 2) + 1)
}

func checkmax(t *testing.T, pxa[]*Paxos, seq int, max int) {
  time.Sleep(3 * time.Second)
  nd := ndecided(t, pxa, seq)
  if nd > max {
    t.Fatalf("too many decided; seq=%v ndecided=%v max=%v", seq, nd, max)
  }
}

func cleanup(pxa []*Paxos) {
  for i := 0; i < len(pxa); i++ {
    if pxa[i] != nil {
      pxa[i].Kill()
    }
  }
}

func noTestSpeed(t *testing.T) {
  runtime.GOMAXPROCS(4)

  const npaxos = 3
  var pxa []*Paxos = make([]*Paxos, npaxos)
  var pxh []string = make([]string, npaxos)
  var uids []int = make([]int, npaxos)
  defer cleanup(pxa)

  for i := 0; i < npaxos; i++ {
    pxh[i] = port("time", i)
  }
  for i := 0; i < npaxos; i++ {
    uids[i] = int(common.Nrand())
    pxa[i] = Make(common.StringPaxos, pxh, i, nil, uids[i], needPersistence,false)
  }

  t0 := time.Now()

  for i := 0; i < 20; i++ {
    pxa[0].Start(i, "x")
    waitn(t, pxa, i, npaxos)
  }

  d := time.Since(t0)
  fmt.Printf("20 agreements %v seconds\n", d.Seconds())
}

func TestBasic(t *testing.T) {
  runtime.GOMAXPROCS(4)

  os.RemoveAll(fmt.Sprintf(db_path))
  os.Mkdir("db", 0777)

  curr_group_size = 3
  const npaxos = 3
  var pxa []*Paxos = make([]*Paxos, npaxos)
  var pxh []string = make([]string, npaxos)
  var uids []int = make([]int, npaxos)
  defer cleanup(pxa)

  for i := 0; i < npaxos; i++ {
    pxh[i] = port("basic", i)
  }
  for i := 0; i < npaxos; i++ {
    uids[i] = int(common.Nrand())
    pxa[i] = Make(common.StringPaxos, pxh, i, nil, uids[i], needPersistence, false)
  }

  fmt.Printf("Test: Single proposer ...\n")

  pxa[0].Start(0, "hello")
  waitn(t, pxa, 0, npaxos)

  fmt.Printf("  ... Passed\n")

  fmt.Printf("Test: Many proposers, same value ...\n")

  for i := 0; i < npaxos; i++ {
    pxa[Owner(1)].Start(1, 77)
  }
  waitn(t, pxa, 1, npaxos)

  fmt.Printf("  ... Passed\n")

  fmt.Printf("Test: Many proposers, different values ...\n")

  pxa[Owner(2)].Start(2, 100)
  pxa[Owner(2)].Start(2, 101)
  pxa[Owner(2)].Start(2, 102)
  waitn(t, pxa, 2, npaxos)

  fmt.Printf("  ... Passed\n")

  fmt.Printf("Test: Out-of-order instances ...\n")

  // change paxos instance being called, since Mencius has designated coordinators
  curr_group_size = 3
  match_value_flag = true
  pxa[Owner(7)].Start(7, 700)
  pxa[Owner(6)].Start(6, 600)
  pxa[Owner(5)].Start(5, 500)
  waitn(t, pxa, 7, npaxos)
  pxa[Owner(4)].Start(4, 400)
  pxa[Owner(3)].Start(3, 300)
  // flush skips
  pxa[Owner(8)].Start(8, 800)
  pxa[Owner(9)].Start(9, 900)
  pxa[Owner(10)].Start(10, 1000)
  waitn(t, pxa, 6, npaxos)
  waitn(t, pxa, 5, npaxos)
  waitn(t, pxa, 4, npaxos)
  waitn(t, pxa, 3, npaxos)
  match_value_flag = false

  if pxa[0].Max() < 7 {
    t.Fatalf("wrong Max()")
  }

  fmt.Printf("  ... Passed\n")
}

func TestForget(t *testing.T) {
  runtime.GOMAXPROCS(4)

  os.RemoveAll(fmt.Sprintf(db_path))
  os.Mkdir("db", 0777)

  curr_group_size = 6
  const npaxos = 6
  var pxa []*Paxos = make([]*Paxos, npaxos)
  var pxh []string = make([]string, npaxos)
  var uids[]int = make([]int, npaxos)


  defer cleanup(pxa)

  for i := 0; i < npaxos; i++ {
    pxh[i] = port("gc", i)
  }
  for i := 0; i < npaxos; i++ {
    uids[i] = int(common.Nrand())
    pxa[i] = Make(common.StringPaxos, pxh, i, nil, uids[i], needPersistence, false)
  }

  fmt.Printf("Test: Forgetting ...\n")

  // initial Min() correct?
  for i := 0; i < npaxos; i++ {
    m := pxa[i].Min()
    if m > 0 {
      t.Fatalf("wrong initial Min() %v", m)
    }
  }

  pxa[Owner(0)].Start(0, "00")
  pxa[Owner(1)].Start(1, "11")
  pxa[Owner(2)].Start(2, "22")
  pxa[Owner(6)].Start(6, "66")
  pxa[Owner(7)].Start(7, "77")

  waitn(t, pxa, 0, npaxos)

  // Min() correct?
  for i := 0; i < npaxos; i++ {
    m := pxa[i].Min()
    if m != 0 {
      t.Fatalf("wrong Min() %v; expected 0", m)
    }
  }

  waitn(t, pxa, 1, npaxos)

  // Min() correct?
  for i := 0; i < npaxos; i++ {
    m := pxa[i].Min()
    if m != 0 {
      t.Fatalf("wrong Min() %v; expected 0", m)
    }
  }

  // everyone Done() -> Min() changes?
  for i := 0; i < npaxos; i++ {
    pxa[i].Done(0)
  }
  for i := 1; i < npaxos; i++ {
    pxa[i].Done(1)
  }
  for i := 0; i < npaxos; i++ {
    pxa[Owner(8+i)].Start(8 + i, "xx")
    // these need to be done sequentially in Mencius,
    // otherwise a higher sequence that happens first
    // might preempt a skip which will delays Done propagation
    waitmajority(t, pxa, 8+i)
  }
  allok := false
  for iters := 0; iters < 12; iters++ {
    allok = true
    for i := 0; i < npaxos; i++ {
      s := pxa[i].Min()
      if s != 1 {
        allok = false
      }
    }
    if allok {
      break
    }
    time.Sleep(1 * time.Second)
  }
  if allok != true {
    t.Fatalf("Min() did not advance after Done()")
  }

  fmt.Printf("  ... Passed\n")
}

func TestManyForget(t *testing.T) {
  runtime.GOMAXPROCS(4)

  os.RemoveAll(fmt.Sprintf(db_path))
  os.Mkdir("db", 0777)

  curr_group_size = 3
  const npaxos = 3
  var pxa []*Paxos = make([]*Paxos, npaxos)
  var pxh []string = make([]string, npaxos)
  var uids []int = make([]int, npaxos)
  defer cleanup(pxa)

  for i := 0; i < npaxos; i++ {
    pxh[i] = port("manygc", i)
  }
  for i := 0; i < npaxos; i++ {
    uids[i] = int(common.Nrand())
    pxa[i] = Make(common.StringPaxos, pxh, i, nil, uids[i],  needPersistence, false)
    pxa[i].unreliable = true
  }

  fmt.Printf("Test: Lots of forgetting ...\n")

  const maxseq = 20
  done := false

  go func() {
    na := rand.Perm(maxseq)
    for i := 0; i < len(na); i++ {
      seq := na[i]
      //j := (rand.Int() % npaxos)
      v := rand.Int()
      pxa[Owner(seq)].Start(seq, v)
      runtime.Gosched()
    }
  }()

  go func() {
    for done == false {
      seq := (rand.Int() % maxseq)
      i := (rand.Int() % npaxos)
      if seq >= pxa[i].Min() {
        decided, _ := pxa[i].Status(seq)
        if decided {
          pxa[i].Done(seq)
        }
      }
      runtime.Gosched()
    }
  }()

  time.Sleep(5 * time.Second)
  done = true
  for i := 0; i < npaxos; i++ {
    pxa[i].unreliable = false
  }
  time.Sleep(2 * time.Second)

  for seq := 0; seq < maxseq; seq++ {
    for i := 0; i < npaxos; i++ {
      if seq >= pxa[i].Min() {
        pxa[i].Status(seq)
      }
    }
  }

  fmt.Printf("  ... Passed\n")
}

//
// does paxos forgetting actually free the memory?
//
func TestForgetMem(t *testing.T) {
  runtime.GOMAXPROCS(4)

  os.RemoveAll(fmt.Sprintf(db_path))
  os.Mkdir("db", 0777)
  fmt.Printf("Test: Paxos frees forgotten instance memory ...\n")

  curr_group_size = 3
  const npaxos = 3
  var pxa []*Paxos = make([]*Paxos, npaxos)
  var pxh []string = make([]string, npaxos)
  var uids[]int = make([]int, npaxos)
  defer cleanup(pxa)

  for i := 0; i < npaxos; i++ {
    pxh[i] = port("gcmem", i)
  }
  for i := 0; i < npaxos; i++ {
    uids[i] = int(common.Nrand())
    pxa[i] = Make(common.StringPaxos, pxh, i, nil, uids[i], needPersistence, false)
  }

  pxa[Owner(0)].Start(0, "x")
  waitn(t, pxa, 0, npaxos)

  runtime.GC()
  var m0 runtime.MemStats
  runtime.ReadMemStats(&m0)
  // m0.Alloc about a megabyte

  for i := 1; i <= 10; i++ {
    big := make([]byte, 1000000)
    for j := 0; j < len(big); j++ {
      big[j] = byte('a' + rand.Int() % 26)
    }
    pxa[Owner(i)].Start(i, string(big))
    waitn(t, pxa, i, npaxos)
  }

  runtime.GC()
  var m1 runtime.MemStats
  runtime.ReadMemStats(&m1)
  // m1.Alloc about 90 megabytes

  for i := 0; i < npaxos; i++ {
    pxa[i].Done(10)
  }
  for i := 0; i < npaxos; i++ {
    pxa[Owner(11+i)].Start(11 + i, "z")
  }
  time.Sleep(3 * time.Second)
  for i := 0; i < npaxos; i++ {
    if pxa[i].Min() != 11 {
      t.Fatalf("expected Min() %v, got %v\n", 11, pxa[i].Min())
    }
  }

  runtime.GC()
  var m2 runtime.MemStats
  runtime.ReadMemStats(&m2)
  // m2.Alloc about 10 megabytes

  if m2.Alloc > (m1.Alloc / 2) {
    t.Fatalf("memory use did not shrink enough")
  }

  fmt.Printf("  ... Passed\n")
}

func TestRPCCount(t *testing.T) {
  runtime.GOMAXPROCS(4)

  os.RemoveAll(fmt.Sprintf(db_path))
  os.Mkdir("db", 0777)
  fmt.Printf("Test: RPC counts aren't too high ...\n")

  curr_group_size = 3
  const npaxos = 3
  var pxa []*Paxos = make([]*Paxos, npaxos)
  var pxh []string = make([]string, npaxos)
  var uids[]int = make([]int, npaxos)
  defer cleanup(pxa)

  for i := 0; i < npaxos; i++ {
    pxh[i] = port("count", i)
  }
  for i := 0; i < npaxos; i++ {
    uids[i] = int(common.Nrand())
    pxa[i] = Make(common.StringPaxos, pxh, i, nil, uids[i], needPersistence, false)
  }

  ninst1 := 5
  seq := 0
  for i := 0; i < ninst1; i++ {
    pxa[Owner(seq)].Start(seq, "x")
    waitn(t, pxa, seq, npaxos)
    seq++
  }

  time.Sleep(2 * time.Second)

  total1 := 0
  for j := 0; j < npaxos; j++ {
    total1 += pxa[j].rpcCount
  }

  // per agreement:
  // 3 prepares
  // 3 accepts
  // 3 decides
  expected1 := ninst1 * npaxos * npaxos
  if total1 > expected1 {
    t.Fatalf("too many RPCs for serial Start()s; %v instances, got %v, expected %v",
      ninst1, total1, expected1)
  }

  ninst2 := 5
  for i := 0; i < ninst2; i++ {
    for j := 0; j < npaxos; j++ {
      go pxa[Owner(seq)].Start(seq, j + (i * 10))
    }
    waitn(t, pxa, seq, npaxos)
    seq++
  }

  time.Sleep(2 * time.Second)

  total2 := 0
  for j := 0; j < npaxos; j++ {
    total2 += pxa[j].rpcCount
  }
  total2 -= total1

  // worst case per agreement:
  // Proposer 1: 3 prep, 3 acc, 3 decides.
  // Proposer 2: 3 prep, 3 acc, 3 prep, 3 acc, 3 decides.
  // Proposer 3: 3 prep, 3 acc, 3 prep, 3 acc, 3 prep, 3 acc, 3 decides.
  expected2 := ninst2 * npaxos * 15
  if total2 > expected2 {
    t.Fatalf("too many RPCs for concurrent Start()s; %v instances, got %v, expected %v",
      ninst2, total2, expected2)
  }

  fmt.Printf("  ... Passed\n")
}

//
// many agreements (without failures)
//
func TestMany(t *testing.T) {
  runtime.GOMAXPROCS(4)

  os.RemoveAll(fmt.Sprintf(db_path))
  os.Mkdir("db", 0777)
  fmt.Printf("Test: Many instances ...\n")

  curr_group_size = 3
  const npaxos = 3
  var pxa []*Paxos = make([]*Paxos, npaxos)
  var pxh []string = make([]string, npaxos)
  var uids[]int = make([]int, npaxos)
  defer cleanup(pxa)

  for i := 0; i < npaxos; i++ {
    pxh[i] = port("many", i)
  }
  for i := 0; i < npaxos; i++ {
    uids[i] = int(common.Nrand())
    pxa[i] = Make(common.StringPaxos, pxh, i, nil, uids[i], needPersistence, false)
  }
  pxa[Owner(0)].Start(0, 0)

  const ninst = 50
  for seq := 1; seq < ninst; seq++ {
    // only 5 active instances, to limit the
    // number of file descriptors.
    for seq >= 5 && ndecided(t, pxa, seq - 5) < npaxos {
      time.Sleep(20 * time.Millisecond)
    }
    for i := 0; i < npaxos; i++ {
      pxa[Owner(seq)].Start(seq, (seq * 10) + i)
    }
  }

  for {
    done := true
    for seq := 1; seq < ninst; seq++ {
      if ndecided(t, pxa, seq) < npaxos {
        done = false
      }
    }
    if done {
      break
    }
    time.Sleep(100 * time.Millisecond)
  }

  fmt.Printf("  ... Passed\n")
}

//
// a peer starts up, with proposal, after others decide.
// then another peer starts, without a proposal.
// 
func TestOld(t *testing.T) {
  runtime.GOMAXPROCS(4)

  os.RemoveAll(fmt.Sprintf(db_path))
  os.Mkdir("db", 0777)
  fmt.Printf("Test: Minority proposal ignored ...\n")

  curr_group_size = 5
  const npaxos = 5
  var pxa []*Paxos = make([]*Paxos, npaxos)
  var pxh []string = make([]string, npaxos)
  var uids[]int = make([]int, npaxos)
  defer cleanup(pxa)

  for i := 0; i < npaxos; i++ {
    uids[i] = int(common.Nrand())
    pxh[i] = port("old", i)
  }

  pxa[1] = Make(common.StringPaxos, pxh, 1, nil, uids[1], needPersistence, false)
  pxa[2] = Make(common.StringPaxos, pxh, 2, nil, uids[2], needPersistence, false)
  pxa[3] = Make(common.StringPaxos, pxh, 3, nil, uids[3], needPersistence, false)
  pxa[Owner(1)].Start(1, 111)

  waitmajority(t, pxa, 1)

  pxa[0] = Make(common.StringPaxos, pxh, 0, nil, uids[0], needPersistence,false)
  // in Mencius, you call Revoke if it's on an instance
  // you don't own and you want to find out about
  //pxa[0].Start(1, 222)
  pxa[0].Revoke(1)

  waitn(t, pxa, 1, 4)

  if false {
    pxa[4] = Make(common.StringPaxos, pxh, 4, nil, uids[4], needPersistence,false)
    waitn(t, pxa, 1, npaxos)
  }

  fmt.Printf("  ... Passed\n")
}

//
// dueling suspicions/revokes.
// then dueling false suspicions with leader proposal.
// 
func TestRevoke(t *testing.T) {
  runtime.GOMAXPROCS(4)

  os.RemoveAll(fmt.Sprintf(db_path))
  os.Mkdir("db", 0777)
  fmt.Printf("Test: Concurrent revocations ...\n")

  curr_group_size = 5
  const npaxos = 5
  var pxa []*Paxos = make([]*Paxos, npaxos)
  var pxh []string = make([]string, npaxos)
  var uids[]int = make([]int, npaxos)
  defer cleanup(pxa)

  for i := 0; i < npaxos; i++ {
    pxh[i] = port("rev", i)
  }
  for i := 0; i < npaxos; i++ {
    uids[i] = int(common.Nrand())
    pxa[i] = Make(common.StringPaxos, pxh, i, nil, uids[i], needPersistence, false)
  }

  // dueling revokes
  pxa[1].Revoke(0)
  pxa[2].Revoke(0)
  pxa[3].Revoke(0)
  pxa[4].Revoke(0)

  waitmajority(t, pxa, 0)

  // single false suspicion duel
  pxa[0].Revoke(1)
  pxa[Owner(1)].Start(1,"LLL")

  waitmajority(t, pxa, 1)

  pxa[Owner(2)].Start(2,"qqq")
  pxa[0].Revoke(2)

  waitmajority(t, pxa, 2)

  pxa[0].Revoke(3)
  pxa[Owner(3)].Start(3,"www")

  waitmajority(t, pxa, 3)

  // multiple false suspicion duel
  pxa[0].Revoke(4)
  pxa[1].Revoke(4)
  pxa[2].Revoke(4)
  pxa[3].Revoke(4)
  pxa[Owner(4)].Start(4,"xxx")

  waitmajority(t, pxa, 4)

  pxa[Owner(5)].Start(5,"yyy")
  pxa[1].Revoke(5)
  pxa[2].Revoke(5)
  pxa[3].Revoke(5)
  pxa[4].Revoke(5)

  waitmajority(t, pxa, 5)

  pxa[0].Revoke(6)
  pxa[Owner(6)].Start(6,"zzz")
  pxa[2].Revoke(6)
  pxa[3].Revoke(6)
  pxa[4].Revoke(6)

  waitmajority(t, pxa, 6)

  pxa[0].Revoke(7)
  pxa[1].Revoke(7)
  pxa[Owner(7)].Start(7,"ttt")
  pxa[3].Revoke(7)
  pxa[4].Revoke(7)

  waitmajority(t, pxa, 7)

  pxa[0].Revoke(8)
  pxa[1].Revoke(8)
  pxa[2].Revoke(8)
  pxa[Owner(8)].Start(8,"uuu")
  pxa[4].Revoke(8)

  waitmajority(t, pxa, 8)

  pxa[0].Revoke(9)
  pxa[1].Revoke(9)
  pxa[2].Revoke(9)
  pxa[3].Revoke(9)
  pxa[Owner(9)].Start(9,"xxx")

  waitmajority(t, pxa, 9)

  pxa[Owner(10)].Start(10,"yyy")
  pxa[1].Revoke(10)
  pxa[2].Revoke(10)
  pxa[3].Revoke(10)
  pxa[4].Revoke(10)

  waitmajority(t, pxa, 10)

  pxa[0].Revoke(11)
  pxa[Owner(11)].Start(11,"zzz")
  pxa[2].Revoke(11)
  pxa[3].Revoke(11)
  pxa[4].Revoke(11)

  waitmajority(t, pxa, 11)

  pxa[0].Revoke(12)
  pxa[1].Revoke(12)
  pxa[Owner(12)].Start(12,"ttt")
  pxa[3].Revoke(12)
  pxa[4].Revoke(12)

  waitmajority(t, pxa, 12)

  pxa[0].Revoke(13)
  pxa[1].Revoke(13)
  pxa[2].Revoke(13)
  pxa[Owner(13)].Start(13,"uuu")
  pxa[4].Revoke(13)

  waitmajority(t, pxa, 13)

  fmt.Printf("  ... Passed\n")
}

//
// many agreements, with unreliable RPC
//
func TestManyUnreliable(t *testing.T) {
  runtime.GOMAXPROCS(4)

  os.RemoveAll(fmt.Sprintf(db_path))
  os.Mkdir("db", 0777)
  fmt.Printf("Test: Many instances, unreliable RPC ...\n")

  curr_group_size = 3
  const npaxos = 3
  var pxa []*Paxos = make([]*Paxos, npaxos)
  var pxh []string = make([]string, npaxos)
  var uids[]int = make([]int, npaxos)
  defer cleanup(pxa)

  // we need to ignore mismatches between No-ops and values, since
  // non-coordinating instances of Mencius may successfully revoke
  match_value_flag = true

  for i := 0; i < npaxos; i++ {
    pxh[i] = port("manyun", i)
  }
  for i := 0; i < npaxos; i++ {
    uids[i] = int(common.Nrand())
    pxa[i] = Make(common.StringPaxos, pxh, i, nil, uids[i], needPersistence, false)
    pxa[i].unreliable = true
    if Owner(0) == i {
      pxa[i].Start(0, 0)
    } else {
      pxa[i].Revoke(0)
    }
  }
  const ninst = 50
  for seq := 1; seq < ninst; seq++ {
    // only 3 active instances, to limit the
    // number of file descriptors.
    for seq >= 3 && ndecided(t, pxa, seq - 3) < npaxos {
      time.Sleep(20 * time.Millisecond)
    }
    for i := 0; i < npaxos; i++ {
      // in Mencius if you are not the owner, you are Revoking an instance
      if Owner(seq) == i {
        pxa[i].Start(seq, (seq * 10) + i)
      } else {
        pxa[i].Revoke(seq)
      }
    }
  }
  for {
    done := true
    for seq := 1; seq < ninst; seq++ {
      if ndecided(t, pxa, seq) < npaxos {
        done = false
      }
    }
    if done {
      break
    }
    time.Sleep(100 * time.Millisecond)
  }
  match_value_flag = false
  fmt.Printf("  ... Passed\n")
}

func pp(tag string, src int, dst int) string {
  s := "/var/tmp/824-"
  s += strconv.Itoa(os.Getuid()) + "/"
  s += "px-" + tag + "-"
  s += strconv.Itoa(os.Getpid()) + "-"
  s += strconv.Itoa(src) + "-"
  s += strconv.Itoa(dst)
  return s
}

func cleanpp(tag string, n int) {
  for i := 0; i < n; i++ {
    for j := 0; j < n; j++ {
      ij := pp(tag, i, j)
      os.Remove(ij)
    }
  }
}

func part(t *testing.T, tag string, npaxos int, p1 []int, p2 []int, p3 []int) {
  cleanpp(tag, npaxos)

  pa := [][]int{p1, p2, p3}
  for pi := 0; pi < len(pa); pi++ {
    p := pa[pi]
    for i := 0; i < len(p); i++ {
      for j := 0; j < len(p); j++ {
        ij := pp(tag, p[i], p[j])
        pj := port(tag, p[j])
        err := os.Link(pj, ij)
        if err != nil {
          // one reason this link can fail is if the
          // corresponding Paxos peer has prematurely quit and
          // deleted its socket file (e.g., called px.Kill()).
          t.Fatalf("os.Link(%v, %v): %v\n", pj, ij, err)
        }
      }
    }
  }
}

func TestPartition(t *testing.T) {
  runtime.GOMAXPROCS(4)

  os.RemoveAll(fmt.Sprintf(db_path))
  os.Mkdir("db", 0777)
  tag := "partition"
  curr_group_size = 5
  const npaxos = 5
  var pxa []*Paxos = make([]*Paxos, npaxos)
  defer cleanup(pxa)
  defer cleanpp(tag, npaxos)
  var uids[]int = make([]int, npaxos)

  for i := 0; i < npaxos; i++ {
    var pxh []string = make([]string, npaxos)
    uids[i] = int(common.Nrand())
    for j := 0; j < npaxos; j++ {
      if j == i {
        pxh[j] = port(tag, i)
      } else {
        pxh[j] = pp(tag, i, j)
      }
    }
    pxa[i] = Make(common.StringPaxos, pxh, i, nil,  uids[i],needPersistence, false)
  }
  defer part(t, tag, npaxos, []int{}, []int{}, []int{})

  seq := 1

  fmt.Printf("Test: No decision if partitioned ...\n")

  part(t, tag, npaxos, []int{0,2}, []int{1,3}, []int{4})
  pxa[Owner(seq)].Start(seq, 111)
  checkmax(t, pxa, seq, 0)
  
  fmt.Printf("  ... Passed\n")

  fmt.Printf("Test: Decision in majority partition ...\n")

  part(t, tag, npaxos, []int{0}, []int{1,2,3}, []int{4})
  time.Sleep(2 * time.Second)
  waitmajority(t, pxa, seq)

  fmt.Printf("  ... Passed\n")

  fmt.Printf("Test: All agree after full heal ...\n")
  // "poking" is calling revoke in Mencius
  pxa[0].Revoke(seq)
  pxa[4].Revoke(seq)
  //pxa[0].Start(seq, 1000) // poke them
  //pxa[4].Start(seq, 1004)
  part(t, tag, npaxos, []int{0,1,2,3,4}, []int{}, []int{})

  waitn(t, pxa, seq, npaxos)

  fmt.Printf("  ... Passed\n")

  fmt.Printf("Test: One peer switches partitions ...\n")

  // To test this in Mencius, the sequence numbers
  // for server 0 to start must be ones it owns
  seq = npaxos
  for iters := 0; iters < 20; iters++ {
    seq = seq + npaxos

    part(t, tag, npaxos, []int{0,1,2}, []int{3,4}, []int{})
    pxa[0].Start(seq, seq * 10)
    // Both 3/4 need to be poked, since there is no decision
    // propagation if a decision has already been reached
    pxa[3].Revoke(seq)
    pxa[4].Revoke(seq)
    //pxa[0].Start(seq, seq * 10)
    //pxa[3].Start(seq, (seq * 10) + 1)
    waitmajority(t, pxa, seq)
    if ndecided(t, pxa, seq) > 3 {
      t.Fatalf("too many decided")
    }
    part(t, tag, npaxos, []int{0,1}, []int{2,3,4}, []int{})
    waitn(t, pxa, seq, npaxos)
  }

  fmt.Printf("  ... Passed\n")

  fmt.Printf("Test: One peer switches partitions, unreliable ...\n")
  
  // Since the revocations might win duels (thereby setting an
  // instance's value to no-op) we must ignore value checks
  match_value_flag = true
  for iters := 0; iters < 20; iters++ {
    seq++

    for i := 0; i < npaxos; i++ {
      pxa[i].unreliable = true
    }

    part(t, tag, npaxos, []int{0,1,2}, []int{3,4}, []int{})
    for i := 0; i < npaxos; i++ {
      if i == Owner(seq) {
        pxa[i].Start(seq, (seq * 10) + i)
      } else {
        pxa[i].Revoke(seq)
      }
    }
    waitn(t, pxa, seq, 3)
    if ndecided(t, pxa, seq) > 3 {
      t.Fatalf("too many decided")
    }
    
    part(t, tag, npaxos, []int{0,1}, []int{2,3,4}, []int{})

    for i := 0; i < npaxos; i++ {
      pxa[i].unreliable = false
    }

    waitn(t, pxa, seq, 5)
  }
  match_value_flag = false

  fmt.Printf("  ... Passed\n")
}

func TestLots(t *testing.T) {
  runtime.GOMAXPROCS(4)

  fmt.Printf("Test: Many requests, changing partitions ...\n")

  // for revocation calls
  match_value_flag = true

  tag := "lots"
  curr_group_size = 5
  const npaxos = 5
  var pxa []*Paxos = make([]*Paxos, npaxos)
  var uids[]int = make([]int, npaxos)
  defer cleanup(pxa)
  defer cleanpp(tag, npaxos)

  for i := 0; i < npaxos; i++ {
    var pxh []string = make([]string, npaxos)
    uids[i] = int(common.Nrand())
    for j := 0; j < npaxos; j++ {
      if j == i {
        pxh[j] = port(tag, i)
      } else {
        pxh[j] = pp(tag, i, j)
      }
    }
    pxa[i] = Make(common.StringPaxos, pxh, i, nil,  uids[i], needPersistence, false)
    pxa[i].unreliable = true
  }
  defer part(t, tag, npaxos, []int{}, []int{}, []int{})

  done := false

  // re-partition periodically
  ch1 := make(chan bool)
  go func() {
    defer func(){ ch1 <- true }()
    for done == false {
      var a [npaxos]int
      for i := 0; i < npaxos; i++ {
        a[i] = (rand.Int() % 3)
      }
      pa := make([][]int, 3)
      for i := 0; i < 3; i++ {
        pa[i] = make([]int, 0)
        for j := 0; j < npaxos; j++ {
          if a[j] == i {
            pa[i] = append(pa[i], j)
          }
        }
      }
      part(t, tag, npaxos, pa[0], pa[1], pa[2])
      time.Sleep(time.Duration(rand.Int63() % 200) * time.Millisecond)
    }
  }()

  seq := 0

  // periodically start a new instance
  ch2 := make(chan bool)
  go func () {
    defer func() { ch2 <- true } ()
    for done == false {
      // how many instances are in progress?
      nd := 0
      for i := 0; i < seq; i++ {
        if ndecided(t, pxa, i) == npaxos {
          nd++
        }
      }
      if seq - nd < 10 {
        for i := 0; i < npaxos; i++ {
          if i == Owner(seq) {
            pxa[i].Start(seq, rand.Int() % 10)
          } else {
            pxa[i].Revoke(seq)
          }
        }
        seq++
      }
      time.Sleep(time.Duration(rand.Int63() % 300) * time.Millisecond)
    }
  }()

  // periodically check that decisions are consistent
  ch3 := make(chan bool)
  go func() {
    defer func() { ch3 <- true }()
    for done == false {
      for i := 0; i < seq; i++ {
        ndecided(t, pxa, i)
      }
      time.Sleep(time.Duration(rand.Int63() % 300) * time.Millisecond)
    }
  }()

  time.Sleep(20 * time.Second)
  done = true
  <- ch1
  <- ch2
  <- ch3

  // repair, then check that all instances decided.
  for i := 0; i < npaxos; i++ {
    pxa[i].unreliable = false
  }
  part(t, tag, npaxos, []int{0,1,2,3,4}, []int{}, []int{})
  time.Sleep(5 * time.Second)

  for i := 0; i < seq; i++ {
    waitmajority(t, pxa, i)
  }
  match_value_flag = false

  fmt.Printf("  ... Passed\n")
}
