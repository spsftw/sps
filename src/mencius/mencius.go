package mencius


//
// Paxos library, to be included in an application.
// Multiple applications will run, each including
// a Paxos peer.
//
// Manages a sequence of agreed-on values.
// The set of peers is fixed.
// Copes with network failures (partition, msg loss, &c).
// Does not store anything persistently, so cannot handle crash+restart.p
//
// The application interface:
//
// px = paxos.Make(peers []string, me string)
// px.Start(seq int, v interface{}) -- coordinator wants to suggest an instance
// px.Status(seq int) (decided bool, v interface{}) -- get info about an instance
// px.Done(seq int) -- ok to forget all instances <= seq
// px.Max() int -- highest instance seq known, or -1
// px.Min() int -- instances before this seq have been forgotten
// px.Revoke(seq int) -- non-coordinator wants to revoke seq
//

import "net"
import "net/rpc"
import "log"
import "os"
import "syscall"
import "sync"
import "fmt"
import "math/rand"
import "math"
import "common"
import "persistence"
import "strconv"
//import "time"

type Paxos struct {
  mu sync.Mutex
  l net.Listener
  dead bool
  unreliable bool
  rpcCount int
  peers []string
  me int // index into peers[]

  NoOp interface{} // no-op value
  paxos_type string
  num_peers int
  in_proposal map[int]bool
  instances map[int]common.MenciusState // Persist
  need_to_skip map[int]map[int]bool // Persist
  done int // Persist
  dones []int // Persist
  min int // Persist
  max int // Persist
  uid int

  // On piazza, TA says no need to persist states for shardmaster paxos group
  // so we have this boolean to turn persistency on and off
  needPersistence bool
  store *persistence.MenciusStore
}

// Your structs here.
const (
  OK = "OK"
  ErrReject = "ErrReject"
  ErrDecided = "ErrDecided"
)

const (
  NoOp = "NoOp"
)

type Err string
type PrepArgs struct {
  Instance int
  N int64
}
type PrepReply struct {
  Err Err
  N_A int64
  V_A interface{}
}
type AcceptArgs struct {
  Instance int
  N int64
  V interface{}
  SkipMap map[int]bool
}
type AcceptReply struct {
  Err Err
  N int64
  DecidedValue interface{}
  Done int
  SkipMap map[int]bool
}
type DecidedArgs struct {
  Instance int
  V interface{}
}
type DecidedReply struct {
}

// Your handlers here.
func (px *Paxos) TransferDB (args *common.TransferPaxosDBArgs, reply *common.TransferPaxosDBReply) error {
  px.mu.Lock()
  defer px.mu.Unlock()
  log.Printf("%v Paxos Begin DB transfer", px.uid)
  reply.NeedToSkip = px.need_to_skip
  reply.Instances = px.instances
  reply.Max = px.max
  reply.Min = px.min
  reply.Done = px.done
  reply.Dones = px.dones
  log.Printf("%v Paxos FINISHES DB transfer", px.uid)
  return nil
}

// This is the traditional Paxos prepare handler
func (px *Paxos) Prep(args *PrepArgs, reply *PrepReply) error {
  px.mu.Lock()
  defer px.mu.Unlock()
  px.CheckMax(args.Instance)
  px.CheckInit(args.Instance)
  n := args.N
  s := px.instances[args.Instance]
  if n > s.N_p {
    s.N_p = n
    if(px.needPersistence){
       px.store.StateStore(args.Instance,  &s)
       //fmt.Println(err)
    }
    px.instances[args.Instance] = s
    reply.N_A = s.N_a
    reply.V_A = s.V_a
    reply.Err = OK
    //fmt.Printf("S%+v sending prepare to %+v for seq %+v, replyNA %+v, replyVA %+v, replyErr %+v\n",px.me,px.Owner(int(args.N)),args.Instance,reply.N_A,reply.V_A,reply.Err)
  } else {
    reply.N_A = s.N_a
    reply.V_A = s.V_a
    reply.Err = ErrReject
  }
  return nil
}

// This is the Mencius propose handler
func (px *Paxos) Accept(args *AcceptArgs, reply *AcceptReply) error {
  px.mu.Lock()
  defer px.mu.Unlock()
  // regardless of the outcome of this suggest,
  // piggyback our done value on the response
  reply.Done = px.done
  px.CheckInit(args.Instance)
  // optimization 2: process piggybacked skip messages on suggest message
  if px.Owner(args.Instance) != px.me {
    //fmt.Printf("S%+v processing skipmap (suggest msg) %+v: %+v\n",px.me,px.Owner(args.Instance),args.SkipMap)
    for i, v := range args.SkipMap {
      if v == true {
        px.CheckInit(i)
        s := px.instances[i]
        s.DecidedValue = px.NoOp
        if px.needPersistence {
          px.store.StateStore(i,  &s)
        }
        px.instances[i] = s
      }
    }
  }

  d := px.instances[args.Instance].DecidedValue
  if d != nil {
    // send learn(learned) to q
    reply.Err = ErrDecided
    reply.DecidedValue = d
    return nil
  }

  px.CheckMax(args.Instance)
  n := args.N
  v := args.V
  s := px.instances[args.Instance]
  if n >= s.N_p && n > s.N_a {
    // p accepts (n, v)
    if n == 0 {
      // call skip on unused instances
      counter := px.done
      for {
        if counter >= args.Instance {
          break
        }
        if px.Owner(counter) == px.me && px.instances[counter].DecidedValue == nil &&
           px.in_proposal[counter] == false {
          px.CheckInit(counter)
          s := px.instances[counter]
          s.DecidedValue = px.NoOp
          if px.needPersistence {
            px.store.StateStore(counter, &s)
          }
          px.instances[counter] = s
          for i, _ := range px.peers {
            if i != px.me {
              m := px.need_to_skip[i]
              m[counter] = true
            }
          }
          if px.needPersistence{
            px.store.NeedToSkipStore(px.need_to_skip)
          }
        }
        counter++
      }
    }
    // optimization 1: piggyback skip messages to q on the accept message
    q := px.Owner(args.Instance)
    reply.SkipMap = make(map[int]bool)
    skip := reply.SkipMap
    for i, v := range px.need_to_skip[q] {
      //fmt.Printf("S%+v sending skipmap (accept reply) to %+v: %+v\n",px.me,q,px.need_to_skip[q])
      skip[i] = v
      delete(px.need_to_skip[q],i)
      if px.needPersistence{
         px.store.NeedToSkipStore(px.need_to_skip)
      }

    }

    s.N_p = n
    s.N_a = n
    s.V_a = v
    //fmt.Printf("S%+v sending accept to %+v for seq %+v, s.N_p %+v, s.N_a %+v, s.V_a %+v\n",px.me,px.Owner(int(s.N_p)),args.Instance,s.N_p,s.N_a,s.V_a)
    if px.needPersistence {
      px.store.StateStore(args.Instance,  &s)
    }
    px.instances[args.Instance] = s
    reply.N = n
    reply.Err = OK
  } else {
    reply.Err = ErrReject
  }
  return nil
}

// This is the decided handler
func (px *Paxos) Decided(args *DecidedArgs, reply *DecidedReply) error {
  px.mu.Lock()
  defer px.mu.Unlock()
  //fmt.Printf("%+v S%+v Decided: %+v, %+v\n",px.peers,px.me,args.Instance,args.V)
  px.CheckMax(args.Instance)
  px.CheckInit(args.Instance)
  s := px.instances[args.Instance]
  s.DecidedValue = args.V
  if px.needPersistence {
    px.store.StateStore(args.Instance,  &s)
  }
  px.instances[args.Instance] = s
  return nil
}

//
// call() sends an RPC to the rpcname handler on server srv
// with arguments args, waits for the reply, and leaves the
// reply in reply. the reply argument should be a pointer
// to a reply structure.
//
// the return value is true if the server responded, and false
// if call() was not able to contact the server. in particular,
// the replys contents are only valid if call() returned true.
//
// you should assume that call() will time out and return an
// error after a while if it does not get a reply from the server.
//
// please use call() to send all RPCs, in client.go and server.go.
// please do not change this function.
//
func call(srv string, name string, args interface{}, reply interface{}) bool {
  c, err := rpc.Dial("unix", srv)
  if err != nil {
    err1 := err.(*net.OpError)
    if err1.Err != syscall.ENOENT && err1.Err != syscall.ECONNREFUSED {
      fmt.Printf("paxos Dial() failed: %v\n", err1)
    }
    return false
  }
  defer c.Close()

  err = c.Call(name, args, reply)
  if err == nil {
    return true
  }

  fmt.Println(err)
  return false
}


//
// the application wants paxos to start agreement on
// instance seq, with proposed value v.
// Start() returns right away; the application will
// call Status() to find out if/when agreement
// is reached.
//
// for Mencius, start should be called with a sequence
// number that the paxos instance owns, otherwise start
// will return and ignore the request; the correct way
// to "usurp" the instance's owner is to use px.Revoke
//
func (px *Paxos) Start(seq int, v interface{}) {
  // fmt.Printf("S%+v Start called: seq %+v, v %+v\n",px.me,seq,v)
  // check ownership in start, ignore if not owner
  if px.Owner(seq) != px.me {
    fmt.Printf("Server %+v is not the owner of instance %+v\n", px.me, seq)
    return
  }
  //if seq < px.Min() {
    //return;
  //}
  // coordinator suggests v
  go px.Propose(seq,v)
}

//
// the application wants paxos to revoke instance seq
//
func (px *Paxos) Revoke(seq int) {
//fmt.Printf("S%+v Revoke called: seq %+v\n",px.me,seq)
  if px.Owner(seq) == px.me {
    fmt.Printf("Server %+v is the owner of attempted revoke on instance %+v\n", px.me, seq)
    return
  }
  go px.Prepare(seq)
}

// non-coordinator wants to propose no-op
func (px *Paxos) Prepare(seq int) {
  px.mu.Lock()
  // being proposed already?
  kill_this := false
  if px.in_proposal[seq] {
    kill_this = true
  } else {
    px.in_proposal[seq] = true
  }
  px.mu.Unlock()
  if kill_this {
    return
  }
  px.CheckMax(seq)
  px.mu.Lock()
  px.CheckInit(seq)
  s := px.instances[seq]
  px.mu.Unlock()
  n := s.N_p
  for {
    if px.Owner(int(n)) == px.me && n > s.N_p && n > s.N_a {
      break
    }
    n++
  }
  //prepare_iteration := 0
  // while not decided
  for {
  //prepare_iteration++
  //fmt.Printf("S%+v prepare iteration %+v for seq %+v, n %+v\n",px.me,prepare_iteration,seq,n)
    var highest_ballot int64 = -1
    d, _ := px.Status(seq)
    if d {
      break
    }
    if px.dead {
      break
    }
    // send prepare(n) to all servers including self
    args := PrepArgs{seq,n}
    num := 0
    var high_na int64
    var high_va interface{}
    for i, v := range px.peers {
      ok := false
      reply := PrepReply{}
      if i == px.me {
        px.Prep(&args,&reply)
        ok = true
      } else {
        ok = call(v,"Paxos.Prep",&args,&reply)
      }
      //fmt.Printf("s%+v called prepare (n %+v) on %+v, %+v, %+v, %+v, %+v\n",px.me,n,i,ok,reply.N_A,reply.V_A,reply.Err)
      //fmt.Printf("s%+v, received prepare from %+v, %+v, %+v, %+v\n",px.me,i,ok,reply.N_A,reply.V_A)
      if ok && reply.Err == OK {
        num++
        if reply.N_A >= high_na {
          high_na = reply.N_A
          high_va = reply.V_A
        }
      } else if ok && reply.Err == ErrReject {
        if highest_ballot < reply.N_A {
          highest_ballot = reply.N_A
        }
      }
    }
    //fmt.Printf("S%+v (non-coordinator) received prepares from %+v servers for seq %+v\n",px.me,num,seq)
    // if prepare_ok(n_a, v_a) from majority
    if num >= int(math.Floor(float64(len(px.peers))/2)+1) {
      //fmt.Printf("S%+v accept iteration %+v for seq %+v\n",px.me,accept_iteration,seq)
      // v' = v_a with highest n_a; choose no-op otherwise
      vp := high_va
      if high_va == nil {
         vp = px.NoOp
      }
      // send accept(n, v') to all
      args := AcceptArgs{}
      args.Instance = seq
      args.N = n
      args.V = vp
      num := 0
      for i, v := range px.peers {
        ok := false
        reply := AcceptReply{}
        if i == px.me {
          px.Accept(&args,&reply)
          ok = true
        } else {
          ok = call(v,"Paxos.Accept",&args,&reply)
        }
        //fmt.Printf("s%+v, n: %+v, return from accept to %+v, %+v, %+v, %+v\n",px.me,n,i,ok,reply.Err,reply.N)
        if ok && reply.Err == OK && reply.N == n {
          num++
        } else if ok && reply.Err == ErrDecided {
          px.mu.Lock()
          s := px.instances[seq]
          s.DecidedValue = reply.DecidedValue
          if px.needPersistence {
            px.store.StateStore(args.Instance, &s)
          }
          px.instances[seq] = s
          px.mu.Unlock()
        }
      }
      //fmt.Printf("S%+v (non-coordinator) received accepts from %+v servers for seq %+v\n",px.me,num,seq)
      // if accept_ok(n) from majority
      if num >= int(math.Floor(float64(len(px.peers))/2)+1) {
        // send decided(v') to all
        args := DecidedArgs{seq,vp}
        for i, v := range px.peers {
          reply := DecidedReply{}
          if i == px.me {
            px.Decided(&args,&reply)
          } else {
            _ = call(v,"Paxos.Decided",&args,&reply)
          }
        }
      }
    }

    // increment n in case of duel
    if(n < highest_ballot) {
      n = highest_ballot
    }
    n++
    for {
      if px.Owner(int(n)) == px.me {
        break
      }
      n++
    }
  }
}

// coordinator proposes value v
func (px *Paxos) Propose(seq int, v interface{}) {
  //fmt.Printf("S%+v data: %+v\n",px.me,px.instances)
  px.mu.Lock()
  // being proposed
  kill_this := false
  if px.in_proposal[seq] {
    kill_this = true
  } else {
    px.in_proposal[seq] = true
  }
  px.mu.Unlock()
  if kill_this {
     return
  }
  //fmt.Printf("S%+v inside propose: %+v, %+v\n",px.me,seq,v)
  iteration := 0
  px.CheckMax(seq)
  px.mu.Lock()
  px.CheckInit(seq)
  px.mu.Unlock()
  n := int64(0)
  // while not decided
  for {
    iteration++ // # of times in this loop
    normal_paxos := false // use normal paxos (two phase) for this iteration?
    failed_normal_prepare := false // failed first phase normal paxos?
    vp := v // value to suggest
    //fmt.Printf("S%+v propose iteration %+v for seq %+v\n",px.me,iteration,seq)
    if iteration > 1 {
      // there is a duel with a revocation, revert to regular Paxos
      //fmt.Printf("S%+v duel! seq %+v iteration %+v\n",px.me,seq,iteration)
      normal_paxos = true
    }
    d, _ := px.Status(seq)
    if d {
      break
    }
    if px.dead {
      break
    }
    if normal_paxos {
      // send prepare(n) to all servers including self
      args := PrepArgs{seq,n}
      num := 0
      var high_na int64
      var high_va interface{}
      for i, v := range px.peers {
        ok := false
        reply := PrepReply{}
        if i == px.me {
          px.Prep(&args,&reply)
          ok = true
        } else {
          ok = call(v,"Paxos.Prep",&args,&reply)
        }
        if ok && reply.Err == OK {
          num++
          if reply.N_A >= high_na {
            high_na = reply.N_A
            high_va = reply.V_A
          }
        }
      }
      // fmt.Printf("S%+v (coordinator) received prepares from %+v servers for seq %+v\n",px.me,num,seq)
      // if prepare_ok(n_a, v_a) from majority
      if num >= int(math.Floor(float64(len(px.peers))/2)+1) {
        // v' = v_a with highest n_a; choose own v otherwise
        if high_va != nil {
          vp = high_va
        }
      } else {
          failed_normal_prepare = true
      }
    }

    if !failed_normal_prepare {
      // send accept(n, v) to all
      args := AcceptArgs{}
      args.Instance = seq
      args.N = n
      args.V = vp
      num := 0
      for i, v := range px.peers {
        // fmt.Printf("S%+v, seq %+v, SENDING ACCEPT TO S%+v\n",px.me,seq,i)
        // optimization 2: p uses suggest message to indicate the instances that p has skipped
        args.SkipMap = make(map[int]bool)
        for j, v := range px.need_to_skip[i] {
          // fmt.Printf("S%+v sending skipmap (suggest msg) to %+v: %+v\n",px.me,i,px.need_to_skip[i])
          args.SkipMap[j] = v
          delete(px.need_to_skip[i],j)
        }
        ok := false
        reply := AcceptReply{}
        // fmt.Printf("s%+v going to call %+v for %+v, %+v\n",px.me,i,seq,v)
        if i == px.me {
          px.Accept(&args,&reply)
          ok = true
        } else {
          ok = call(v,"Paxos.Accept",&args,&reply)
          // fmt.Printf("(SUGGEST) s%+v returning from accept call to %+v for %+v, %+v, results: %+v,%+v\n",px.me,i,seq,v,ok,reply.Err)
        }
        if ok && reply.Err == OK && reply.N == n {
          num++
        } else if ok && reply.Err == ErrDecided {
          px.mu.Lock()
          s := px.instances[seq]
          s.DecidedValue = reply.DecidedValue
          if px.needPersistence {
            px.store.StateStore(seq,  &s)
          }
          px.instances[seq] = s
          px.mu.Unlock()
        }
        if ok {
          //log.Printf("px.dones %v", px.dones)
          px.dones[i] = reply.Done
          // fmt.Printf("S%+v received done from %+v: %+v\n",px.me,i,reply.Done)
        } else {
          // in case unreliable RPC, we need to make sure
          // px.Min() doesn't advance because of 0s
          px.dones[i] = -1
        }
        // optimization 1: process piggybacked skip messages
        if i != px.me {
          // fmt.Printf("S%+v processing skipmap (returned accept msg) %+v: %+v\n",px.me,i,reply.SkipMap)
          for j, v := range reply.SkipMap {
            if v == true {
              px.mu.Lock()
              px.CheckInit(j)
              s := px.instances[j]
              s.DecidedValue = px.NoOp
              if px.needPersistence {
                px.store.StateStore(j,  &s)
              }
              px.instances[j] = s
              px.mu.Unlock()
            }
          }
        }
      }
      // fmt.Printf("S%+v Dones: %+v\n",px.me,px.dones)
      m := int(^uint(0) >> 1)
      for _,d := range px.dones {
        if d < m {
          m = d
        }
      }
      if m > px.min {
        //store the min info
        if px.needPersistence{
          px.store.MinStore(m)
        }
        px.min = m
        for k, _ := range px.instances {
          if k <= m {
            delete(px.instances, k)
          }
        }
      }
      // fmt.Printf("S%+v, dones: %+v px.Min(): %+v\n",px.me,px.dones,px.Min())
      // fmt.Printf("S%+v (coordinator) received accepts from %+v servers for seq %+v\n",px.me,num,seq)
      // if accept_ok(n) from majority
      if num >= int(math.Floor(float64(len(px.peers))/2)+1) {
        // send decided(v) to all
        args := DecidedArgs{seq,vp}
        for i, v := range px.peers {
          reply := DecidedReply{}
          if i == px.me {
            px.Decided(&args,&reply)
          } else {
            _ = call(v,"Paxos.Decided",&args,&reply)
          }
        }
      }
    }
    // increment n in case of duel with false suspicion
    n = n + int64(len(px.peers))
  }
}

func (px *Paxos) CheckMax(seq int) {
  if seq > px.max {
    //store the max info
    if px.needPersistence {
      px.store.MaxStore(seq)
    }
    px.max = seq
  }
}

//
// the application on this machine is done with
// all instances <= seq.
//
// see the comments for Min() for more explanation.
//
func (px *Paxos) Done(seq int) {
  if seq > px.done {
    if px.needPersistence {
      px.store.DoneStore(seq)
    }
    px.done = seq
  }
}

//
// the application wants to know the
// highest instance sequence known to
// this peer.
//
func (px *Paxos) Max() int {
  return px.max
}

func (px *Paxos) SetMax(max int) {
  px.max = max
}

//
// Min() should return one more than the minimum among z_i,
// where z_i is the highest number ever passed
// to Done() on peer i. A peers z_i is -1 if it has
// never called Done().
//
// Paxos is required to have forgotten all information
// about any instances it knows that are < Min().
// The point is to free up memory in long-running
// Paxos-based servers.
//
// Paxos peers need to exchange their highest Done()
// arguments in order to implement Min(). These
// exchanges can be piggybacked on ordinary Paxos
// agreement protocol messages, so it is OK if one
// peers Min does not reflect another Peers Done()
// until after the next instance is agreed to.
//
// The fact that Min() is defined as a minimum over
// *all* Paxos peers means that Min() cannot increase until
// all peers have been heard from. So if a peer is dead
// or unreachable, other peers Min()s will not increase
// even if all reachable peers call Done. The reason for
// this is that when the unreachable peer comes back to
// life, it will need to catch up on instances that it
// missed -- the other peers therefor cannot forget these
// instances.
// 
func (px *Paxos) Min() int {
  return px.min+1
}

//
// the application wants to know whether this
// peer thinks an instance has been decided,
// and if so what the agreed value is. Status()
// should just inspect the local peer state;
// it should not contact other Paxos peers.
//
func (px *Paxos) Status(seq int) (bool, interface{}) {
  //fmt.Printf("S%+v PX.MIN: %+v\n",px.me,px.Min())
  px.mu.Lock()
  v := px.instances[seq].DecidedValue
  px.mu.Unlock()
//  log.Printf("%v Status Seq %d V %v px.Min() %v", px.uid, seq, v, px.Min())
  if v != nil && seq >= px.Min() {
    return true, v
  }
  return false, nil
}

//
// tell the peer to shut itself down.
// for testing.
// please do not change this function.
//
func (px *Paxos) Kill() {
  px.dead = true
  if px.l != nil {
    px.l.Close()
  }
  if px.store != nil {
    px.store.Kill()
  }
}

//
// returns the owner (index of the server 
// in peers[]) of given instance number
//
func (px *Paxos) Owner(instance int) (int) {
  return instance % px.num_peers
}

//
// are we the owner?
//
func (px *Paxos) IsOwner(instance int) (bool) {
  if (instance % px.num_peers) == px.me {
    return true
  }
  return false
}

//
// get next set of nil instances with same owner
// processed - the last committed op
// instance - to lookup owner
// size - # of entries
//
/*func (px *Paxos) NilInstances(processed int, instance int, size int) ([]int) {
  var set []int
  owner := px.Owner(instance)
  start := processed + 1
  curr := start
  for {
    if owner == px.Owner(curr) {
      break
    }
    curr++
  }
  for {
    if (curr - start) > size {
      break
    }
    set = append(set, curr)
    curr = curr + px.num_peers
  }
  return set
}*/

//
// ensures state's n_a is properly
// initialized to -1 as required in Mencius
// note: the code surrounding a call to
// CheckInit should be in px.mu.Lock().
//
func (px *Paxos) CheckInit(seq int) {
  s := px.instances[seq]
  if s.Init == false {
    s.N_a = -1
    s.Init = true
    if px.needPersistence {
        px.store.StateStore(seq,  &s)
    }
    px.instances[seq] = s
  }
}

func(px *Paxos) HasDB() bool{
  if _, err := os.Stat("./db/s"+ strconv.Itoa(px.uid) +"_menciusstore.db"); os.IsNotExist(err) {
    fmt.Printf("no such file or directory: %s", "s"+ strconv.Itoa(px.uid) +"_menciusstore.db")
    return false
  }
  return true
}
func (px *Paxos) getDB(r *common.TransferPaxosDBReply) {
  transferDBArgs := common.TransferPaxosDBArgs{}
  ok := false
  for !px.dead && !ok{
     for _, s := range px.peers {
        ok = call(s, "Paxos.TransferDB", &transferDBArgs, r)
        if ok {
           break
        }
     }
  }
}
func(px *Paxos) LoadFromDB(){
  need_to_skip, err := px.store.NeedToSkipGet()
  if err == "DbOk" {
    px.need_to_skip = need_to_skip
  }
  instances, err := px.store.StateGetAll()
  if err == "DbOk"{
    px.instances = instances
  }
  max, err := px.store.MaxGet()
  if err == "DbOk" {
    px.max = max
  }
  min, err := px.store.MinGet()
  if err == "DbOk" {
    px.min = min
  }
  done, err := px.store.DoneGet()
  if err == "DbOk" {
    px.done = done
  }
  dones, err := px.store.DonesGet()
  if err == "DbOk" {
    px.dones = dones
  }
}

// for benchmarking
func(px *Paxos) GetRPCCount() int {
  return px.rpcCount
}
func(px *Paxos) ResetRPCCount() {
  px.rpcCount = 0
}
func(px *Paxos) SetUnreliable(unreliable bool) {
  px.unreliable = unreliable
}

//
// the application wants to create a paxos peer.
// the ports of all the paxos peers (including this one)
// are in peers[]. this servers port is peers[me].
//
func Make(paxosType string, peers []string, me int, rpcs *rpc.Server, uid int, needPersistence bool, restart bool) *Paxos {
  px := &Paxos{}
  px.peers = peers
  px.me = me
  px.uid = uid

  // Your initialization code here.
  px.num_peers = len(peers)
  px.instances = make(map[int]common.MenciusState)
  px.max = 0
  px.done = -1
  px.dones = make([]int, len(px.peers))
  px.min = -1
  px.in_proposal = make(map[int]bool)
  px.need_to_skip = make(map[int]map[int]bool)
  if paxosType == common.ShardKVPaxos {
    px.NoOp = common.ShardKVOp{}
  } else if paxosType == common.ShardMasterPaxos {
    px.NoOp = common.ShardMasterOp{}
  } else {
    px.NoOp = "NoOp"
  }
  px.paxos_type = paxosType
  px.needPersistence = needPersistence
  for i, _ := range px.peers {
    px.need_to_skip[i] = make(map[int]bool)
  }


  if needPersistence {
    hasDB := px.HasDB()
    if hasDB {
      log.Printf("%d Paxos restarted, found its disk", px.uid)
      store, err := persistence.MakeMenciusStore("s"+ strconv.Itoa(px.uid), paxosType, false)
      if err == "DbOk" {
        fmt.Println("Store started")
        px.store = store
      } else{
        fmt.Println("Failed to start store ", err)
      }
      px.LoadFromDB()
    } else {
      if restart {
        // lost disk
        log.Printf("%d Paxos has lost its disk", px.uid)
        r := &common.TransferPaxosDBReply{}
        px.getDB(r)
        px.need_to_skip = r.NeedToSkip
        px.instances = r.Instances
        px.max = r.Max
        px.min = r.Min
        px.done = r.Done
        log.Printf("r %v", r)
  log.Printf("px.dones %v", px.dones)
        if r.Err != common.ErrNoKey {
          log.Printf("@@@@@@@@@@@@")
           px.dones = r.Dones
        }
  log.Printf("px.dones %v", px.dones)
        r.Dones = px.dones
        store, err := persistence.MakeMenciusStore("s"+ strconv.Itoa(px.uid), paxosType, true)
        if err == "DbOk" {
          fmt.Println("Store started")
          px.store = store
        } else{
          fmt.Println("Failed to start store ", err)
        }
        err = px.store.NeedToSkipStore(r.NeedToSkip)
        if err != common.DbOk { panic("oops") }
        err = px.store.StateStoreAll(r.Instances)
        if err != common.DbOk { panic("oops") }
        err = px.store.MaxStore(r.Max)
        if err != common.DbOk { panic("oops") }
        err = px.store.MinStore(r.Min)
        if err != common.DbOk { panic("oops") }
        err = px.store.DoneStore(r.Done)
        if err != common.DbOk { panic("oops") }
        err =px.store.DonesStore(r.Dones)
        if err != common.DbOk { panic("oops") }
      } else {
        // normal start
        store, err := persistence.MakeMenciusStore("s"+ strconv.Itoa(px.uid), paxosType, true)
        if err == "DbOk" {
          fmt.Println("Store started")
          px.store = store
        } else{
          fmt.Println("Failed to start store ", err)
        }
      }
    }
  }

  if rpcs != nil {
    // caller will create socket &c
    rpcs.Register(px)
  } else {
    rpcs = rpc.NewServer()
    rpcs.Register(px)

    // prepare to receive connections from clients.
    // change "unix" to "tcp" to use over a network.
    os.Remove(peers[me]) // only needed for "unix"
    l, e := net.Listen("unix", peers[me]);
    if e != nil {
      log.Fatal("listen error: ", e);
    }
    px.l = l

    // please do not change any of the following code,
    // or do anything to subvert it.

    // create a thread to accept RPC connections
    go func() {
      for px.dead == false {
        conn, err := px.l.Accept()
        if err == nil && px.dead == false {
          if px.unreliable && (rand.Int63() % 1000) < 100 {
            // discard the request.
            conn.Close()
          } else if px.unreliable && (rand.Int63() % 1000) < 200 {
            // process the request but force discard of reply.
            c1 := conn.(*net.UnixConn)
            f, _ := c1.File()
            err := syscall.Shutdown(int(f.Fd()), syscall.SHUT_WR)
            if err != nil {
              fmt.Printf("shutdown: %v\n", err)
            }
            px.rpcCount++
            go rpcs.ServeConn(conn)
          } else {
            px.rpcCount++
            go rpcs.ServeConn(conn)
          }
        } else if err == nil {
          conn.Close()
        }
        if err != nil && px.dead == false {
          fmt.Printf("Paxos(%v) accept: %v\n", me, err.Error())
        }
      }
    }()
  }
  return px
}
