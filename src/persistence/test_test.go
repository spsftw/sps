package persistence

import "testing"
import "fmt"
import "strconv"
import "common"
import "os"
import _ "github.com/mattn/go-sqlite3"

func setup() {
  os.RemoveAll("./db/")
  os.Mkdir("db", 0777)
}

func cleanup() {
//  os.RemoveAll("./db/")
}

func TestKVReqPersistence(t *testing.T) {
  setup()
  defer cleanup()
  fmt.Printf("\nTest: Test KvReqStore Persistence ...\n")
  name := "s1"
  kvreqdb, err := MakeKvReqStore(name, true)
  if err != "DbOk" {
    t.Fatalf("Cannot create database %v\n", name)
  }

  var shardid int

  // Writing to the KvStore and ReqStore
  shardid = 0
  for sn := 0; sn < 10; sn++ {
    kvreqdb.KvStore(strconv.Itoa(sn), shardid, fmt.Sprintf("Test1%d", sn))
    reply := &common.KVReply{Cmd: "Get", Sn: int64(sn), Err: "Err", Value: strconv.Itoa(sn), PreviousValue: strconv.Itoa(sn)}
    kvreqdb.ReqStore(shardid, reply)
    shardid += 1
    if shardid >= 2 {
      shardid = 0
    }
  }

  // Writing to the Metadata
  kvreqdb.MDataStore(int(13))

  // Close the database
  kvreqdb.Kill()

  // Re-open the database
  kvreqdb, err = MakeKvReqStore(name, false)
  if err != "DbOk" {
    t.Fatalf("Cannot open existing database %v\n", name)
  }

  fmt.Println("\nUpdating old key value in the database")

  shardid = 10
  for key := 0; key < 10; key++ {
    // Writing to KvStore
    kvreqdb.KvStore(strconv.Itoa(key), shardid, fmt.Sprintf("Update%d", key))
    // Writing to ReqStore
    reply := &common.KVReply{Cmd: "PutHash", Sn: int64(10+key), Err: "Ok", Value: strconv.Itoa(10+key), PreviousValue: strconv.Itoa(10+key)}
    kvreqdb.ReqStore(shardid, reply)
    shardid += 1
    if shardid >= 12 {
      shardid = 10
    }
  }

  // Writing to the Metadata
  kvreqdb.MDataStore(int(23))

  count := 0
  // Reading from the KvStore
  for shardid = 10; shardid < 12; shardid++ {
    data, _ := kvreqdb.KvReadAll(shardid)
    for key, value := range data {
      count += 1
      if fmt.Sprintf("Update%v", key) != value {
        t.Fatalf("KvStore does not get updated; key: %v, shardid: %v, value: %v\n", key, value, shardid)
      }
      fmt.Printf("shardid: %v, key: %v, value: %v\n", shardid, key, value)
    }
  }
  if count != 10 {
    t.Fatalf("KvStore should have had 10 data; had %v instead\n", count)
  }
  fmt.Println("")

  // Reading from the ReqStore
  count = 0
  for shardid = 10; shardid < 12; shardid++ {
    data, _ := kvreqdb.ReqReadAll(shardid)
    for sn, reply := range data {
      count += 1
      if ((reply.Cmd != "PutHash") || (reply.Err != "Ok") || (reply.Value != strconv.Itoa(int(sn))) ||
          (reply.PreviousValue != strconv.Itoa(int(sn)))) {
        t.Fatalf("ReqStore does not get updated; shardid: %v, sn: %v, reply: %v\n", shardid, sn, reply)
      }
      fmt.Printf("shardid: %v, sn: %v, reply: %v\n", shardid, sn, reply)
    }
  }
  if count != 10 {
    t.Fatalf("ReqStore should have had 10 data; had %v instead\n", count)
  }
  fmt.Println("")

  // Reading from the Metadata
  lastSeq, _ := kvreqdb.MDataGetValue()
  if lastSeq != 23 {
    t.Fatalf("Metadata does not get updated; expect: 13, get: %v", lastSeq)
  }
  fmt.Printf("Metadata lastSeq: %v\n", lastSeq)

  os.Remove(fmt.Sprintf("./db/%v_kvstore.db", name))
  fmt.Printf("\n  ... Passed\n")
}

func TestKVReqBlockRead(t *testing.T) {
  setup()
  defer cleanup()
  fmt.Printf("\nTest: Test KvReqStore Block Read ...\n")
  name := "blockRead"
  kvreqdb, err := MakeKvReqStore(name, true)
  if err != "DbOk" {
    t.Fatalf("Cannot create database %v\n", name)
  }

  dataCount := 24
  var shardid int

  // Writing to the KvStore and ReqStore
  shardid = 0
  for sn := 0; sn < dataCount; sn++ {
    kvreqdb.KvStore(strconv.Itoa(sn), shardid, fmt.Sprintf("Test1%d", sn))
    reply := &common.KVReply{Cmd: "Get", Sn: int64(sn), Err: "Err", Value: strconv.Itoa(sn), PreviousValue: strconv.Itoa(sn)}
    kvreqdb.ReqStore(shardid, reply)
    shardid += 1
    if shardid >= 2 {
      shardid = 0
    }
  }

  limit := 4

  // Reading from the KvStore
  kvCount, _ := kvreqdb.KvGetCount()
  if kvCount != dataCount {
    t.Fatalf("KvStore should have had %v data; had %v instead\n", dataCount, kvCount)
  }
  for shardid = 0; shardid < 2; shardid++ {
    for offset := 0; offset < dataCount/2; offset += limit {
      data, _ := kvreqdb.KvReadBlock(shardid, limit, offset)
      fmt.Printf("\nReading KVStore starting from offset %v to %v\n", offset, limit+offset-1);
      count := 0
      for key, value := range data {
        count += 1
        if fmt.Sprintf("Test1%v", key) != value {
          t.Fatalf("KvStore does not have the correct value; key: %v, shardid: %v, value: %v\n", key, value, shardid)
        }
        fmt.Printf("shardid: %v, key: %v, value: %v\n", shardid, key, value)
      }
      if offset + limit < dataCount/2 {
        if count != limit {
          t.Fatalf("KvStore should have had %v data; had %v instead\n", limit, count)
        }
      }
    }
  }
  fmt.Println("")

  // Reading from the ReqStore
  reqCount, _ := kvreqdb.KvGetCount()
  if reqCount != dataCount {
    t.Fatalf("ReqStore should have had %v data; had %v instead\n", dataCount, reqCount)
  }

  limit = 7
  for shardid = 0; shardid < 2; shardid++ {
    for offset := 0; offset < dataCount/2; offset += limit {
      fmt.Printf("\nReading KVStore starting from offset %v to %v\n", offset, limit+offset-1);
      data, _ := kvreqdb.ReqReadBlock(shardid, limit, offset)
      count := 0
      for sn, reply := range data {
        count += 1
        if ((reply.Cmd != "Get") || (reply.Err != "Err") || (reply.Value != strconv.Itoa(int(sn))) ||
            (reply.PreviousValue != strconv.Itoa(int(sn)))) {
          t.Fatalf("ReqStore does not have the correct value; shardid: %v, sn: %v, reply: %v\n", shardid, sn, reply)
        }
        fmt.Printf("shardid: %v, sn: %v, reply: %v\n", shardid, sn, reply)
      }
      if offset + limit < dataCount/2 {
        if count != limit {
          t.Fatalf("ReqStore should have had %v data; had %v instead\n", limit, count)
        }
      }
    }
  }
  fmt.Println("")

  os.Remove(fmt.Sprintf("./db/%v_kvstore.db", name))
  fmt.Printf("\n  ... Passed\n")
}


func TestKVReqTransaction (t *testing.T) {
  // kvstore.go batch store
  setup()
  defer cleanup()

  fmt.Printf("\nTest: Test KvReqStore Transaction ...\n\n")
  name := "s183278342325"
  kvreqdb, err := MakeKvReqStore(name, true)
  if err != "DbOk" {
    t.Fatalf("Cannot create database \n")
  }

  kvMap := make(map[int]map[string]string)
  replyMap := make(map[int]map[int64]*common.KVReply)

  // Data to write to the KvStore and ReqStore
  shard1kvMap := make(map[string]string)
  shard1kvMap["aeru28"] = "3781"
  shard1kvMap["294237xjs"] = "jdf821"
  shard2kvMap := make(map[string]string)
  shard2kvMap["2212hsdfg9"] = "sjf81"
  shard2kvMap["91"] ="1302"
  shard3kvMap := make(map[string]string)
  shard3kvMap["12"] = "aa"
  shard4kvMap := make(map[string]string)
  kvMap[1] = shard1kvMap
  kvMap[2] = shard2kvMap
  kvMap[3] = shard3kvMap
  kvMap[4] = shard4kvMap

  kvMaps := []map[string]string {shard1kvMap, shard2kvMap,shard3kvMap, shard4kvMap}
  shard1replyMap := make(map[int64]*common.KVReply)
  shard2replyMap := make(map[int64]*common.KVReply)
  shard3replyMap := make(map[int64]*common.KVReply)
  shard4replyMap := make(map[int64]*common.KVReply)
  shard1replyMap[1343] = &common.KVReply{Cmd: "Get", Sn: 1343, Err: "Err", Value: "2234", PreviousValue: "2445"}
  shard1replyMap[21421043] = &common.KVReply{Cmd: "Get", Sn: 21421043, Err: "Err", Value: "hello", PreviousValue: "uuuuea"}
  shard1replyMap[21043] = &common.KVReply{Cmd: "Put", Sn: 21043, Err: "Err", Value: "helldfd", PreviousValue: "uuet"}
  shard2replyMap[103] = &common.KVReply{Cmd: "PutHash", Sn: 103, Err: "Err", Value: "lldfd", PreviousValue: "uuet"}
  shard3replyMap[9103] = &common.KVReply{Cmd: "PutHash", Sn: 9103, Err: "Err", Value: "lldfd", PreviousValue: "uuet"}
  replyMap[1] = shard1replyMap
  replyMap[2] = shard2replyMap
  replyMap[3] = shard3replyMap
  replyMap[4] = shard4replyMap

  replyMaps := [] map[int64] *common.KVReply {shard1replyMap, shard2replyMap, shard3replyMap, shard4replyMap}
//  fmt.Printf("KVMap %+v \n replyMap %+v", kvMap, replyMap)
  cfg := []byte{1, 2, 3, 4, 5}
  err = kvreqdb.BatchStore(kvMap, replyMap, 13, cfg)
  if err != "DbOk" {
     fmt.Println("Something wrong")
  }
  // Reading from the KvStore
  for shardid := 1; shardid <= 4; shardid++ {
    data, err := kvreqdb.KvReadAll(shardid)
    if err != "DbOk" {
       panic("reading data")
    }
    AssertKVMapEqual(data, kvMaps[shardid-1])
  }

  // Reading from the ReqStore
  for shardid := 1; shardid <= 4; shardid++ {
    data, err := kvreqdb.ReqReadAll(shardid)
    if err != "DbOk" {
       panic("reading data")
    }
    AssertReplyMapEqual(data, replyMaps[shardid-1])
  }

  // Reading from the Metadata
  metaValue, err := kvreqdb.MDataGetValue()
  if err != "DbOk" {
     panic("reading data")
  }
  if metaValue != 13 {
     panic("nooo")
  }
  fmt.Printf("\n  ... Passed\n")
}

func AssertKVMapEqual(a map[string]string, b map[string]string) {
   for mapKey, mapValue := range a {
       if b[mapKey] != mapValue {
          panic("nooo")
       }
   }
}

func AssertReplyMapEqual(a map[int64]*common.KVReply, b map[int64]*common.KVReply) {
  for mapKey, mapValue := range a {
      AssertKVReplyEqual(mapValue, b[mapKey])
  }
}

func AssertKVReplyEqual(a *common.KVReply, b *common.KVReply) {
  //fmt.Printf("a: %+v\n b: %+v \n", a, b)
  if a.Cmd != b.Cmd || a.Sn != b.Sn || a.Err != b.Err || a.Value != b.Value || a.PreviousValue != b.PreviousValue {
     panic("ooops")
  }
}

/*func TestKVReqTransactionAbort(t *testing.T) {
  // IMPORTANT: to run this test successfully, uncomment tx.Commit() in 
  // kvstore.go batch store
  
  fmt.Printf("\nTest: Test KvReqStore Transaction Abort ...\n\n")
  kvname := "kvdb"
  reqname := "reqdb"
  kvreqdb, err := MakeKvReqStore(kvname, reqname, true)
  if err != "DbOk" {
    t.Fatalf("Cannot create database %v and %v\n", kvname, reqname)
  }

  kvMap := make(map[int]map[string]string)
  replyMap := make(map[int]map[int64]*common.KVReply)

  var shardid int
  // Data to write to the KvStore and ReqStore
  shardid = 0
  for i := 0; i < 10; i++ {
    _, exist := kvMap[shardid]
    if !exist {
      kvMap[shardid] = make(map[string]string)
      replyMap[shardid] = make(map[int64]*common.KVReply)
    }

    kvMap[shardid][strconv.Itoa(i)] = fmt.Sprintf("TestTransaction%d", i)
    reply := &common.KVReply{Cmd: "Get", Sn: int64(i), Err: "Err", Value: strconv.Itoa(i), PreviousValue: strconv.Itoa(i)}
    replyMap[shardid][int64(i)] = reply

    shardid += 1
    if shardid >= 2 {
      shardid = 0
    }
  }
  kvreqdb.BatchStore(kvMap, replyMap, 13)
  
  // Reading from the KvStore
  for shardid = 0; shardid < 2; shardid++ {
    data, _ := kvreqdb.KvReadAll(shardid)
    if len(data) != 0 {
      t.Fatalf("KvStore should not have had anything in it; had %v data instead\n", len(data))
    }
    fmt.Printf("KvStore with shardid %v doesn't have anything in it as expected\n", shardid)
  }
  fmt.Println("")

  // Reading from the ReqStore
  for shardid = 10; shardid < 12; shardid++ {
    data, _ := kvreqdb.ReqReadAll(shardid)
    if len(data) != 0 {
      t.Fatalf("ReqStore should not have had anything in it; had %v data instead\n", len(data))
    }
    fmt.Printf("ReqStore with shardid %v doesn't have anything in it as expected\n", shardid)
  }
  fmt.Println("")

  // Reading from the Metadata
  _, dbError := kvreqdb.MDataGetValue()
  if dbError != "ErrDbNoKey" {
    t.Fatalf("Metadata should not have had anything in it")
  }
  fmt.Printf("Metadata doesn't have anything in it as expected\n")

  os.Remove(fmt.Sprintf("%v_%v.db", kvname, reqname))
  fmt.Printf("\n  ... Passed\n")
}*/


func TestMenciusMetaDataPersistence(t *testing.T) {
  setup()
  defer cleanup()
  fmt.Printf("\nTest: Test Mencius MetaData Persistence ...\n")
  name := "s1"
  menciusdb, err := MakeMenciusStore(name, common.ShardKVPaxos, true)
  if err != "DbOk" {
    t.Fatalf("Cannot create database %v\n", name)
  }
  n := make (map[int] map[int] bool)
  a := make (map[int] bool)
  b := make (map[int] bool)
  c := make (map[int] bool)
  d := make (map[int] bool)
  a[1837] = true
  a[281] = true
  a[192] = false
  a[102] = true
  b[108112] = true
  b[18271] = true
  b[112] = true
  c[129] = true
  n[1] = a
  n[0] = b
  n[2] = c
  n[3] = d
  menciusdb.NeedToSkipStore(n)
  if err != "DbOk" {
     t.Fatalf("Something's wrong")
  }
  newMap, err := menciusdb.NeedToSkipGet()
  if err != "DbOk" {
    t.Fatalf("Something's wrong")
  }
  fmt.Printf("Expected %v\n", newMap)
  fmt.Printf("Actual %v\n", n)
  if !AssertNeedToSkipEqual(n, newMap) {
    t.Fatalf("Something's wrong")
  }

  delete(n, 1)
  menciusdb.NeedToSkipDelete(1)
  newMap2, err := menciusdb.NeedToSkipGet()
  if err != "DbOk" {
    t.Fatalf("Something's wrong")
  }

  if !AssertNeedToSkipEqual(n, newMap2) {
    t.Fatalf("Something's wrong")
  }
  menciusdb.MinStore(2731)
  dones := []int{12,28340,1982,21922,1,0,12}
  menciusdb.DonesStore(dones)
  menciusdb.MinStore(231)
  menciusdb.MaxStore(21334)
  menciusdb.DoneStore(3592893)
  menciusdb.DeadStore(true)
  dead, err := menciusdb.DeadGet()
  if err != "DbOk"  || dead != true{
    t.Fatalf("Something's wrong")
  }
  min, err := menciusdb.MinGet()
  if err != "DbOk" {
    t.Fatalf("Something's wrong")
  }
  max, err := menciusdb.MaxGet()
  if err != "DbOk" {
    t.Fatalf("Something's wrong")
  }
  if max != 21334 {
    t.Fatalf("Something's wrong Expected Max: %d Actual %d", 21334, max)
  }
  if min != 231 {
    t.Fatalf("Something's wrong Expected Min: %d Actual %d", 213, min)
  }
  done, err := menciusdb.DoneGet()
  if err != "DbOk" {
    t.Fatalf("Something's wrong")
  }
  if done != 3592893 {
    t.Fatalf("Something's wrong Expected Done: %d Actual %d", 3593893, done)
  }
  newdones, err := menciusdb.DonesGet()
  if err != "DbOk" {
    t.Fatalf("Something's wrong")
  }

//  fmt.Printf("newDones %v\n", newdones)
  for i, d_s := range  dones {
      if d_s != newdones[i] {
         t.Fatalf("Something's wrong")
      }
  }
}

func AssertNeedToSkipEqual(a map[int] map[int] bool, b map[int] map[int] bool) bool {
   for mapKey, mapValue := range a {
      if !AssertNeedToSkipSubEqual(mapValue, b[mapKey]) {
         return false
      }
   }
   return true
}

func AssertNeedToSkipSubEqual( a map[int] bool, b map[int] bool) bool {
   for mapKey, mapValue := range a {
       if mapValue != b[mapKey] {
          return false
       }
   }
   return true
}
func TestMenciusStatePersistence(t *testing.T) {
  setup()
  defer cleanup()
  fmt.Printf("\nTest: Test Mencius State Persistence ...\n")
  name := "s1"
  menciusdb, err := MakeMenciusStore(name, common.ShardKVPaxos, true)
  if err != "DbOk" {
    t.Fatalf("Cannot create database %v\n", name)
  }
  d_1 := common.ShardKVOp{Cmd: "Get", Key: "h10shaa", ClientId: 19341901037, Sn: 273893921389}
  d_2 := common.ShardKVOp{Cmd: "Get", Key: "shaa", ClientId: 19341907, Sn: 273893921389}
  shards := [common.NShards]int64 {1973,100182,10291,1001,13,1244, 0101,3984,13213,131}
  groups := make (map[int64] []string)
  groups[1] = []string{"string", "hello", "world"}
  groups[2] = []string{"2838", "zzzlo", "wruuwd"}
  v_1 := common.ShardKVOp{Cmd: "Reconfig", ConfigNum: 132, Shards: shards, Groups: groups}
  state1 := common.MenciusState{N_p : 34, N_a: 20472, Init: true, DecidedValue: d_1, V_a: v_1}
  state2 := common.MenciusState{N_p : 4, N_a: 20472, Init: false, DecidedValue: d_2, V_a: v_1}
  menciusdb.StateStore(1, &state1)
  menciusdb.StateStore(2, &state2)
  stateFetched, err := menciusdb.StateGet(1)
  if err != "DbOk" {
     t.Fatalf("Something's wrong")
  }
  fmt.Printf("stateFetched %v\n", stateFetched)
  fmt.Printf("state %v\n", state1)

  if !AssertMenciusStateEqual(stateFetched, &state1) {
     fmt.Printf("Something's wrong")
  }
  m, err := menciusdb.StateGetAll()
  if err != "DbOk" {
     t.Fatalf("Something's wrong")
  }
  if !AssertMenciusStateEqual(m[1], &state1) || !AssertMenciusStateEqual(m[2], &state2) {
     t.Fatalf("Something's wrong")
  }
  fmt.Printf("map %v \n %v\n", m[1], m[2])
  // Close the database
  menciusdb.Kill()
}


func TestMenciusStatePersistenceAll(t *testing.T) {
  setup()
  defer cleanup()
  fmt.Printf("\nTest: Test Mencius State Persistence ...\n")
  name := "s1"
  menciusdb, err := MakeMenciusStore(name, common.ShardKVPaxos, true)
  if err != "DbOk" {
    t.Fatalf("Cannot create database %v\n", name)
  }
  d_1 := common.ShardKVOp{Cmd: "Get", Key: "h10shaa", ClientId: 19341901037, Sn: 273893921389}
  d_2 := common.ShardKVOp{Cmd: "Get", Key: "shaa", ClientId: 19341907, Sn: 273893921389}
  shards := [common.NShards]int64 {1973,100182,10291,1001,13,1244, 0101,3984,13213,131}
  groups := make (map[int64] []string)
  v_1 := common.ShardKVOp{Cmd: "Reconfig", ConfigNum: 132, Shards: shards, Groups: groups}
  state1 := common.MenciusState{N_p : 34, N_a: 20472, Init: true, DecidedValue: d_1, V_a: v_1}
  state2 := common.MenciusState{N_p : 4, N_a: 20472, Init: false, DecidedValue: d_2, V_a: v_1}
  m := make (map[int] common.MenciusState)
  m[1] = state1
  m[2] = state2
  menciusdb.StateStoreAll(m)
  mNew, err := menciusdb.StateGetAll()
  if err != "DbOk" {
     t.Fatalf("Something's wrong")
  }
//  fmt.Printf("m %v\n", m)
//  fmt.Printf("mNew %v\n", mNew)
  if !AssertMenciusStateEqual(mNew[1], &state1) || !AssertMenciusStateEqual(mNew[2], &state2) {
     t.Fatalf("Something's wrong")
  }
  // Close the database
  menciusdb.Kill()
}

func AssertMenciusStateEqual(a common.MenciusState, b *common.MenciusState) bool {
  return a.N_p == b.N_p && a.N_a == b.N_a && a.Init == b.Init && AssertShardKVOpEqual(a.DecidedValue, b.DecidedValue) && AssertShardKVOpEqual(a.V_a, b.V_a)
}

func AssertShardKVOpEqual(a interface{}, b interface{}) bool{
  ac, errConvert := a.(common.ShardKVOp)
  if errConvert != true {
     fmt.Printf("Something's wrong\n")
  }
  bc, errConvert := b.(common.ShardKVOp)
  if errConvert != true {
     fmt.Printf("Something's wrong\n")
  }

  return ac.Cmd == bc.Cmd && ac.Key == bc.Key && ac.ClientId == bc.ClientId && ac.Sn == bc.Sn && ac.Value == bc.Value && ac.ConfigNum == bc.ConfigNum && AssertShardsEqual(ac.Shards, bc.Shards) && AssertGroupsEqual(ac.Groups, bc.Groups)
}

func AssertGroupsEqual(a map[int64] []string, b map[int64] []string) bool {
//  fmt.Printf("a: %v \n b: %v\n", a, b)
  if len(a) != len(b) {
     return false
  }
  for mapkey, mapvalue := range a {
      if !AssertServersEqual(b[mapkey], mapvalue) {
         return false
      }
  }
  return true
}

func AssertShardsEqual(a [common.NShards] int64, b [common.NShards]int64) bool{
   for i, s := range a {
       if b[i] != s {
          // fmt.Printf("b[i] %v s %v", b[i], s)
          return false
       }
   }
   return true
}
func AssertServersEqual(a []string, b []string) bool {
   if len(a) != len(b) {
      return false
   }
   for i, s := range a {
       if b[i] != s {
          return false
       }
   }
   return true
}
