package persistence

import "database/sql"
import "fmt"
import _ "github.com/mattn/go-sqlite3"
import "log"
import "os"
import "common"

const (
  KVTable = "KVTable"
  ReqTable = "ReqTable"
  Config = "Config"
)

type KvReqStore struct {
  dead bool
  db *sql.DB

  // Prepared Stmts for K/V Storage
  kvStoreStmt *sql.Stmt
  kvGetValueStmt *sql.Stmt
  kvReadBlockStmt *sql.Stmt
  kvReadAllStmt *sql.Stmt
  kvDeleteAllStmt *sql.Stmt
  kvCountStmt *sql.Stmt

  // Prepared Stmts for Client's Request Storage
  reqStoreStmt *sql.Stmt
  reqGetValueStmt *sql.Stmt
  reqReadAllStmt *sql.Stmt
  reqReadBlockStmt *sql.Stmt
  reqDeleteAllStmt *sql.Stmt
  reqCountStmt *sql.Stmt

  mdataStoreStmt *sql.Stmt
  mdataGetValueStmt *sql.Stmt
  mdataDeleteAllStmt *sql.Stmt

  cfgStoreStmt *sql.Stmt
  cfgGetValueStmt *sql.Stmt
  cfgDeleteAllStmt *sql.Stmt
}


func MakeKvReqStore(name string, createNew bool) (*KvReqStore, common.DbError) {
  kvreqdb := new(KvReqStore)
  filename := fmt.Sprintf("./db/%v_kvstore.db", name)

  kvreqdb.dead = false
  if createNew {
    os.Remove(filename)
  }

  var err error
  kvreqdb.db, err = sql.Open("sqlite3", filename)
  if err != nil {
      log.Printf("%q: %s\n", err, filename)
    return kvreqdb, "ErrDbNoConnection"
  }
  if createNew {
    sql := fmt.Sprintf(`create table %v (key text not null primary key, shardid integer, value text); 
      delete from %v;`, KVTable, KVTable)

    _, err = kvreqdb.db.Exec(sql)
    if err != nil {
      log.Printf("%q: %s\n", err, sql)
      return kvreqdb, "ErrDbCreate"
    }

    sql = fmt.Sprintf(`create table %v (sn integer not null primary key, shardid integer, cmd text, err text, value text, prevValue text); 
      delete from %v;`, ReqTable, ReqTable)

    _, err = kvreqdb.db.Exec(sql)
    if err != nil {
      log.Printf("%q: %s\n", err, sql)
      return kvreqdb, "ErrDbCreate"
    }

    sql = `create table metadata (key integer not null primary key, lastseq integer); 
      delete from metadata;`

    _, err = kvreqdb.db.Exec(sql)
    if err != nil {
      log.Printf("%q: %s\n", err, sql)
      return kvreqdb, "ErrDbCreate"
    }

    sql = fmt.Sprintf(`create table %v (key integer not null primary key, cfg blob); 
      delete from %v;`, Config, Config)

    _, err = kvreqdb.db.Exec(sql)
    if err != nil {
      log.Printf("%q: %s\n", err, sql)
      return kvreqdb, "ErrDbCreate"
    }
  }

  /* 
  * KV STORAGE 
  */

  // Prepare query for Store method
  kvreqdb.kvStoreStmt, err = kvreqdb.db.Prepare(fmt.Sprintf("insert or replace into %v(key, shardid, value) values(?, ?, ?)", KVTable))
  if err != nil {
    log.Println(err)
    return kvreqdb, "ErrDbCreate"
  }

  // Prepare query for GetValue method
  kvreqdb.kvGetValueStmt, err = kvreqdb.db.Prepare(fmt.Sprintf("select value from %v where key = ?", KVTable))
  if err != nil {
    log.Println(err)
    return kvreqdb, "ErrDbCreate"
  }
  
  // Prepare query for ReadBlock method
  kvreqdb.kvReadBlockStmt, err = kvreqdb.db.Prepare(fmt.Sprintf("select key, value from %v where shardid = ? limit ? offset ?", KVTable))
  if err != nil {
    log.Println(err)
    return kvreqdb, "ErrDbCreate"
  }

  // Prepare query for ReadAll method
  kvreqdb.kvReadAllStmt, err = kvreqdb.db.Prepare(fmt.Sprintf("select key, value from %v where shardid = ?", KVTable))
  if err != nil {
    log.Println(err)
    return kvreqdb, "ErrDbCreate"
  }

  // Prepare query for DeleteAll method
  kvreqdb.kvDeleteAllStmt, err = kvreqdb.db.Prepare(fmt.Sprintf("delete from %v", KVTable))
  if err != nil {
    log.Println(err)
    return kvreqdb, "ErrDbCreate"
  }

  // Prepare query for GetRowCount method
  kvreqdb.kvCountStmt, err = kvreqdb.db.Prepare(fmt.Sprintf("select count(*) from %v", KVTable))
  if err != nil {
    log.Println(err)
    return kvreqdb, "ErrDbCreate"
  }

  /* 
  * CLIENT'S REQUEST STORAGE 
  */

  // Prepare query for Store method
  kvreqdb.reqStoreStmt, err = kvreqdb.db.Prepare(fmt.Sprintf("insert or replace into %v(sn, shardid, cmd, err, value, prevValue) values(?, ?, ?, ?, ?, ?)", ReqTable))
  if err != nil {
    log.Println(err)
    return kvreqdb, "ErrDbCreate"
  }

  // Prepare query for GetValue method
  kvreqdb.reqGetValueStmt, err = kvreqdb.db.Prepare(fmt.Sprintf("select cmd, err, value, prevValue from %v where sn = ?", ReqTable))
  if err != nil {
    log.Println(err)
    return kvreqdb, "ErrDbCreate"
  }

  // Prepare query for ReadBlock method
  kvreqdb.reqReadBlockStmt, err = kvreqdb.db.Prepare(fmt.Sprintf("select sn, cmd, err, value, prevValue from %v where shardid = ? limit ? offset ?", ReqTable))
  if err != nil {
    log.Println(err)
    return kvreqdb, "ErrDbCreate"
  }

  // Prepare query for ReadAll method
  kvreqdb.reqReadAllStmt, err = kvreqdb.db.Prepare(fmt.Sprintf("select sn, cmd, err, value, prevValue from %v where shardid = ?", ReqTable))
  if err != nil {
    log.Println(err)
    return kvreqdb, "ErrDbCreate"
  }

  // Prepare query for DeleteAll method
  kvreqdb.reqDeleteAllStmt, err = kvreqdb.db.Prepare(fmt.Sprintf("delete from %v", ReqTable))
  if err != nil {
    log.Println(err)
    return kvreqdb, "ErrDbCreate"
  }

  // Prepare query for GetRowCount method
  kvreqdb.reqCountStmt, err = kvreqdb.db.Prepare(fmt.Sprintf("select count(*) from %v", ReqTable))
  if err != nil {
    log.Println(err)
    return kvreqdb, "ErrDbCreate"
  }

  /* 
  * METADATA STORAGE 
  */

  // Prepare query for Store method
  kvreqdb.mdataStoreStmt, err = kvreqdb.db.Prepare("insert or replace into metadata(key, lastseq) values(?, ?)")
  if err != nil {
    log.Println(err)
    return kvreqdb, "ErrDbCreate"
  }

  // Prepare query for GetValue method
  kvreqdb.mdataGetValueStmt, err = kvreqdb.db.Prepare("select lastseq from metadata where key = ?")
  if err != nil {
    log.Println(err)
    return kvreqdb, "ErrDbCreate"
  }

  // Prepare query for DeleteAll method
  kvreqdb.mdataDeleteAllStmt, err = kvreqdb.db.Prepare("delete from metadata")
  if err != nil {
    log.Println(err)
    return kvreqdb, "ErrDbCreate"
  }

  /* 
  * CONFIG STORAGE
  */

  // Prepare query for Store method
  kvreqdb.cfgStoreStmt, err = kvreqdb.db.Prepare(fmt.Sprintf("insert or replace into %v(key, cfg) values(?, ?)",Config))
  if err != nil {
    log.Println(err)
    return kvreqdb, "ErrDbCreate"
  }

  // Prepare query for GetValue method
  kvreqdb.cfgGetValueStmt, err = kvreqdb.db.Prepare(fmt.Sprintf("select cfg from %v where key = ?",Config))
  if err != nil {
    log.Println(err)
    return kvreqdb, "ErrDbCreate"
  }

  // Prepare query for DeleteAll method
  kvreqdb.cfgDeleteAllStmt, err = kvreqdb.db.Prepare(fmt.Sprintf("delete from %v",Config))
  if err != nil {
    log.Println(err)
    return kvreqdb, "ErrDbCreate"
  }

  return kvreqdb, "DbOk"
}


////////////////////////////////////////////////////////////////////////////////
// KV Storage
////////////////////////////////////////////////////////////////////////////////

func (kvreqdb *KvReqStore) KvStore(key string, shardId int, value string) common.DbError {
  if (kvreqdb.dead) {
    log.Printf("%v KvStore: %v\n", KVTable, "ErrDbDead")
    return "ErrDbDead"
  }
  _, err := kvreqdb.kvStoreStmt.Exec(key, shardId, value)
  if err != nil {
    log.Printf("%v KvStore: %v\n", KVTable, err)
    return "ErrDbExec"
  }
  return "DbOk"
}

func (kvreqdb *KvReqStore) KvGetCount() (int, common.DbError) {
  value := -1
  if (kvreqdb.dead) {
    log.Printf("%v KvGetCount: %v\n", KVTable, "ErrDbDead")
    return value, "ErrDbDead"
  }
  err := kvreqdb.kvCountStmt.QueryRow().Scan(&value)
  if err != nil {
    log.Printf("%v KvGetCount: %v\n", KVTable, err)
    return value, "ErrDbGetCount"
  }
  return value, "DbOk"
}

func (kvreqdb *KvReqStore) KvGetValue(key string) (string, common.DbError) {
  if (kvreqdb.dead) {
    log.Printf("%v KvGetValue: %v\n", KVTable, "ErrDbDead")
    return "", "ErrDbDead"
  }
  value := ""
  err := kvreqdb.kvGetValueStmt.QueryRow(key).Scan(&value)
  if err != nil {
    // If the key is not in the database
    if err == sql.ErrNoRows {
      return value, "ErrDbNoKey"
    }
    log.Printf("%v KvGetValue: %v\n", KVTable, err)
    return value, "ErrDbQuery"
  }
  return value, "DbOk"
}

/*
* Get LIMIT number of data which belongs to shardid starting from row OFFSET 
*/
func (kvreqdb *KvReqStore) KvReadBlock(shardId int, limit int, offset int) (map[string]string, common.DbError) {
  data := make(map[string]string)
  if (kvreqdb.dead) {
    log.Printf("%v KvReadBlock: %v\n", KVTable, "ErrDbDead")
    return data, "ErrDbDead"
  }
  rows, err := kvreqdb.kvReadBlockStmt.Query(shardId, limit, offset)
  if err != nil {
    log.Printf("%v KvReadBlock: %v\n", KVTable, err)
    return data, "ErrDbQuery"
  }
  defer rows.Close()
  for rows.Next() {
    var key string
    var value string
    rows.Scan(&key, &value)
    data[key] = value
  }
  rows.Close()
  return data, "DbOk"
}

/*
* Get all data which belongs to shardid
*/
func (kvreqdb *KvReqStore) KvReadAll(shardId int) (map[string]string, common.DbError) {
  data := make(map[string]string)
  if (kvreqdb.dead) {
    log.Printf("%v KvReadAll: %v\n", KVTable, "ErrDbDead")
    return data, "ErrDbDead"
  }
  rows, err := kvreqdb.kvReadAllStmt.Query(shardId)
  if err != nil {
    log.Printf("%v KvReadAll: %v\n", KVTable, err)
    return data, "ErrDbQuery"
  }
  defer rows.Close()
  for rows.Next() {
    var key string
    var value string
    rows.Scan(&key, &value)
    data[key] = value
  }
  rows.Close()
  return data, "DbOk"
}


////////////////////////////////////////////////////////////////////////////////
// Client's Request Storage
////////////////////////////////////////////////////////////////////////////////

func (kvreqdb *KvReqStore) ReqStore(shardId int, reply *common.KVReply) common.DbError {
  if (kvreqdb.dead) {
    log.Printf("%v ReqStore: %v\n", ReqTable, "ErrDbDead")
    return "ErrDbDead"
  }
  _, err := kvreqdb.reqStoreStmt.Exec(reply.Sn, shardId, reply.Cmd, string(reply.Err), reply.Value, reply.PreviousValue)
  if err != nil {
    log.Printf("%v ReqStore: %v\n", ReqTable, err)
    return "ErrDbExec"
  }
  return "DbOk"
}

func (kvreqdb *KvReqStore) ReqGetCount() (int, common.DbError) {
  value := -1
  if (kvreqdb.dead) {
    log.Printf("%v ReqGetCount: %v\n", ReqTable, "ErrDbDead")
    return value, "ErrDbDead"
  }
  err := kvreqdb.reqCountStmt.QueryRow().Scan(&value)
  if err != nil {
    log.Printf("%v ReqGetCount: %v\n", ReqTable, err)
    return value, "ErrDbGetCount"
  }
  return value, "DbOk"
}

/*
* Get request with id == sn
*/
func (kvreqdb *KvReqStore) ReqGetValue(sn int64) (*common.KVReply, common.DbError) {
  reply := &common.KVReply{Sn: sn}
  if (kvreqdb.dead) {
    log.Printf("%v ReqGetValue: %v\n", ReqTable, "ErrDbDead")
    return reply, "ErrDbDead"
  }
  var errMsg string
  err := kvreqdb.reqGetValueStmt.QueryRow(sn).Scan(&reply.Cmd, &errMsg, &reply.Value, &reply.PreviousValue)
  reply.Err = common.Err(errMsg)
  if err != nil {
    // If there is no request with id sn in the database
    if err == sql.ErrNoRows {
      return reply, "ErrDbNoKey"
    }
    log.Printf("%v ReqGetValue: %v\n", ReqTable, err)
    return reply, "ErrDbQuery"
  }
  return reply, "DbOk"
}

/*
* Get LIMIT number of requests which belongs to shardid starting from row OFFSET 
*/
func (kvreqdb *KvReqStore) ReqReadBlock(shardId int, limit int, offset int) (map[int64]*common.KVReply, common.DbError) {
  data := make(map[int64]*common.KVReply)
  if (kvreqdb.dead) {
    log.Printf("%v ReqReadBlock: %v\n", ReqTable, "ErrDbDead")
    return data, "ErrDbDead"
  }
  rows, err := kvreqdb.reqReadBlockStmt.Query(shardId, limit, offset)
  if err != nil {
    log.Printf("%v ReqReadBlock: %v\n", ReqTable, err)
    return data, "ErrDbQuery"
  }
  defer rows.Close()
  for rows.Next() {
    var errMsg string
    reply := &common.KVReply{}
    rows.Scan(&reply.Sn, &reply.Cmd, &errMsg, &reply.Value, &reply.PreviousValue)
    reply.Err = common.Err(errMsg)
    data[reply.Sn] = reply
  }
  rows.Close()
  return data, "DbOk"
}

/*
* Get all requests which belongs to shardid
*/
func (kvreqdb *KvReqStore) ReqReadAll(shardId int) (map[int64]*common.KVReply, common.DbError) {
  data := make(map[int64]*common.KVReply)
  if (kvreqdb.dead) {
    log.Printf("%v ReqReadAll: %v\n", ReqTable, "ErrDbDead")
    return data, "ErrDbDead"
  }
  rows, err := kvreqdb.reqReadAllStmt.Query(shardId)
  if err != nil {
    log.Printf("%v ReqReadAll: %v\n", ReqTable, err)
    return data, "ErrDbQuery"
  }
  defer rows.Close()
  for rows.Next() {
    var errMsg string
    reply := &common.KVReply{}
    rows.Scan(&reply.Sn, &reply.Cmd, &errMsg, &reply.Value, &reply.PreviousValue)
    reply.Err = common.Err(errMsg)
    data[reply.Sn] = reply
  }
  rows.Close()
  return data, "DbOk"
}


////////////////////////////////////////////////////////////////////////////////
// Metadata Storage
////////////////////////////////////////////////////////////////////////////////

func (kvreqdb *KvReqStore) MDataStore(lastSeq int) common.DbError {
  if (kvreqdb.dead) {
    log.Printf("MDataStore: %v\n", "ErrDbDead")
    return "ErrDbDead"
  }
  _, err := kvreqdb.mdataStoreStmt.Exec(0, lastSeq)
  if err != nil {
    log.Printf("MDataStore: %v\n", err)
    return "ErrDbExec"
  }
  return "DbOk"
}

func (kvreqdb *KvReqStore) MDataGetValue() (int, common.DbError) {
  lastSeq := -1
  if (kvreqdb.dead) {
    log.Printf("MdataGetValue: %v\n", "ErrDbDead")
    return lastSeq, "ErrDbDead"
  }
  err := kvreqdb.mdataGetValueStmt.QueryRow(0).Scan(&lastSeq)
  if err != nil {
    // If there is no request with id sn in the database
    if err == sql.ErrNoRows {
      return lastSeq, "ErrDbNoKey"
    }
    log.Printf("MdataGetValue: %v\n", err)
    return lastSeq, "ErrDbQuery"
  }
  return lastSeq, "DbOk"
}

////////////////////////////////////////////////////////////////////////////////
// Config Storage
////////////////////////////////////////////////////////////////////////////////

func (kvreqdb *KvReqStore) ConfigStore(config []byte) common.DbError {
  if (kvreqdb.dead) {
    log.Printf("ConfigStore: %v\n", "ErrDbDead")
    return "ErrDbDead"
  }
  _, err := kvreqdb.cfgStoreStmt.Exec(0, config)
  if err != nil {
    log.Printf("ConfigStore: %v\n", err)
    return "ErrDbExec"
  }
  return "DbOk"
}

func (kvreqdb *KvReqStore) ConfigGetValue() ([]byte, common.DbError) {
  cfg := []byte{}
  if (kvreqdb.dead) {
    log.Printf("ConfigGetValue: %v\n", "ErrDbDead")
    return cfg, "ErrDbDead"
  }
  err := kvreqdb.cfgGetValueStmt.QueryRow(0).Scan(&cfg)
  if err != nil {
    // If there is no request with id 0 in the database
    if err == sql.ErrNoRows {
      return cfg, "ErrDbNoKey"
    }
    log.Printf("ConfigGetValue: %v\n", err)
    return cfg, "ErrDbQuery"
  }
  return cfg, "DbOk"
}

////////////////////////////////////////////////////////////////////////////////
// IMPORTANT: NEED TO KILL BEFORE TERMINATING THE SERVER
////////////////////////////////////////////////////////////////////////////////

func (kvreqdb *KvReqStore) BatchStore(kvMap map[int]map[string]string, replyMap map[int]map[int64]*common.KVReply, lastSeq int, encodedConfig []byte) common.DbError {
  if (kvreqdb.dead) {
    log.Printf("%v and %v BatchStore: %v\n", KVTable, ReqTable, "ErrDbDead")
    return "ErrDbDead"
  }

  // Begin transaction for batch store
  tx, err := kvreqdb.db.Begin()
  if err != nil {
    log.Printf("%v and %v BatchStore: %v\n", KVTable, ReqTable, err)
    return "ErrDbBeginTx"
  }

  // IMPORTANT: Cannot used prepared statements here since prepared stmt has automatic
  // begin and commit
  // Writing data for KvStore first
  kvmapStmt, err := tx.Prepare("insert or replace into KVTable(key, shardid, value) values(?, ?, ?)")
  defer kvmapStmt.Close()
  for shardId, keyValue := range kvMap {
    for key, value := range keyValue {
      _, err = kvmapStmt.Exec(key, shardId, value)
      if err != nil {
         log.Fatal("Writing KVMap in BatchStore err: %v", err)
      }
    }
  }

  replymapStmt, err := tx.Prepare("insert or replace into ReqTable(sn, shardid, cmd, err, value, prevValue) values(?, ?, ?, ?, ?, ?)")
  defer replymapStmt.Close()
  // Next, writing data from the replyMap
  for shardId, snReply := range replyMap {
    for _, reply := range snReply {
      _, err = replymapStmt.Exec(reply.Sn, shardId, reply.Cmd, string(reply.Err), reply.Value, reply.PreviousValue)
      if err != nil {
         log.Fatal("Writing ReplyMap in BatchStore err: %v", err)
      }
    }
  }

  // Record the metadata
  metaStmt, err := tx.Prepare("insert or replace into metadata(key, lastseq) values(?, ?)")

  defer metaStmt.Close()
  _, err = metaStmt.Exec(0, lastSeq)
  if err != nil {
     log.Fatal("Writing metadata in BatchStore err: %v", err)
  }

  // Config
  cfgStmt, err := tx.Prepare("insert or replace into config(key, cfg) values(?, ?)")

  defer cfgStmt.Close()
  _, err = cfgStmt.Exec(0, encodedConfig)
  if err != nil {
     log.Fatal("Writing config in BatchStore err: %v", err)
  }

  // Commit transaction
  err = tx.Commit()
  if err != nil {
    log.Printf("%v and %v BatchStore: %v\n", KVTable, ReqTable, err)
    return "ErrDbTxCommit"
  }
  return "DbOk"
}

func (kvreqdb *KvReqStore) DeleteAll() common.DbError {
  if (kvreqdb.dead) {
    log.Printf("%v and %v DeleteAll: %v\n", KVTable, ReqTable, "ErrDbDead")
    return "ErrDbDead"
  }
  _, err := kvreqdb.kvDeleteAllStmt.Exec()
  if err != nil {
    log.Printf("%v DeleteAll: %v\n", KVTable, err)
    return "ErrDbExec"
  }
  _, err = kvreqdb.reqDeleteAllStmt.Exec()
  if err != nil {
    log.Printf("%v DeleteAll: %v\n", ReqTable, err)
    return "ErrDbExec"
  }
  _, err = kvreqdb.mdataDeleteAllStmt.Exec()
  if err != nil {
    log.Printf("Metadata DeleteAll: %v\n", err)
    return "ErrDbExec"
  }
  _, err = kvreqdb.cfgDeleteAllStmt.Exec()
  if err != nil {
    log.Printf("%v DeleteAll: %v\n", Config, err)
    return "ErrDbExec"
  }
  return "DbOk"
}

func (kvreqdb *KvReqStore) Kill() {
  kvreqdb.dead = true

  kvreqdb.kvStoreStmt.Close()
  kvreqdb.kvGetValueStmt.Close()
  kvreqdb.kvReadBlockStmt.Close()   
  kvreqdb.kvReadAllStmt.Close() 
  kvreqdb.kvDeleteAllStmt.Close() 
  kvreqdb.kvCountStmt.Close() 

  kvreqdb.reqStoreStmt.Close()
  kvreqdb.reqGetValueStmt.Close()
  kvreqdb.reqReadBlockStmt.Close() 
  kvreqdb.reqReadAllStmt.Close() 
  kvreqdb.reqDeleteAllStmt.Close() 
  kvreqdb.reqCountStmt.Close() 

  kvreqdb.mdataStoreStmt.Close()
  kvreqdb.mdataGetValueStmt.Close()
  kvreqdb.mdataDeleteAllStmt.Close() 

  kvreqdb.cfgStoreStmt.Close()
  kvreqdb.cfgGetValueStmt.Close()
  kvreqdb.cfgDeleteAllStmt.Close() 

  kvreqdb.db.Close()
}
