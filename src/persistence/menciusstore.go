package persistence

import "database/sql"
import "fmt"
import _ "github.com/mattn/go-sqlite3"
import "log"
import "os"
import "common"
import "strings"
import "strconv"
import "encoding/gob"
import "bytes"

const (
  StateTable = "StateTable"
  DoneTable = "DoneTable"
  DonesTable = "DonesTable"
  MinTable = "MinTable"
  MaxTable = "MaxTable"
  NeedToSkipTable = "NeedToSkipTable"
  DeadTable = "DeadTable"
)

type MenciusStore struct {
  dead bool
  db *sql.DB
  paxosType string
  stateStoreStmt *sql.Stmt
  stateGetStmt *sql.Stmt
  stateGetAllStmt *sql.Stmt
  deadStoreStmt *sql.Stmt
  deadGetStmt *sql.Stmt
  metaStoreStmt *sql.Stmt
  metaGetStmt *sql.Stmt
  doneStoreStmt *sql.Stmt
  doneGetStmt *sql.Stmt
  donesStoreStmt *sql.Stmt
  donesGetStmt *sql.Stmt
  minStoreStmt *sql.Stmt
  minGetStmt *sql.Stmt
  maxStoreStmt *sql.Stmt
  maxGetStmt *sql.Stmt
  needToSkipStoreStmt *sql.Stmt
  needToSkipGetStmt *sql.Stmt
  needToSkipDeleteStmt *sql.Stmt
}


func MakeMenciusStore(name string, paxosType string, createNew bool) (*MenciusStore, common.DbError) {
  menciusdb := new(MenciusStore)
  menciusdb.paxosType = paxosType
  filename := fmt.Sprintf("./db/%v_menciusstore.db", name)
  if createNew {
    os.Remove(filename)
  }

  var err error
  menciusdb.db, err = sql.Open("sqlite3", filename)
  if err != nil {
    log.Printf("%q: %s\n", err, filename)
    return menciusdb, "ErrDbNoConnection"
  }
  if createNew {
    // StateTable
    var sql string
    if paxosType == common.ShardKVPaxos {
       sql = fmt.Sprintf(`create table %v (num integer not null primary key, n_p integer, n_a integer, init integer, d_cmd text, d_key text, d_clientid integer, d_sn integer, d_value text, d_confignum integer, d_shards blob, d_groups blob, v_cmd text, v_key text, v_clientid integer, v_sn integer, v_value text, v_confignum integer, v_shards blob, v_groups blob); 
       delete from %v;`, StateTable, StateTable)
    } else if paxosType == common.StringPaxos {
       sql = fmt.Sprintf(`create table %v (num integer not null primary key, n_p text, n_a text, init integer, d text, v text); 
       delete from %v;`, StateTable, StateTable)
    } else {
      panic("paxos type persistence is not supported in menciusstore.go")
    }
    _, err = menciusdb.db.Exec(sql)
    if err != nil {
      log.Printf("%q: %s\n", err, sql)
      return menciusdb, "ErrDbCreate"
    }
    // DeadTable
    sql = fmt.Sprintf(`create table %v (key integer not null primary key, dead integer);
    delete from %v;`, DeadTable, DeadTable)
    _, err = menciusdb.db.Exec(sql)
    if err != nil {
      log.Printf("%q: %s\n", err, sql)
      return menciusdb, "ErrDbCreate"
    }

    // NeedToSkipTable
    sql = fmt.Sprintf(`create table %v (key integer not null primary key, need_to_skip text);
    delete from %v;`, NeedToSkipTable, NeedToSkipTable)
    _, err = menciusdb.db.Exec(sql)
    if err != nil {
      log.Printf("%q: %s\n", err, sql)
      return menciusdb, "ErrDbCreate"
    }
    // MinTable
    sql = fmt.Sprintf(`create table %v (key integer not null primary key, min integer);
    delete from %v;`, MinTable, MinTable)
    _, err = menciusdb.db.Exec(sql)
    if err != nil {
      log.Printf("%q: %s\n", err, sql)
      return menciusdb, "ErrDbCreate"
    }
    // MaxTable
    sql = fmt.Sprintf(`create table %v (key integer not null primary key, max integer);
    delete from %v;`, MaxTable, MaxTable)
    _, err = menciusdb.db.Exec(sql)
    if err != nil {
      log.Printf("%q: %s\n", err, sql)
      return menciusdb, "ErrDbCreate"
    }
    // DoneTable
    sql = fmt.Sprintf(`create table %v (key integer not null primary key, done integer);
    delete from %v;`, DoneTable, DoneTable)
    _, err = menciusdb.db.Exec(sql)
    if err != nil {
      log.Printf("%q: %s\n", err, sql)
      return menciusdb, "ErrDbCreate"
    }
    // DonesTable
    sql = fmt.Sprintf(`create table %v (key integer not null primary key, dones text);
    delete from %v;`, DonesTable, DonesTable)
    _, err = menciusdb.db.Exec(sql)
    if err != nil {
      log.Printf("%q: %s\n", err, sql)
      return menciusdb, "ErrDbCreate"
    }

  }

  // State
  if paxosType == common.ShardKVPaxos {
    menciusdb.stateStoreStmt, err = menciusdb.db.Prepare(fmt.Sprintf("insert or replace into %v(num, n_p, n_a, init, d_cmd, d_key, d_clientid, d_sn, d_value, d_confignum, d_shards, d_groups, v_cmd, v_key, v_clientid, v_sn, v_value, v_confignum, v_shards, v_groups) values(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)", StateTable))
  } else if paxosType == common.StringPaxos {
    menciusdb.stateStoreStmt, err = menciusdb.db.Prepare(fmt.Sprintf("insert or replace into %v(num, n_p, n_a, init, d, v) values(?, ?, ?, ?, ?, ?)", StateTable))
  } else {
    panic("paxos persistence not supported")
  }
  if err != nil {
    log.Println(err)
    return menciusdb, "ErrDbCreate"
  }
  menciusdb.stateGetStmt, err = menciusdb.db.Prepare(fmt.Sprintf("select * from %v where num = ?", StateTable))
  if err != nil {
    log.Println(err)
    return menciusdb, "ErrDbCreate"
  }

  menciusdb.stateGetAllStmt, err = menciusdb.db.Prepare(fmt.Sprintf("select * from %v", StateTable))
  if err != nil {
    log.Println(err)
    return menciusdb, "ErrDbCreate"
  }
  // Dead
  menciusdb.deadStoreStmt, err = menciusdb.db.Prepare(fmt.Sprintf("insert or replace into %v(key, dead) values(?, ?)", DeadTable))
  if err != nil {
    log.Println(err)
    return menciusdb, "ErrDbCreate"
  }

  menciusdb.deadGetStmt, err = menciusdb.db.Prepare(fmt.Sprintf("select dead from %v where key = ?", DeadTable))
  if err != nil {
    log.Println(err)
    return menciusdb, "ErrDbCreate"
  }

  // Need-to-skip
  menciusdb.needToSkipStoreStmt, err = menciusdb.db.Prepare(fmt.Sprintf("insert or replace into %v(key, need_to_skip) values(?, ?)", NeedToSkipTable))
  if err != nil {
    log.Println(err)
    return menciusdb, "ErrDbCreate"
  }

  menciusdb.needToSkipGetStmt, err = menciusdb.db.Prepare(fmt.Sprintf("select need_to_skip from %v where key = ?", NeedToSkipTable))
  if err != nil {
    log.Println(err)
    return menciusdb, "ErrDbCreate"
  }
  menciusdb.needToSkipDeleteStmt, err = menciusdb.db.Prepare(fmt.Sprintf("delete from %v where key = ?", NeedToSkipTable))
  if err != nil {
    log.Println(err)
    return menciusdb, "ErrDbCreate"
  }

  // Done
  menciusdb.doneStoreStmt, err = menciusdb.db.Prepare(fmt.Sprintf("insert or replace into %v(key, done) values(?, ?)", DoneTable))
  if err != nil {
    log.Println(err)
    return menciusdb, "ErrDbCreate"
  }

  menciusdb.doneGetStmt, err = menciusdb.db.Prepare(fmt.Sprintf("select done from %v where key = ?", DoneTable))
  if err != nil {
    log.Println(err)
    return menciusdb, "ErrDbCreate"
  }

  // Dones
  menciusdb.donesStoreStmt, err = menciusdb.db.Prepare(fmt.Sprintf("insert or replace into %v(key, dones) values(?, ?)", DonesTable))
  if err != nil {
    log.Println(err)
    return menciusdb, "ErrDbCreate"
  }

  menciusdb.donesGetStmt, err = menciusdb.db.Prepare(fmt.Sprintf("select dones from %v where key = ?", DonesTable))
  if err != nil {
    log.Println(err)
    return menciusdb, "ErrDbCreate"
  }


  // Min
  menciusdb.minStoreStmt, err = menciusdb.db.Prepare(fmt.Sprintf("insert or replace into %v(key, min) values(?, ?)", MinTable))
  if err != nil {
    log.Println(err)
    return menciusdb, "ErrDbCreate"
  }

  menciusdb.minGetStmt, err = menciusdb.db.Prepare(fmt.Sprintf("select min from %v where key = ?", MinTable))
  if err != nil {
    log.Println(err)
    return menciusdb, "ErrDbCreate"
  }


  // Max
  menciusdb.maxStoreStmt, err = menciusdb.db.Prepare(fmt.Sprintf("insert or replace into %v(key, max) values(?, ?)", MaxTable))
  if err != nil {
    log.Println(err)
    return menciusdb, "ErrDbCreate"
  }

  menciusdb.maxGetStmt, err = menciusdb.db.Prepare(fmt.Sprintf("select max from %v where key = ?", MaxTable))
  if err != nil {
    log.Println(err)
    return menciusdb, "ErrDbCreate"
  }

  return menciusdb, "DbOk"
}
/**
func (menciusdb *MenciusStore) BatchStore(needToSkip map[int]map[int]bool, instances map[int]MenciusState, done int, Dones []int, Min int, Max int) {

}
**/

////////////////////////////////////////////////////////////////////////////////
// MenciusState Struct Storage
////////////////////////////////////////////////////////////////////////////////
func (menciusdb *MenciusStore) StateStoreAll(m map[int]common.MenciusState) common.DbError {
  if (menciusdb.dead) {
    log.Printf("%v MenciusStore: %v\n", StateTable, "ErrDbDead")
    return "ErrDbDead"
  }
  for i, s := range m {
    if menciusdb.paxosType == common.ShardKVPaxos {
      d, ok := s.DecidedValue.(common.ShardKVOp)
      if !ok {
         d = common.ShardKVOp{}
      }
      v, ok := s.V_a.(common.ShardKVOp)
      if !ok {
         v = common.ShardKVOp{}
      }
      shards := new(bytes.Buffer)
      shards_enc := gob.NewEncoder(shards)
      shards_enc.Encode(d.Shards)

      groups := new(bytes.Buffer)
      groups_enc := gob.NewEncoder(groups)
      groups_enc.Encode(d.Groups)

      v_shards := new(bytes.Buffer)
      v_shards_enc := gob.NewEncoder(v_shards)
      v_shards_enc.Encode(v.Shards)

      v_groups := new(bytes.Buffer)
      v_groups_enc := gob.NewEncoder(v_groups)
      v_groups_enc.Encode(v.Groups)

      _, err := menciusdb.stateStoreStmt.Exec(i, s.N_p, s.N_a, s.Init, d.Cmd, d.Key, d.ClientId, d.Sn, d.Value, d.ConfigNum, shards.Bytes(), groups.Bytes(), v.Cmd, v.Key, v.ClientId, v.Sn, v.Value, v.ConfigNum, v_shards.Bytes(), v_groups.Bytes())
      if err != nil {
        log.Printf("%v MenciusStore: %v\n", StateTable, err)
        return "ErrDbExec"
      }
    } else if menciusdb.paxosType == common.StringPaxos {
      _, err := menciusdb.stateStoreStmt.Exec(i, s.N_p, s.N_a, s.Init, s.DecidedValue, s.V_a)
      if err != nil {
        log.Printf("%v MenciusStore: %v\n", StateTable, err)
        return "ErrDbExec"
      }
    }
  }

  return "DbOk"
}


func (menciusdb *MenciusStore) StateStore(num int, s *common.MenciusState) common.DbError {
  if (menciusdb.dead) {
    log.Printf("%v MenciusStore: %v\n", StateTable, "ErrDbDead")
    return "ErrDbDead"
  }

  if menciusdb.paxosType == common.ShardKVPaxos {
    d, ok := s.DecidedValue.(common.ShardKVOp)
    if !ok {
       d = common.ShardKVOp{}
    }
    v, ok := s.V_a.(common.ShardKVOp)
    if !ok {
       v = common.ShardKVOp{}
    }

    shards := new(bytes.Buffer)
    shards_enc := gob.NewEncoder(shards)
    shards_enc.Encode(d.Shards)

    groups := new(bytes.Buffer)
    groups_enc := gob.NewEncoder(groups)
    groups_enc.Encode(d.Groups)

    v_shards := new(bytes.Buffer)
    v_shards_enc := gob.NewEncoder(v_shards)
    v_shards_enc.Encode(v.Shards)

    v_groups := new(bytes.Buffer)
    v_groups_enc := gob.NewEncoder(v_groups)
    v_groups_enc.Encode(v.Groups)

    _, err := menciusdb.stateStoreStmt.Exec(num, s.N_p, s.N_a, s.Init, d.Cmd, d.Key, d.ClientId, d.Sn, d.Value, d.ConfigNum, shards.Bytes(), groups.Bytes(), v.Cmd, v.Key, v.ClientId, v.Sn, v.Value, v.ConfigNum, v_shards.Bytes(), v_groups.Bytes())

    if err != nil {
      log.Printf("%v MenciusStore: %v\n", StateTable, err)
      return "ErrDbExec"
    }
    return "DbOk"
  } else if menciusdb.paxosType == common.StringPaxos {
    _, err := menciusdb.stateStoreStmt.Exec(num, s.N_p, s.N_a, s.Init, s.DecidedValue, s.V_a)
    if err != nil {
      log.Printf("%v MenciusStore: %v\n", StateTable, err)
      return "ErrDbExec"
    }
    return "DbOk"
  }
  return "ErrDbExec"
}

func (menciusdb *MenciusStore) StateGet(num int) (common.MenciusState, common.DbError) {
  s := common.MenciusState{}
  if (menciusdb.dead) {
    log.Printf("%v MenciusStateGet: %v\n", StateTable, "ErrDbDead")
    return s, "ErrDbDead"
  }
  if menciusdb.paxosType == common.ShardKVPaxos {
    s := common.MenciusState{}
    d := common.ShardKVOp{}
    v := common.ShardKVOp{}
    s.DecidedValue = d
    s.V_a = v
    d_shards := []byte{}
    d_groups := []byte{}
    v_shards := []byte{}
    v_groups := []byte{}
    var numKey int
    err := menciusdb.stateGetStmt.QueryRow(num).Scan(&numKey, &s.N_p, &s.N_a, &s.Init, &d.Cmd, &d.Key, &d.ClientId, &d.Sn, &d.Value, &d.ConfigNum, &d_shards, &d_groups, &v.Cmd, &v.Key, &v.ClientId, &v.Sn, &v.Value, &v.ConfigNum, &v_shards, &v_groups)
    if numKey != num { panic("something's wrong")}
    buf1 := bytes.NewBuffer(d_shards)
    dec1 := gob.NewDecoder(buf1)
    dec1.Decode(&d.Shards)
    buf2 := bytes.NewBuffer(d_shards)
    dec2 := gob.NewDecoder(buf2)
    dec2.Decode(&d.Groups)
    buf3 := bytes.NewBuffer(v_shards)
    dec3 := gob.NewDecoder(buf3)
    dec3.Decode(&v.Shards)
    buf4 := bytes.NewBuffer(v_groups)
    dec4 := gob.NewDecoder(buf4)
    dec4.Decode(&v.Groups)
    s.DecidedValue = d
    s.V_a = v
    if err != nil {
      // If the key is not in the database
      if err == sql.ErrNoRows {
        return s, "ErrDbNoKey"
      }
      log.Printf("%v MenciusStateGet: %v\n", StateTable, err)
      return s, "ErrDbQuery"
    }
    return s, "DbOk"
  } else if menciusdb.paxosType == common.StringPaxos {
    var numKey int
    err := menciusdb.stateGetStmt.QueryRow(num).Scan(&numKey, &s.N_p, &s.N_a, &s.Init, &s.DecidedValue, &s.V_a)
    if numKey != num { panic("something's wrong")}
    if err != nil {
      // If the key is not in the database
      if err == sql.ErrNoRows {
        return s, "ErrDbNoKey"
      }
      log.Printf("%v KvGetValue: %v\n", StateTable, err)
      return s, "ErrDbQuery"
    }
    return s, "DbOk"
  }
  return s, "ErrDbExec"
}

func (menciusdb *MenciusStore) StateGetAll() (map[int] common.MenciusState, common.DbError) {
  if (menciusdb.dead) {
    log.Printf("%v KvGetValue: %v\n", StateTable, "ErrDbDead")
    return nil, "ErrDbDead"
  }
  rows, err := menciusdb.stateGetAllStmt.Query()
  if err != nil {
     log.Printf("Something wrong in StateGetAll()")
     return nil, "ErrDbQuery"
  }
  defer rows.Close()
  m := make(map[int] common.MenciusState)
  for rows.Next() {
    s := common.MenciusState{}
    var numKey int
    if menciusdb.paxosType == common.ShardKVPaxos {
      d := common.ShardKVOp{}
      v := common.ShardKVOp{}
      s.DecidedValue = d
      s.V_a = v
      d_shards := []byte{}
      d_groups := []byte{}
      v_shards := []byte{}
      v_groups := []byte{}

      rows.Scan(&numKey, &s.N_p, &s.N_a, &s.Init, &d.Cmd, &d.Key, &d.ClientId, &d.Sn, &d.Value, &d.ConfigNum, &d_shards, &d_groups, &v.Cmd, &v.Key, &v.ClientId, &v.Sn, &v.Value, &v.ConfigNum, &v_shards, &v_groups)
      buf1 := bytes.NewBuffer(d_shards)
      dec1 := gob.NewDecoder(buf1)
      dec1.Decode(&d.Shards)
      buf2 := bytes.NewBuffer(d_shards)
      dec2 := gob.NewDecoder(buf2)
      dec2.Decode(&d.Groups)
      buf3 := bytes.NewBuffer(v_shards)
      dec3 := gob.NewDecoder(buf3)
      dec3.Decode(&v.Shards)
      buf4 := bytes.NewBuffer(v_groups)
      dec4 := gob.NewDecoder(buf4)
      dec4.Decode(&v.Groups)
        s.DecidedValue = d
      s.V_a = v
    } else if menciusdb.paxosType == common.StringPaxos {
      rows.Scan(&numKey, &s.N_p, &s.N_a, &s.Init, &s.DecidedValue, &s.V_a)
    }
    m[numKey] = s
  }
  rows.Close()
  return m, "DbOk"
}

func (menciusdb *MenciusStore) NeedToSkipStore(needToSkip map[int] map[int] bool) common.DbError {
  if (menciusdb.dead) {
    log.Printf("%v MenciusStore: %v\n", NeedToSkipTable, "ErrDbDead")
    return "ErrDbDead"
  }
  var store string
  for mapKey, mapValue := range needToSkip {
      entry := strconv.Itoa(mapKey) + ">"
      for mK, mV := range mapValue {
          if mV == true {
            entry += strconv.Itoa(mK) + ":1 "
          } else {
            entry += strconv.Itoa(mK) + ":0 "
          }
      }
      store += entry + ","
  }
  _, err := menciusdb.needToSkipStoreStmt.Exec(0, store)
  if err != nil {
    log.Printf("%v MenciusStore: %v\n", NeedToSkipTable, err)
    return "ErrDbExec"
  }
  return "DbOk"
}
func (menciusdb *MenciusStore) NeedToSkipGet() (map[int] map[int] bool, common.DbError) {
  if (menciusdb.dead) {
    log.Printf("%v MenciusStore: %v\n", NeedToSkipTable, "ErrDbDead")
    return nil, "ErrDbDead"
  }
  var result string
  r := make (map[int] map[int] bool)
  err := menciusdb.needToSkipGetStmt.QueryRow(0).Scan(&result)
  if err != nil {
    // If the key is not in the database
    if err == sql.ErrNoRows {
      return r, "ErrDbNoKey"
    }
    log.Printf("%v MenciusStore: %v\n", NeedToSkipTable, err)
    return r, "ErrDbQuery"
  }
  r_intermediate := strings.Split(result, ",")
  for _, s := range r_intermediate {
      s_intermediate := strings.Split(s, ">")
      if len(s_intermediate) == 2 {
        s_map := make (map[int] bool)
        for _, j := range strings.Split(s_intermediate[1], " ") {
            if len(j) > 0 {
               j_intermediate := strings.Split(j, ":")
               converted, err := strconv.Atoi(j_intermediate[0])
               if err != nil { panic("something's wrong in menciusstore.go")}
               if j_intermediate[1] == "1" {
                  s_map[converted] = true
               } else {
                  s_map[converted] = false
               }
            }
        }
        converted, err := strconv.Atoi(s_intermediate[0])
        if err != nil { panic("oops conversion failed")}
        r[converted] = s_map
      }
  }
  return r, "DbOk"
}

func (menciusdb *MenciusStore) NeedToSkipDelete(key int) common.DbError {
  if (menciusdb.dead) {
    log.Printf("%v MenciusStore: %v\n", NeedToSkipTable, "ErrDbDead")
    return "ErrDbDead"
  }
  _, err := menciusdb.needToSkipDeleteStmt.Exec(key)
  if err != nil {
    // If the key is not in the database
    if err == sql.ErrNoRows {
      return "DbOk"
    }
    log.Printf("%v MenciusStore: %v\n", NeedToSkipTable, err)
    return "ErrDbQuery"
  }
  return "DbOk"
}

func (menciusdb *MenciusStore) DeadStore(dead bool) common.DbError {
  if (menciusdb.dead) {
    log.Printf("%v MenciusStore: %v\n", DeadTable, "ErrDbDead")
    return "ErrDbDead"
  }
  _, err := menciusdb.deadStoreStmt.Exec(0, dead)
  if err != nil {
    log.Printf("%v MenciusStore: %v\n", DeadTable, err)
    return "ErrDbExec"
  }
  return "DbOk"
}

func (menciusdb *MenciusStore) DeadGet() (bool, common.DbError) {
  if (menciusdb.dead) {
    log.Printf("%v MenciusStore: %v\n", DeadTable, "ErrDbDead")
    return true, "ErrDbDead"
  }
  var dead bool
  err := menciusdb.deadGetStmt.QueryRow(0).Scan(&dead)
  if err != nil {
    // If the key is not in the database
    if err == sql.ErrNoRows {
      return dead, "ErrDbNoKey"
    }
    log.Printf("%v MenciusStore: %v\n", DeadTable, err)
    return dead, "ErrDbQuery"
  }
  return dead, "DbOk"
}

func (menciusdb *MenciusStore) DonesStore(input []int) common.DbError {
  if (menciusdb.dead) {
    log.Printf("%v MenciusStore: %v\n", DonesTable, "ErrDbDead")
    return "ErrDbDead"
  }
  var store string
  for _, s := range input {
      store += strconv.Itoa(s) + ","
  }
  // log.Printf("Store: %s", store)
  _, err := menciusdb.donesStoreStmt.Exec(0, store)
  if err != nil {
    log.Printf("%v MenciusStore: %v\n", DonesTable, err)
    return "ErrDbExec"
  }
  return "DbOk"
}

func (menciusdb *MenciusStore) DonesGet() ([]int, common.DbError) {
  if (menciusdb.dead) {
    log.Printf("%v MenciusStore: %v\n", DonesTable, "ErrDbDead")
    return nil, "ErrDbDead"
  }
  var result string
  var r []int
  err := menciusdb.donesGetStmt.QueryRow(0).Scan(&result)
  if err != nil {
    // If the key is not in the database
    if err == sql.ErrNoRows {
      return r, "ErrDbNoKey"
    }
    log.Printf("%v MenciusStore: %v\n", DoneTable, err)
    return r, "ErrDbQuery"
  }
  r_intermediate := strings.Split(result, ",")
  r = make ([]int, len(r_intermediate)-1)
  for i, j := range r_intermediate {
     if len(j) > 0 {
        converted, err := strconv.Atoi(j)
        if err != nil { panic("oops")}
        r[i] = converted
     }
  }
  return r, "DbOk"
}

func (menciusdb *MenciusStore) DoneStore(input int) common.DbError {
  if (menciusdb.dead) {
    log.Printf("%v MenciusStore: %v\n", DoneTable, "ErrDbDead")
    return "ErrDbDead"
  }
  _, err := menciusdb.doneStoreStmt.Exec(0, input)
  if err != nil {
    log.Printf("%v MenciusStore: %v\n", DoneTable, err)
    return "ErrDbExec"
  }
  return "DbOk"
}

func (menciusdb *MenciusStore) DoneGet() (int, common.DbError) {
  if (menciusdb.dead) {
    log.Printf("%v MenciusStore: %v\n", DoneTable, "ErrDbDead")
    return -1, "ErrDbDead"
  }
  var r int = -1
  err := menciusdb.doneGetStmt.QueryRow(0).Scan(&r)
  if err != nil {
    // If the key is not in the database
    if err == sql.ErrNoRows {
      return r, "ErrDbNoKey"
    }
    log.Printf("%v MenciusStore: %v\n", DoneTable, err)
    return r, "ErrDbQuery"
  }
  return r, "DbOk"
}

func (menciusdb *MenciusStore) MinStore(min int) common.DbError {
  if (menciusdb.dead) {
    log.Printf("%v MenciusStore: %v\n", MinTable, "ErrDbDead")
    return "ErrDbDead"
  }
  _, err := menciusdb.minStoreStmt.Exec(0, min)
  if err != nil {
    log.Printf("%v MenciusStore: %v\n", MinTable, err)
    return "ErrDbExec"
  }
  return "DbOk"
}

func (menciusdb *MenciusStore) MinGet() (int, common.DbError) {
  if (menciusdb.dead) {
    log.Printf("%v MenciusStore: %v\n", MinTable, "ErrDbDead")
    return -1, "ErrDbDead"
  }
  var min int = -1
  err := menciusdb.minGetStmt.QueryRow(0).Scan(&min)
  if err != nil {
    // If the key is not in the database
    if err == sql.ErrNoRows {
      return min, "ErrDbNoKey"
    }
    log.Printf("%v MenciusStore: %v\n", MinTable, err)
    return min, "ErrDbQuery"
  }
  return min, "DbOk"
}
func (menciusdb *MenciusStore) MaxStore(max int) common.DbError {
  if (menciusdb.dead) {
    log.Printf("%v MenciusStore: %v\n", MaxTable, "ErrDbDead")
    return "ErrDbDead"
  }
  _, err := menciusdb.maxStoreStmt.Exec(0, max)
  if err != nil {
    log.Printf("%v MenciusStore: %v\n", MaxTable, err)
    return "ErrDbExec"
  }
  return "DbOk"
}

func (menciusdb *MenciusStore) MaxGet() (int, common.DbError) {
  if (menciusdb.dead) {
    log.Printf("%v MenciusStore: %v\n", MaxTable, "ErrDbDead")
    return -1, "ErrDbDead"
  }
  var max int = -1
  err := menciusdb.maxGetStmt.QueryRow(0).Scan(&max)
  if err != nil {
    // If the key is not in the database
    if err == sql.ErrNoRows {
      return max, "ErrDbNoKey"
    }
    log.Printf("%v MenciusStore: %v\n", MaxTable, err)
    return max, "ErrDbQuery"
  }
  return max, "DbOk"
}

func (menciusdb *MenciusStore) Kill() {
  menciusdb.dead = true

  menciusdb.stateStoreStmt.Close()
  menciusdb.stateGetStmt.Close()
  menciusdb.stateGetAllStmt.Close()
  menciusdb.minStoreStmt.Close()
  menciusdb.minGetStmt.Close()
  menciusdb.maxStoreStmt.Close()
  menciusdb.maxGetStmt.Close()
  menciusdb.doneStoreStmt.Close()
  menciusdb.doneGetStmt.Close()
  menciusdb.donesStoreStmt.Close()
  menciusdb.donesGetStmt.Close()
  menciusdb.needToSkipStoreStmt.Close()
  menciusdb.needToSkipGetStmt.Close()
  menciusdb.deadStoreStmt.Close()
  menciusdb.deadGetStmt.Close()
  menciusdb.db.Close()
}
