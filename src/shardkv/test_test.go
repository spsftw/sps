package shardkv

import "testing"
import "shardmaster"
import "runtime"
import "strconv"
import "os"
import "time"
import "fmt"
import "sync"
import "math/rand"
import "math"
import "common"
import "reflect"

//
// Lab4B extended tests for persistence,
// including original Lab4B tests.
//
// It's recommended to run these tests one
// at a time with "go test -run TestXXX".
// TestMassiveData requires a longer timeout 
// than 10 mins, e.g. use "-timeout 25200s".
//

/*******************************************/
// FULL TEST LIST
//
// TestSingleCrash
// TestSingleCrashUnreliable
// TestSingleDiskCrash
// TestSingleDiskCrashUnreliable
// TestSingleLargeDiskCrash
// TestSingleLargeDiskCrashUnreliable
// TestCornerCaseDiskCrash
// TestCornerCaseDiskCrashUnreliable
// TestAllCrash
// TestAllCrashUnreliable
// TestAllLargeCrash
// TestAllLargeCrashUnreliable
// TestMassiveData
// TestMassiveDataUnreliable
// TestBasic
// TestMove
// TestLimp
// TestConcurrent
// TestConcurrentUnreliable
// 
/*******************************************/

const test_persistence = true // persistence testing
const db_path = "./db/"
const ngroups = 3 // replica groups
const nreplicas = 3 // servers per group
const hundred_mb = 100000000
const fifty_mb = 50000000
const ten_mb = 10000000
func RestartReplica(group int, replica int, gids []int64, smh []string, ha [][]string, sa [][]*ShardKV, uids[][]int) {
  fmt.Printf("*******")
  fmt.Printf("Restarting replica %d ", uids[group][replica])
  fmt.Printf("*******")
  sa[group][replica] = StartServer(gids[group], smh, ha[group],uids[group][replica], replica, true /** restart* */)
}

func singleCrash(t *testing.T, disk_loss bool, large_puts bool, unreliable bool) {
  smh, gids, ha, sa, clean, uids := setup("crash"+strconv.FormatBool(disk_loss)+strconv.FormatBool(large_puts)+strconv.FormatBool(unreliable), unreliable)
  defer clean()

  mck := shardmaster.MakeClerk(smh)
  for i := 0; i < len(gids); i++ {
    mck.Join(gids[i], ha[i])
  }
  ck := MakeClerk(smh)

  // puts to a replica group
  rand.Seed(time.Now().Unix())
  rand_group := rand.Intn(ngroups)
  cfg := mck.Query(-1)
  shrds := make([]int64, 10)
  count := 0
  for i, g := range cfg.Shards {
    if g == gids[rand_group] {
      shrds[count] = int64(i)
      count++
    }
  }

  shrds = shrds[:count]
  vals_stage1 := make(map[string]string) // before crash
  vals_stage2 := make(map[string]string) // while crashed
  vals_stage3 := make(map[string]string) // after crash
  //fmt.Printf("cfg.Shards: %+v\n",cfg.Shards)
  //fmt.Printf("Targeted Shards: %+v\n",shrds)
  /*runtime.GC()
  var m0 runtime.MemStats
  runtime.ReadMemStats(&m0)
  fmt.Printf("memstats (before puts): %+v\n",m0)*/
  num_keys := 5//25
  data_size := fifty_mb // 50 MB
  if large_puts {
    num_keys = 1
  }
  for _, v := range shrds {
    for j := 0; j < num_keys; j++ {
      arr := []byte(strconv.Itoa(rand.Intn(math.MaxInt32)))
      key_len := len(arr)
      if large_puts {
        arr = make([]byte, data_size)
        for j := 1; j < len(arr); j++ {
          arr[j] = byte('a' + rand.Int() % 26)
        }
        //fmt.Printf("shard %+v put large key %+v of %+v\n",v,j+1,num_keys)
      }
      arr[0] = byte(v + 100)
      key := string(arr[:key_len])
      val := string(arr[:])
      vals_stage1[key] = val
      //fmt.Printf("ck.Put, shard %+v, key %+v, val %+v\n",key2shard(key),key,val)
      fmt.Printf("ck.Put, shard %+v, key %+v\n",key2shard(key),key)
      ck.Put(key, val)
    }
  }
  /*runtime.GC()
  var m1 runtime.MemStats
  runtime.ReadMemStats(&m1)
  fmt.Printf("memstats (after puts): %+v\n",m1)*/
  // random server in group crashes
  rand_server := rand.Intn(nreplicas)
  fmt.Printf("kill replica %+v\n",rand_server)
  sa[rand_group][rand_server].kill()
  if disk_loss {
    // wipe DB of crashed server
    kv_db := db_path + "s" + strconv.Itoa(sa[rand_group][rand_server].uid) + "_" + strconv.Itoa(int(gids[rand_group])) + "_kvstore.db"
    mencius_db := db_path + "s" + strconv.Itoa(sa[rand_group][rand_server].uid) + "_menciusstore.db"
    err := os.Remove(kv_db)
    if err != nil {
      t.Fatalf("Failed to remove database %+v\n",kv_db)
    }
    fmt.Printf("removing %+v\n",kv_db)
    err = os.Remove(mencius_db)
    if err != nil {
      t.Fatalf("Failed to remove database %+v\n",mencius_db)
    }
    fmt.Printf("removing %+v\n",mencius_db)
  }
  // puts to the replica group
  for _, v := range shrds {
    for j := 0; j < num_keys; j++ {
      arr := []byte(strconv.Itoa(rand.Intn(math.MaxInt32)))
      key_len := len(arr)
      if large_puts {
        arr = make([]byte, data_size)
        for j := 1; j < len(arr); j++ {
          arr[j] = byte('a' + rand.Int() % 26)
        }
      }
      arr[0] = byte(v + 100)
      key := string(arr[:key_len])
      val := string(arr[:])
      vals_stage2[key] = val
      ck.Put(key, val)
    }
  }
  // bring server back up
  RestartReplica(rand_group, rand_server, gids, smh, ha, sa, uids)
  for _, v := range shrds {
    for j := 0; j < num_keys; j++ {
      arr := []byte(strconv.Itoa(rand.Intn(math.MaxInt32)))
      key_len := len(arr)
      if large_puts {
        arr = make([]byte, data_size)
        for j := 1; j < len(arr); j++ {
          arr[j] = byte('a' + rand.Int() % 26)
        }
      }
      arr[0] = byte(v + 100)
      key := string(arr[:key_len])
      val := string(arr[:])
      vals_stage3[key] = val
      //fmt.Printf("ck.Put, shard %+v, key %+v, val %+v\n",key2shard(key),key,val)
      ck.Put(key, val)
    }
  }
  for {
    if sa[rand_group][rand_server].upToDate {
      break
    }
    time.Sleep(1 * time.Second)
  }
  if !unreliable {
    // check that all puts exist on crashed server
    total := 0
    pass := 0
    fmt.Printf("Keys still on replica %+v: [",rand_server)
    for k, _ := range vals_stage1 {
      s := key2shard(k)
      if vals_stage1[k] == sa[rand_group][rand_server].kvmap[s][k] {
        fmt.Printf(" y ")
        pass++
      } else {
        fmt.Printf(" n ")
      }
      total++
    }
    for k, _ := range vals_stage2 {
      s := key2shard(k)
      if vals_stage2[k] == sa[rand_group][rand_server].kvmap[s][k] {
        fmt.Printf(" y ")
        pass++
      } else {
        fmt.Printf(" n ")
      }
      total++
    }
    for k, _ := range vals_stage3 {
      s := key2shard(k)
      if vals_stage3[k] == sa[rand_group][rand_server].kvmap[s][k] {
        fmt.Printf(" y ")
        pass++
      } else {
        fmt.Printf(" n ")
      }
      total++
    }
    fmt.Printf("]\n")
    //for _, s := range shrds {
      //len := len(sa[rand_group][rand_server].kvmap[s])
      //fmt.Printf("crashed kvserver kvmap shard %+v is length %+v\n",s,len)
    //}
    //for i, _ := range sa[rand_group] {
      //fmt.Printf("sa[%+v].kvmap: %+v\n",i,sa[rand_group][i].kvmap)
    //}
    if total != pass {
      t.Fatalf("Restarted replica is missing %+v keys of %+v\n",total-pass,total)
    }
    // consistency check
    for i, _ := range sa[rand_group] {
      if i != rand_server {
        if reflect.DeepEqual(sa[rand_group][rand_server].kvmap,sa[rand_group][i].kvmap) {
          fmt.Printf("Replica %+v passed consistency check\n",i)
        } else {
          t.Fatalf("Replica %+v failed consistency check\n",i)
        }
      }
    }
  } else {
    // unreliable
    keys_total := 0
    keys_pass := 0
    count := 0
    fmt.Printf("Keys still on majority: [")
    for k, v := range vals_stage1 {
      keys_total++
      count = 0
      shard_num := key2shard(k)
      for i, _ := range sa[rand_group] {
        if sa[rand_group][i].kvmap[shard_num][k] == v {
          count++
        }
      }
      if count > int(nreplicas/2) {
        fmt.Printf(" y ")
        keys_pass++
      } else {
        fmt.Printf(" n ")
      }
    }
    for k, v := range vals_stage2 {
      keys_total++
      count = 0
      shard_num := key2shard(k)
      for i, _ := range sa[rand_group] {
        if sa[rand_group][i].kvmap[shard_num][k] == v {
          count++
        }
      }
      if count > int(nreplicas/2) {
        fmt.Printf(" y ")
        keys_pass++
      } else {
        fmt.Printf(" n ")
      }
    }
    for k, v := range vals_stage3 {
      keys_total++
      count = 0
      shard_num := key2shard(k)
      for i, _ := range sa[rand_group] {
        if sa[rand_group][i].kvmap[shard_num][k] == v {
          count++
        }
      }
      if count > int(nreplicas/2) {
        fmt.Printf(" y ")
        keys_pass++
      } else {
        fmt.Printf(" n ")
      }
    }
    fmt.Printf("]\n")
    if keys_total != keys_pass {
      t.Fatalf("Majority is missing %+v keys of %+v\n",keys_total-keys_pass,keys_total)
    }
  }
}

func TestSingleCrash(t *testing.T) {
  if test_persistence {
    fmt.Printf("Test: Single Crash ...\n")
    singleCrash(t, false, false, false)
    fmt.Printf("  ... Passed\n")
  }
}

func TestSingleCrashUnreliable(t *testing.T) {
  if test_persistence {
    fmt.Printf("Test: Single Crash (unreliable) ...\n")
    singleCrash(t, false, false, true)
    fmt.Printf("  ... Passed\n")
  }
}

func TestSingleDiskCrash(t *testing.T) {
  if test_persistence {
    fmt.Printf("Test: Single crash with disk failure ...\n")
    singleCrash(t, true, false, false)
    fmt.Printf("  ... Passed\n")
  }
}

func TestSingleDiskCrashUnreliable(t *testing.T) {
  if test_persistence {
    fmt.Printf("Test: Single crash with disk failure (unreliable) ...\n")
    singleCrash(t, true, false, true)
    fmt.Printf("  ... Passed\n")
  }
}

func TestSingleLargeDiskCrash(t *testing.T) {
  if test_persistence {
    fmt.Printf("Test: Single crash with GBs of data and disk failure ...\n")
    singleCrash(t, true, true, false)
    fmt.Printf("  ... Passed\n")
  }
}

func TestSingleLargeDiskCrashUnreliable(t *testing.T) {
  if test_persistence {
    fmt.Printf("Test: Single crash with GBs of data and disk failure (unreliable) ...\n")
    singleCrash(t, true, true, true)
    fmt.Printf("  ... Passed\n")
  }
}

func cornerCrash(t *testing.T, unreliable bool) {
  tag := "corner"+strconv.FormatBool(unreliable)
  smh, gids, ha, sa, clean, uids := setup(tag, unreliable)
  _ = uids
  defer clean()

  mck := shardmaster.MakeClerk(smh)
  for i := 0; i < len(gids); i++ {
    mck.Join(gids[i], ha[i])
  }
  ck := MakeClerk(smh)

  // make minority of peers in a replica group deaf
  rand.Seed(time.Now().Unix())
  rand_group := rand.Intn(ngroups)
  cfg := mck.Query(-1)
  shrds := make([]int64, 10)
  count := 0
  for i, g := range cfg.Shards {
    if g == gids[rand_group] {
      shrds[count] = int64(i)
      count++
    }
  }
  shrds = shrds[:count]
  r_vals := make(map[string]string)
  minority_num := int(nreplicas/2)
  for i := 0; i < minority_num; i++ {
    fmt.Printf("deafen replica %+v\n",i)
    sa[rand_group][i].deaf = true
  }
  // 50+ puts
  num_keys := 25
  for _, v := range shrds {
    for j := 0; j < num_keys; j++ {
      arr := []byte(strconv.Itoa(rand.Intn(math.MaxInt32)))
      arr[0] = byte(v + 100)
      key := string(arr[:len(arr)])
      val := string(arr[:])
      r_vals[key] = val
      fmt.Printf("ck.Put, shard %+v, key %+v, val %+v\n",key2shard(key),key,val)
      ck.Put(key, val)
    }
  }
  // one non-deaf peer crashes and loses disk
  rand_crash := minority_num + rand.Intn(nreplicas-minority_num)
  sa[rand_group][rand_crash].kill()
  fmt.Printf("kill replica %+v\n",rand_crash)
  kv_db := db_path + "s" + strconv.Itoa(sa[rand_group][rand_crash].uid) + "_" + strconv.Itoa(int(gids[rand_group])) + "_kvstore.db"
  mencius_db := db_path + "s" + strconv.Itoa(sa[rand_group][rand_crash].uid) + "_menciusstore.db"
  err := os.Remove(kv_db)
  if err != nil {
    t.Fatalf("Failed to remove database %+v\n",kv_db)
  }
  fmt.Printf("removing %+v\n",kv_db)
  err = os.Remove(mencius_db)
  if err != nil {
    t.Fatalf("Failed to remove database %+v\n",mencius_db)
  }
  fmt.Printf("removing %+v\n",mencius_db)
  // make all peers non-deaf and restart
  for i := 0; i < minority_num; i++ {
    sa[rand_group][i].deaf = false
  }
  //for i := 0; i < common.NShards; i++ {
    //sa[rand_group][rand_crash].kvmap[i] = make(map[string]string)
  //}
  //for i, _ := range sa[rand_group] {
    //fmt.Printf("sa[%+v].kvmap: %+v\n",i,sa[rand_group][i].kvmap)
  //}
  RestartReplica(rand_group, rand_crash, gids, smh, ha, sa, uids)
  // check that all peers in the replica group are consistent
  keys_total := 0
  keys_pass := 0
  for k, _ := range r_vals {
    keys_total++
    if r_vals[k] == ck.Get(k) {
      keys_pass++
    }
  }
  if keys_total != keys_pass {
    t.Fatalf("Servers are missing %+v keys of %+v\n",keys_total-keys_pass,keys_total)
  }
  for i,_ := range sa[rand_group] {
    if reflect.DeepEqual(sa[rand_group][0].kvmap,sa[rand_group][i].kvmap) {
      fmt.Printf("Replica %+v passed consistency check\n",i)
    } else {
      t.Fatalf("Replica %+v failed consistency check\n",i)
    }
  }
}

func TestCornerCaseDiskCrash(t *testing.T) {
  if test_persistence {
    fmt.Printf("Test: Corner case disk failure ...\n")
    cornerCrash(t, false)
    fmt.Printf("  ... Passed\n")
  }
}

func TestCornerCaseDiskCrashUnreliable(t *testing.T) {
  if test_persistence {
    fmt.Printf("Test: Corner case disk failure (unreliable) ...\n")
    cornerCrash(t, true)
    fmt.Printf("  ... Passed\n")
  }
}

func allCrash(t *testing.T, large_puts bool, unreliable bool) {
  smh, gids, ha, sa, clean, uids := setup("allcrash"+strconv.FormatBool(large_puts)+strconv.FormatBool(unreliable), unreliable)
  defer clean()

  mck := shardmaster.MakeClerk(smh)
  for i := 0; i < len(gids); i++ {
    mck.Join(gids[i], ha[i])
  }
  ck := MakeClerk(smh)

  // puts to all replica groups
  rand.Seed(time.Now().Unix())
  vals_before := make(map[string]string) // before crash
  vals_after := make(map[string]string) // after crash

  num_keys := 10
  data_size := hundred_mb // 100 MB
  if large_puts {
    num_keys = 3
  }

  for v := 0; v < common.NShards; v++ {
    for j := 0; j < num_keys; j++ {
      arr := []byte(strconv.Itoa(rand.Intn(math.MaxInt32)))
      key_len := len(arr)
      if large_puts {
        arr = make([]byte, data_size)
        for j := 1; j < len(arr); j++ {
          arr[j] = byte('a' + rand.Int() % 26)
        }
        fmt.Printf("shard %+v put large key %+v of %+v\n",v,j+1,num_keys)
      }
      arr[0] = byte(v + 100)
      key := string(arr[:key_len])
      val := string(arr[:])
      vals_before[key] = val
      //fmt.Printf("ck.Put, shard %+v, key %+v, val %+v\n",key2shard(key),key,val)
      ck.Put(key, val)
    }
  }
  time.Sleep(1 * time.Second)
  // all servers crash
  for i := 0; i < ngroups; i++ {
    for j := 0; j < nreplicas; j++ {
      sa[i][j].kill()
      fmt.Printf("kill group %+v replica %+v\n",i,j)
    }
  }
  time.Sleep(1 * time.Second)
  // bring servers back up
  for i := 0; i < ngroups; i++ {
    for j := 0; j < nreplicas; j++ {
      RestartReplica(i, j, gids, smh, ha, sa, uids)
    }
  }
  time.Sleep(3 * time.Second)
  for v := 0; v < common.NShards; v++ {
    for j := 0; j < num_keys; j++ {
      arr := []byte(strconv.Itoa(rand.Intn(math.MaxInt32)))
      key_len := len(arr)
      if large_puts {
        arr = make([]byte, data_size)
        for j := 1; j < len(arr); j++ {
          arr[j] = byte('a' + rand.Int() % 26)
        }
      }
      arr[0] = byte(v + 100)
      key := string(arr[:key_len])
      val := string(arr[:])
      vals_after[key] = val
      //fmt.Printf("ck.Put, shard %+v, key %+v, val %+v\n",key2shard(key),key,val)
      ck.Put(key, val)
    }
  }
  time.Sleep(3 * time.Second) // time for background thread processing
  // check that all puts exist on crashed servers
  total := 0
  pass := 0
  for k, _ := range vals_before {
    total++
    if vals_before[k] == ck.Get(k) {
      pass++
    }
  }
  for k, _ := range vals_after {
    total++
    if vals_after[k] == ck.Get(k) {
      pass++
    }
  }
  if total != pass {
    t.Fatalf("Servers are missing %+v keys of %+v\n",total-pass,total)
  }
  if !unreliable {
    // consistency check
    for i, _ := range sa {
      for j,_ := range sa[i] {
        if reflect.DeepEqual(sa[i][0].kvmap,sa[i][j].kvmap) {
          fmt.Printf("Group %+v replica %+v passed consistency check\n",i,j)
        } else {
          t.Fatalf("Group %+v replica %+v failed consistency check\n",i,j)
        }
      }
    }
  }
}

func TestAllCrash(t *testing.T) {
  if test_persistence {
    fmt.Printf("Test: All servers crash ...\n")
    allCrash(t, false, false)
    fmt.Printf("  ... Passed\n")
  }
}

func TestAllCrashUnreliable(t *testing.T) {
  if test_persistence {
    fmt.Printf("Test: All servers crash (unreliable) ...\n")
    allCrash(t, false, true)
    fmt.Printf("  ... Passed\n")
  }
}

func TestAllLargeCrash(t *testing.T) {
  if test_persistence {
    fmt.Printf("Test: All servers crash with GBs of data ...\n")
    allCrash(t, true, false)
    fmt.Printf("  ... Passed\n")
  }
}

func TestAllLargeCrashUnreliable(t *testing.T) {
  if test_persistence {
    fmt.Printf("Test: All servers crash with GBs of data (unreliable) ...\n")
    allCrash(t, true, true)
    fmt.Printf("  ... Passed\n")
  }
}

func keysInMajority(t *testing.T, r_vals map[string]string, sa [][]*ShardKV, rand_group int, cfg common.Config, gids []int64) {
  keys_total := 0
  keys_pass := 0
  count := 0
  fmt.Printf("Keys still on majority: [")
  for k, v := range r_vals {
    shard_num := key2shard(k)
    if cfg.Shards[shard_num] == gids[rand_group] {
      keys_total++
      count = 0
      for i, _ := range sa[rand_group] {
        if sa[rand_group][i].kvmap[shard_num][k] == v {
          count++
        }
      }
      if count > int(nreplicas/2) {
        fmt.Printf(" y ")
        keys_pass++
      } else {
        fmt.Printf(" n ")
      }
    }
  }
  fmt.Printf("]\n")
  if keys_total != keys_pass {
    t.Fatalf("Majority is missing %+v keys of %+v\n",keys_total-keys_pass,keys_total)
  }
}

func massiveDataTest(t *testing.T, unreliable bool) {
  smh, gids, ha, sa, clean, uids := setup("massive"+strconv.FormatBool(unreliable), unreliable)
  defer clean()

  mck := shardmaster.MakeClerk(smh)
  for i := 0; i < len(gids); i++ {
    mck.Join(gids[i], ha[i])
  }
  ck := MakeClerk(smh)

  unreliable_str := " "
  if unreliable {
    unreliable_str = " (unreliable) "
  }

  fmt.Printf("Test: (Massive) Writing 300GB to disk%+v...\n",unreliable_str)
  // store 10GB per shard (x10 shards x3 replicas = 300GB)
  rand.Seed(time.Now().Unix())
  cfg := mck.Query(-1)
  num_keys := 1//100 // to test local set to 1
  data_size := ten_mb//hundred_mb // to test local set to ten_mb
  // use random sampling to test that keys are present
  r_vals := make(map[string]string)
  r_sample := make(map[int]bool)
  for r := 0; r < 3; r++ {
    // assumes at least 3GB of RAM (100MB x10 shards x3)
    r_sample[rand.Intn(num_keys)] = true
  }
  for v := 0; v < common.NShards; v++ {
    for j := 0; j < num_keys; j++ {
      arr := make([]byte, data_size)
      key_len := len(arr)
      for k := 1; k < len(arr); k++ {
        arr[k] = byte('a' + rand.Int() % 26)
      }
      arr[0] = byte(v + 100)
      key := string(arr[:key_len])
      val := string(arr[:])
      fmt.Printf("Shard %+v massive put %+v of %+v\n",v,j+1,num_keys)
      if r_sample[j] {
        r_vals[key] = val
        fmt.Printf("Saved massive put %+v of %+v\n",j+1,num_keys)
      }
      ck.Put(key, val)
    }
  }
  fmt.Printf("  ... Passed\n")

  fmt.Printf("Test: (Massive) Survives single crash%+v...\n",unreliable_str)

  rand_group := rand.Intn(ngroups)
  rand_server := rand.Intn(nreplicas)
  time.Sleep(7 * time.Second)
  fmt.Printf("kill group %+v replica %+v\n",rand_group,rand_server)
  sa[rand_group][rand_server].kill()
  time.Sleep(7 * time.Second)
  RestartReplica(rand_group, rand_server, gids, smh, ha, sa, uids)
  time.Sleep(7 * time.Second)
  if !unreliable {
    keys_total := 0
    keys_found := 0
    fmt.Printf("Keys still on replica %+v: [",rand_server)
    for k, _ := range r_vals {
      shard_num := key2shard(k)
      if cfg.Shards[shard_num] == gids[rand_group] {
        keys_total++
        if r_vals[k] == sa[rand_group][rand_server].kvmap[shard_num][k] {
          fmt.Printf(" y ")
          keys_found++
        } else {
          fmt.Printf(" n ")
        }
      }
    }
    fmt.Printf("]\n")
    if keys_total != keys_found {
      t.Fatalf("Restarted replica is missing %+v keys of %+v\n",keys_total-keys_found,keys_total)
    }
    for i, _ := range sa[rand_group] {
      // just check length as a rough consist. check
      if i != rand_server {
        if len(sa[rand_group][rand_server].kvmap) == len(sa[rand_group][i].kvmap) {
          fmt.Printf("Replica %+v passed consistency check\n",i)
        } else {
          t.Fatalf("Replica %+v failed consistency check\n",i)
        }
      }
    }
  } else {
    // unreliable
    keysInMajority(t, r_vals, sa, rand_group, cfg, gids)
  }
  
  fmt.Printf("  ... Passed\n")

  fmt.Printf("Test: (Massive) Survives single crash with disk failure%+v...\n",unreliable_str)

  rand_group = rand.Intn(ngroups)
  rand_server = rand.Intn(nreplicas)
  fmt.Printf("kill group %+v replica %+v\n",rand_group,rand_server)
  time.Sleep(7 * time.Second)
  sa[rand_group][rand_server].kill()
  time.Sleep(7 * time.Second)
  kv_db := db_path + "s" + strconv.Itoa(sa[rand_group][rand_server].uid) + "_" + strconv.Itoa(int(gids[rand_group])) + "_kvstore.db"
  mencius_db := db_path + "s" + strconv.Itoa(sa[rand_group][rand_server].uid) + "_menciusstore.db"
  err := os.Remove(kv_db)
  if err != nil {
    t.Fatalf("Failed to remove database %+v\n",kv_db)
  }
  fmt.Printf("removing %+v\n",kv_db)
  err = os.Remove(mencius_db)
  if err != nil {
    t.Fatalf("Failed to remove database %+v\n",mencius_db)
  }
  fmt.Printf("removing %+v\n",mencius_db)
  time.Sleep(7 * time.Second)
  RestartReplica(rand_group, rand_server, gids, smh, ha, sa, uids)
  time.Sleep(7 * time.Second)
  if !unreliable {
    keys_total := 0
    keys_found := 0
    fmt.Printf("Keys still on replica %+v: [",rand_server)
    for k, _ := range r_vals {
      shard_num := key2shard(k)
      if cfg.Shards[shard_num] == gids[rand_group] {
        keys_total++
        if r_vals[k] == sa[rand_group][rand_server].kvmap[shard_num][k] {
          fmt.Printf(" y ")
          keys_found++
        } else {
          fmt.Printf(" n ")
        }
      }
    }
    fmt.Printf("]\n")
    if keys_total != keys_found {
      t.Fatalf("Restarted replica is missing %+v keys of %+v\n",keys_total-keys_found,keys_total)
    }
    for i, _ := range sa[rand_group] {
      // just check length as a rough consist. check
      if i != rand_server {
        if len(sa[rand_group][rand_server].kvmap) == len(sa[rand_group][i].kvmap) {
          fmt.Printf("Replica %+v passed consistency check\n",i)
        } else {
          t.Fatalf("Replica %+v failed consistency check\n",i)
        }
      }
    }
  } else {
    // unreliable
    keysInMajority(t, r_vals, sa, rand_group, cfg, gids)
  }

  fmt.Printf("  ... Passed\n")

  fmt.Printf("Test: (Massive) Survives crash of all servers%+v...\n",unreliable_str)

  for i := 0; i < ngroups; i++ {
    for j := 0; j < nreplicas; j++ {
      sa[i][j].kill()
      fmt.Printf("kill group %+v replica %+v\n",i,j)
    }
  }
  time.Sleep(7 * time.Second)
  // bring servers back up
  for i := 0; i < ngroups; i++ {
    for j := 0; j < nreplicas; j++ {
      RestartReplica(i, j, gids, smh, ha, sa, uids)
    }
  }
  time.Sleep(7 * time.Second)
  keys_total := 0
  keys_found := 0
  for k, _ := range r_vals {
    keys_total++
    if r_vals[k] == ck.Get(k) {
      keys_found++
    }
  }
  if keys_total != keys_found {
    t.Fatalf("Servers are missing %+v keys of %+v\n",keys_total-keys_found,keys_total)
  }
  for i, _ := range sa {
    for j,_ := range sa[i] {
      // just check length as a rough consist. check
      if len(sa[i][0].kvmap) == len(sa[i][j].kvmap) {
        fmt.Printf("Group %+v replica %+v passed consistency check\n",i,j)
      } else {
        t.Fatalf("Group %+v replica %+v failed consistency check\n",i,j)
      }
    }
  }

  fmt.Printf("  ... Passed\n")
}

func TestMassiveData(t *testing.T) {
  // WARNING: This test is likely to fail locally unless you change num_keys
  // to a much lower value (e.g., 1). You must specify a higher timeout value
  // for this test (e.g., -timeout 25200s).
  if test_persistence {
    massiveDataTest(t, false)
  }
}

func TestMassiveDataUnreliable(t *testing.T) {
  // WARNING: This test is likely to fail locally unless you change num_keys
  // to a much lower value (e.g., 1). You must specify a higher timeout value
  // for this test (e.g., -timeout 25200s).
  if test_persistence {
    massiveDataTest(t, true)
  }
}

func port(tag string, host int) string {
  s := "/var/tmp/824-"
  s += strconv.Itoa(os.Getuid()) + "/"
  os.Mkdir(s, 0777)
  s += "skv-"
  s += strconv.Itoa(os.Getpid()) + "-"
  s += tag + "-"
  s += strconv.Itoa(host)
  return s
}

func NextValue(hprev string, val string) string {
  h := common.Hash(hprev + val)
  return strconv.Itoa(int(h))
}

func mcleanup(sma []*shardmaster.ShardMaster) {
  for i := 0; i < len(sma); i++ {
    if sma[i] != nil {
      sma[i].Kill()
    }
  }
}

func cleanup(sa [][]*ShardKV) {

  for i := 0; i < len(sa); i++ {
    for j := 0; j < len(sa[i]); j++ {
      sa[i][j].kill()
    }
  }
  os.RemoveAll(fmt.Sprintf(db_path))
}

func setup(tag string, unreliable bool) ([]string, []int64, [][]string, [][]*ShardKV, func(), [][]int) {
  runtime.GOMAXPROCS(4)
  os.Mkdir("db", 0777)
  const nmasters = 3
  var sma []*shardmaster.ShardMaster = make([]*shardmaster.ShardMaster, nmasters)
  var smh []string = make([]string, nmasters)
  // defer mcleanup(sma)
  for i := 0; i < nmasters; i++ {
    smh[i] = port(tag+"m", i)
  }
  for i := 0; i < nmasters; i++ {
    sma[i] = shardmaster.StartServer(smh, i)
  }

  //const ngroups = 3   // replica groups
  //const nreplicas = 3 // servers per group
  gids := make([]int64, ngroups)    // each group ID
  ha := make([][]string, ngroups)   // ShardKV ports, [group][replica]
  sa := make([][]*ShardKV, ngroups) // ShardKVs
  uids := make([][]int, ngroups) // uids
  // defer cleanup(sa)
  for i := 0; i < ngroups; i++ {
    gids[i] = int64(i + 100)
    sa[i] = make([]*ShardKV, nreplicas)
    ha[i] = make([]string, nreplicas)
    uids[i] = make([]int, nreplicas)
    for j := 0; j < nreplicas; j++ {
      uids[i][j] = int(common.Nrand())
      ha[i][j] = port(tag+"s", (i*nreplicas)+j)
    }
    for j := 0; j < nreplicas; j++ {
      sa[i][j] = StartServer(gids[i], smh, ha[i], uids[i][j], j, false /**normal start **/)
      sa[i][j].unreliable = unreliable
    }
  }

  clean := func() { cleanup(sa) ; mcleanup(sma) }
  return smh, gids, ha, sa, clean, uids
}

func TestBasic(t *testing.T) {
  smh, gids, ha, _, clean, _ := setup("basic", false)
  defer clean()

  fmt.Printf("Test: Basic Join/Leave ...\n")

  mck := shardmaster.MakeClerk(smh)
  mck.Join(gids[0], ha[0])

  ck := MakeClerk(smh)

  ck.Put("a", "x")
  v := ck.PutHash("a", "b")
  if v != "x" {
    t.Fatalf("Puthash got wrong value")
  }
  ov := NextValue("x", "b")
  if ck.Get("a") != ov {
    t.Fatalf("Get got wrong value")
  }

  keys := make([]string, 10)
  vals := make([]string, len(keys))
  for i := 0; i < len(keys); i++ {
    keys[i] = strconv.Itoa(rand.Int())
    vals[i] = strconv.Itoa(rand.Int())
    ck.Put(keys[i], vals[i])
  }

  // are keys still there after joins?
  for g := 1; g < len(gids); g++ {
    mck.Join(gids[g], ha[g])
    time.Sleep(1 * time.Second)
    for i := 0; i < len(keys); i++ {
      v := ck.Get(keys[i])
      if v != vals[i] {
        t.Fatalf("joining; wrong value; g=%v k=%v wanted=%v got=%v",
          g, keys[i], vals[i], v)
      }
      vals[i] = strconv.Itoa(rand.Int())
      ck.Put(keys[i], vals[i])
    }
  }
  // are keys still there after leaves?
  for g := 0; g < len(gids)-1; g++ {
    mck.Leave(gids[g])
    time.Sleep(1 * time.Second)
    for i := 0; i < len(keys); i++ {
      v := ck.Get(keys[i])
      if v != vals[i] {
        t.Fatalf("leaving; wrong value; g=%v k=%v wanted=%v got=%v",
          g, keys[i], vals[i], v)
      }
      vals[i] = strconv.Itoa(rand.Int())
      ck.Put(keys[i], vals[i])
    }
  }

  fmt.Printf("  ... Passed\n")
}

func TestMove(t *testing.T) {
  smh, gids, ha, _, clean, _ := setup("move", false)
  defer clean()

  fmt.Printf("Test: Shards really move ...\n")

  mck := shardmaster.MakeClerk(smh)
  mck.Join(gids[0], ha[0])

  ck := MakeClerk(smh)

  // insert one key per shard
  for i := 0; i < common.NShards; i++ {
    ck.Put(string('0'+i), string('0'+i))
  }

  // add group 1.
  mck.Join(gids[1], ha[1])
  time.Sleep(5 * time.Second)
  
  // check that keys are still there.
  for i := 0; i < common.NShards; i++ {
    actual := ck.Get(string('0'+i))
    if actual != string('0'+i) {
      t.Fatalf("missing key/value expected= %s actual= %s", string('0'+i), actual)
    }
  }

  // remove sockets from group 0.
  for i := 0; i < len(ha[0]); i++ {
    os.Remove(ha[0][i])
  }

  count := 0
  var mu sync.Mutex
  for i := 0; i < common.NShards; i++ {
    go func(me int) {
      myck := MakeClerk(smh)
      v := myck.Get(string('0'+me))
      if v == string('0'+me) {
        mu.Lock()
        count++
        mu.Unlock()
      } else {
        t.Fatalf("Get(%v) yielded %v\n", i, v)
      }
    }(i)
  }

  time.Sleep(10 * time.Second)

  if count > common.NShards / 3 && count < 2*(common.NShards/3) {
    fmt.Printf("  ... Passed\n")
  } else {
    t.Fatalf("%v keys worked after killing 1/2 of groups; wanted %v",
      count, common.NShards / 2)
  }

}

func TestLimp(t *testing.T) {
  smh, gids, ha, sa, clean, _ := setup("limp", false)
  defer clean()

  fmt.Printf("Test: Reconfiguration with some dead replicas ...\n")

  mck := shardmaster.MakeClerk(smh)
  mck.Join(gids[0], ha[0])

  ck := MakeClerk(smh)

  ck.Put("a", "b")
  if ck.Get("a") != "b" {
    t.Fatalf("got wrong value")
  }

  for g := 0; g < len(sa); g++ {
    sa[g][rand.Int() % len(sa[g])].kill()
  }

  keys := make([]string, 10)
  vals := make([]string, len(keys))
  for i := 0; i < len(keys); i++ {
    keys[i] = strconv.Itoa(rand.Int())
    vals[i] = strconv.Itoa(rand.Int())
    ck.Put(keys[i], vals[i])
  }

  // are keys still there after joins?
  for g := 1; g < len(gids); g++ {
    mck.Join(gids[g], ha[g])
    time.Sleep(1 * time.Second)
    for i := 0; i < len(keys); i++ {
      v := ck.Get(keys[i])
      if v != vals[i] {
        t.Fatalf("joining; wrong value; g=%v k=%v wanted=%v got=%v",
          g, keys[i], vals[i], v)
      }
      vals[i] = strconv.Itoa(rand.Int())
      ck.Put(keys[i], vals[i])
    }
  }
  
  // are keys still there after leaves?
  for g := 0; g < len(gids)-1; g++ {
    mck.Leave(gids[g])
    time.Sleep(2 * time.Second)
    for i := 0; i < len(sa[g]); i++ {
      sa[g][i].kill()
    }
    for i := 0; i < len(keys); i++ {
      v := ck.Get(keys[i])
      if v != vals[i] {
        t.Fatalf("leaving; wrong value; g=%v k=%v wanted=%v got=%v",
          g, keys[i], vals[i], v)
      }
      vals[i] = strconv.Itoa(rand.Int())
      ck.Put(keys[i], vals[i])
    }
  }

  fmt.Printf("  ... Passed\n")
}

func doConcurrent(t *testing.T, unreliable bool) {
  smh, gids, ha, _, clean, _ := setup("conc"+strconv.FormatBool(unreliable), unreliable)
  defer clean()

  mck := shardmaster.MakeClerk(smh)
  for i := 0; i < len(gids); i++ {
    mck.Join(gids[i], ha[i])
  }

  const npara = 11
  var ca [npara]chan bool
  for i := 0; i < npara; i++ {
    ca[i] = make(chan bool)
    go func(me int) {
      ok := true
      defer func() { ca[me] <- ok }()
      ck := MakeClerk(smh)
      mymck := shardmaster.MakeClerk(smh)
      key := strconv.Itoa(me)
      last := ""
      for iters := 0; iters < 3; iters++ {
        nv := strconv.Itoa(rand.Int())
        v := ck.PutHash(key, nv)
        if v != last {
          ok = false
          t.Fatalf("PutHash(%v) expected %v got %v\n", key, last, v)
        }
        last = NextValue(last, nv)
        v = ck.Get(key)
        if v != last {
          ok = false
          t.Fatalf("Get(%v) expected %v got %v\n", key, last, v)
        }

        mymck.Move(rand.Int() % common.NShards,
          gids[rand.Int() % len(gids)])

        time.Sleep(time.Duration(rand.Int() % 30) * time.Millisecond)
      }
    }(i)
  }

  for i := 0; i < npara; i++ {
    x := <- ca[i]
    if x == false {
      t.Fatalf("something is wrong")
    }
  }

}

func TestConcurrent(t *testing.T) {
  fmt.Printf("Test: Concurrent Put/Get/Move ...\n")
  doConcurrent(t, false)

  fmt.Printf("  ... Passed\n")
}

func TestConcurrentUnreliable(t *testing.T) {
  fmt.Printf("Test: Concurrent Put/Get/Move (unreliable) ...\n")
  doConcurrent(t, true)
  fmt.Printf("  ... Passed\n")
}
