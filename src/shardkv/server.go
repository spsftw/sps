package shardkv

import "net"
import "fmt"
import "net/rpc"
import "log"
import "time"
import "mencius"
import "sync"
import "os"
import "syscall"
import "encoding/gob"
import "math/rand"
import "shardmaster"
import "strconv"
import "common"
import "persistence"
import "bytes"

//
// Lab4B using Mencius.
//

const Debug=0

func DPrintf(format string, a ...interface{}) (n int, err error) {
  if Debug > 0 {
     log.Printf(format, a...)
  }
  return
}

type ShardKV struct {
  mu sync.Mutex
  l net.Listener
  me int
  dead bool // for testing
  unreliable bool // for testing
  sm *shardmaster.Clerk
  px *mencius.Paxos
  rpcCount int

  gid int64 // my replica group ID

  // Your definitions here.
  config *common.Config  // current configuration
  processed int // processed instance number
  kvmap [common.NShards] map[string]string // shard to kvmap
  replymap [common.NShards] map[int64] *common.KVReply // shard to replymap
  proposed int
  servers []string
  restart bool
  //persistence variables
  kvReqStore *persistence.KvReqStore
  dirtyMap map[int]map[string]string
  dirtyReqs map[int]map[int64]*common.KVReply
  uid int
  lastStore int
  // timing variables
  lastTickMax int
  lastTickProcessed int
  upToDate bool
  // for deafness
  deaf bool
}


func (kv *ShardKV) Get(args *common.GetArgs, reply *common.GetReply) error {
  // DPrintf("gid: %d GET KEY %s", kv.gid, args.Key)
  kv.mu.Lock()
  defer kv.mu.Unlock()

  var instance int
  instance = kv.NextIndex()//kv.px.Max() + 1
  var op common.ShardKVOp
  op = common.ShardKVOp{Cmd:"Get", Key: args.Key, ClientId: args.ClientId, Sn: args.Sn}
  kv.px.Start(instance, op)
  // wait for agreement
  currentOp := kv.WaitForAgreement(instance)
  if currentOp == nil {  // paxos dead
     reply.Err = common.ErrWrongGroup
     return nil
  }
  for currentOp.Sn != op.Sn {
     instance = kv.NextIndex()//kv.px.Max() + 1
     kv.px.Start(instance, op)
     currentOp = kv.WaitForAgreement(instance)
     if currentOp == nil {
        reply.Err = common.ErrWrongGroup
        return nil
     }
  }
  kv.mu.Unlock()
  // check whether instances before that has been processed
  for kv.processed < instance {
      time.Sleep(10 * time.Millisecond)
  }

  kv.mu.Lock()
  shard := key2shard(args.Key)
  processedReply, processedReplyExisted := kv.replymap[shard][args.Sn]
  if !processedReplyExisted {
     reply.Err = common.ErrWrongGroup
     DPrintf("me %d gid %d GET Sn %d returning common.ErrWrongGroup", kv.me, kv.gid, args.Sn)
  } else {
     reply.Value = processedReply.Value
     reply.Err = processedReply.Err
  }
  return nil
}

func (kv *ShardKV) Transfer(args *common.TransferArgs, reply *common.TransferReply) error {
  kv.mu.Lock()
  defer kv.mu.Unlock()
  if kv.config.Num >= args.ConfigNum {
     reply.KVMap = make(map[int] map[string] string)
     reply.ReplyMap = make (map[int] map[int64] *common.KVReply)
     var logShards [common.NShards] int
     for i, s := range args.Shards {
        if s == 1 {
           reply.KVMap[i] = make(map[string] string)
           reply.ReplyMap[i] = make(map[int64] *common.KVReply)
           for mapkey, mapvalue := range kv.kvmap[i] {
               reply.KVMap[i][mapkey] = mapvalue
           }

           for sn, storedreply := range kv.replymap[i] {
               reply.ReplyMap[i][sn] = storedreply
           }
           logShards[i] = i
        }
    }
    DPrintf("OK me %d gid %d ok want shard %d config %d. now %d", kv.me, kv.gid, logShards, args.ConfigNum, kv.config.Num)
    reply.Err = common.OK
  } else {
    DPrintf("NO me %d dead %v gid %d  want %d. now %d", kv.me, kv.dead, kv.gid, args.ConfigNum, kv.config.Num)
    reply.Err = common.ErrWait
  }
  return nil
}

func (kv *ShardKV) checkServer(shard int) bool {
  return kv.config.Shards[shard] == kv.gid
}

func (kv *ShardKV) requestShards(config *common.Config, transferreply *common.TransferReply) {
  DPrintf("RequestShards me %d gid %d New config %d old config %d", kv.me, kv.gid, config.Num, kv.config.Num)
  if kv.config.Num > 0 { // if kv.config.Num == 0, no need for state transfer
    var oldguy int64
    var oldguyshards [common.NShards]int
    for i, gid := range kv.config.Shards {
        newgid := config.Shards[i]
        if gid != kv.gid && newgid == kv.gid {
           if oldguy != 0 && oldguy != gid { log.Printf("Error in assumption about shardmaster") }
           if oldguy == 0 { oldguy = gid }
           oldguyshards[i] = 1
        }
    }
    if oldguy != 0 {
       kv.getShards(oldguy, oldguyshards, config.Num, transferreply)
    }
  }
}

func (kv *ShardKV) getShards(gid int64, shards [common.NShards]int, configNum int, transferreply *common.TransferReply) {
  DPrintf("GetShards me %d gid %d New config %d old config %d", kv.me, kv.gid, configNum, kv.config.Num)
  transferargs := common.TransferArgs{Shards: shards, ConfigNum: configNum}
  okToBreak := false
  for !okToBreak && !kv.dead {
    for i, s := range kv.config.Groups[gid] {
      DPrintf("me %d gid %d New config %d old config %d asking for %d %d shard %d", kv.me, kv.gid, configNum, kv.config.Num, gid, i, shards)
      ok := call(s, "ShardKV.Transfer", &transferargs, transferreply)
      okToBreak = (ok && transferreply.Err == common.OK)
      if okToBreak {
         break
      }
      if transferreply.Err == common.ErrWait {
         time.Sleep(10 * time.Millisecond)
      }
     }
   }
}

func (kv *ShardKV) getKVProcessed(processed []int) {
  queryKVProcessedArgs := common.QueryKVProcessedArgs{}
  queryKVProcessedReply := common.QueryKVProcessedReply{}
  rec := make([]bool, len(kv.servers))
  ok := false
  for !kv.dead {
    for i, s := range kv.servers {
        if i == kv.me {
          rec[i] = true
          continue
        }
        ok = call(s, "ShardKV.QueryKVProcessed", &queryKVProcessedArgs, &queryKVProcessedReply)
        //fmt.Printf("Returned from QueryKVProcessed call to %+v: %+v\n",i,queryKVProcessedReply)
        if ok {
          processed[i] = queryKVProcessedReply.Processed
          rec[i] = true
        }
    }
    count := 0
    for _, v := range rec {
        if v == true {
          count++
        }
    }
    if count >= len(kv.servers) {
      break
    }
  }
}

func (kv *ShardKV) QueryKVProcessed(args *common.QueryKVProcessedArgs, reply *common.QueryKVProcessedReply) error {
  kv.mu.Lock()
  defer kv.mu.Unlock()
  reply.Processed = kv.processed
  reply.Err = common.OK
  return nil
}

func (kv *ShardKV) getPXMax(maxes []int) {
  queryPXMaxArgs := common.QueryPXMaxArgs{}
  queryPXMaxReply := common.QueryPXMaxReply{}
  rec := make([]bool, len(kv.servers))
  ok := false
  for !kv.dead {
    for i, s := range kv.servers {
        if i == kv.me {
          rec[i] = true
          continue
        }
        ok = call(s, "ShardKV.QueryPXMax", &queryPXMaxArgs, &queryPXMaxReply)
        fmt.Printf("Returned from QueryPXMax call to %+v: %+v\n",i,queryPXMaxReply)
        if ok {
          maxes[i] = queryPXMaxReply.Max
          rec[i] = true
        }
    }
    count := 0
    for _, v := range rec {
        if v == true {
          count++
        }
    }
    //fmt.Printf("count: %+v, len(kv.servers): %+v, rec: %+v\n",count,len(kv.servers),rec)
    if count >= len(kv.servers) {
      break
    }
  }
}

func (kv *ShardKV) QueryPXMax(args *common.QueryPXMaxArgs, reply *common.QueryPXMaxReply) error {
  kv.mu.Lock()
  defer kv.mu.Unlock()
  reply.Max = kv.px.Max()
  reply.Err = common.OK
  return nil
}

func (kv *ShardKV) moveMax(gMax int) {
  moveMaxArgs := common.MoveMaxArgs{MaxGlobal: gMax}
  moveMaxReply := common.MoveMaxReply{}
  rec := make([]bool, len(kv.servers))
  ok := false
  for !kv.dead {
    for i, s := range kv.servers {
        if i == kv.me {
          rec[i] = true
          continue
        }
        ok = call(s, "ShardKV.MoveMax", &moveMaxArgs, &moveMaxReply)
        fmt.Printf("Returned from MoveMax call to %+v: %+v\n",i,moveMaxReply)
        if ok {
          rec[i] = true
        }
    }
    count := 0
    for _, v := range rec {
        if v == true {
          count++
        }
    }
    if count >= len(kv.servers) {
        break
    }
  }
}

func (kv *ShardKV) MoveMax(args *common.MoveMaxArgs, reply *common.MoveMaxReply) error {
  kv.mu.Lock()
  defer kv.mu.Unlock()
  if args.MaxGlobal > kv.px.Max() {
    kv.px.SetMax(args.MaxGlobal)
  }
  reply.Err = common.OK
  return nil
}

func (kv *ShardKV) getDB(transferDBReply *common.TransferDBReply) {
  transferDBArgs := common.TransferDBArgs{}
  ok := false
  for !kv.dead && !ok{
    for _, s := range kv.servers {
        ok = call(s, "ShardKV.TransferDB", &transferDBArgs, transferDBReply)
        if ok {
           break
        }
    }
  }
}

func (kv *ShardKV) TransferDB(args *common.TransferDBArgs, reply *common.TransferDBReply) error {
  kv.mu.Lock()
  defer kv.mu.Unlock()
  reply.KVMap = make(map[int]map[string]string)
  reply.ReplyMap = make (map[int]map[int64] *common.KVReply)
  fmt.Println("Transferring from DB")
  for s := range(kv.kvmap){
    kvmap, err := kv.kvReqStore.KvReadAll(s)
    if(err == "DbOk"){
      reply.KVMap[s] = kvmap
    }
  }

  for s := range kv.replymap {
    replymap, err := kv.kvReqStore.ReqReadAll(s)
    if(err == "DbOk"){
      reply.ReplyMap[s] = replymap
    }
  }
  processed, err := kv.kvReqStore.MDataGetValue()
  if(err == "DbOk"){
    reply.Processed = processed
  }
  cfg, err := kv.kvReqStore.ConfigGetValue()
  if(err == "DbOk"){
    // decode
    var config common.Config
    buf := bytes.NewBuffer(cfg)
    dec := gob.NewDecoder(buf)
    dec.Decode(&config)
    reply.Config = &config
  }
  fmt.Println("Transferring from DB COMPLETED")
  //fmt.Printf("KVMap %v\n ReplyMap %v \n Processed %v \n Config %v \n", reply.KVMap, reply.ReplyMap, reply.Processed, reply.Config)
  return nil
}

func (kv *ShardKV) updateStates(transferreply *common.TransferReply) {
     // log.Printf("me %d gid %d update shard %d ", kv.me, kv.gid, shard) 
     var logShards [common.NShards] int
     for shard, shardkvmap := range transferreply.KVMap {
       for mapkey, mapvalue := range shardkvmap {
           kv.kvmap[shard][mapkey] = mapvalue
           // log.Printf("me %d gid %d shard %d key %s value %s", kv.me, kv.gid, shard, mapkey, mapvalue)

           kv.dirtyMap[shard][mapkey] = mapvalue
       }
       logShards[shard] = shard
     }
     for shard, shardreplymap := range transferreply.ReplyMap {
       for sn, reply := range shardreplymap {
           kv.replymap[shard][sn] = reply
           kv.dirtyReqs[shard][sn] = reply
       }
     }
     DPrintf("me %d gid %d finish update shards %d", kv.me, kv.gid, logShards) 
}

func (kv *ShardKV) Put(args *common.PutArgs, reply *common.PutReply) error {
  kv.mu.Lock()
  defer  kv.mu.Unlock()

 // log.Printf("me %d gid: %d PUT KEY %s VALUE %s Sn: %d", kv.uid, kv.gid, args.Key, args.Value, args.Sn)
  var instance int
  instance = kv.NextIndex()//kv.px.Max() + 1
  var op common.ShardKVOp
  if args.DoHash {
     op = common.ShardKVOp{Cmd:"PutHash", Key: args.Key, ClientId: args.ClientId, Sn: args.Sn, Value: args.Value}
  } else {
     op = common.ShardKVOp{Cmd:"Put", Key: args.Key, ClientId: args.ClientId, Sn: args.Sn, Value: args.Value}
  }
  kv.px.Start(instance, op)
  // wait for agreement
  currentOp := kv.WaitForAgreement(instance)
  if currentOp == nil { // paxos dead 
     reply.Err = common.ErrWrongGroup
     return nil
  }

  for currentOp.Sn != op.Sn {
     instance = kv.NextIndex()//kv.px.Max() + 1
     kv.px.Start(instance, op)
     currentOp = kv.WaitForAgreement(instance)
     if currentOp == nil {
        reply.Err = common.ErrWrongGroup
        return nil
     }
  }
  // need to wait for processing
  kv.mu.Unlock()
  for kv.processed < instance {
      time.Sleep(10 * time.Millisecond)
  }
  kv.mu.Lock()

  shard := key2shard(args.Key)
  processedReply, processedReplyExisted := kv.replymap[shard][args.Sn]
  if !processedReplyExisted {
     reply.Err = common.ErrWrongGroup
  } else {
     reply.PreviousValue = processedReply.PreviousValue
     reply.Err = processedReply.Err
  }
  return nil
}

//
// Ask the shardmaster if there's a new configuration;
// if so, re-configure.
//
func (kv *ShardKV) tick() {
  kv.mu.Lock()
  defer kv.mu.Unlock()
  var c common.Config
  c = kv.sm.Query(kv.config.Num + 1)
  //kv.CheckRestart()
  if c.Num > kv.config.Num {
     if c.Num == kv.proposed {
        return
     }
     DPrintf("me %d gid %d Update config now %d to %d", kv.me, kv.gid, kv.config.Num, c.Num)
     var instance int
     instance = kv.NextIndex()//kv.px.Max() + 1
     var op common.ShardKVOp
     op = common.ShardKVOp{Cmd:"Reconfig", ConfigNum: c.Num, Shards: c.Shards, Groups: c.Groups}
     kv.px.Start(instance, op)
     // wait for agreement
     currentOp := kv.WaitForAgreement(instance)
     if currentOp == nil {
        DPrintf("me %d gid %d tick killed", kv.me, kv.gid)
        return
     }
     for !(currentOp.Cmd == "Reconfig" && currentOp.ConfigNum == c.Num) {
         DPrintf("me %d gid %d Proposing NEW CONFIG %d", kv.me, kv.gid, c.Num)
         instance = kv.NextIndex()//kv.px.Max() + 1
         kv.px.Start(instance, op)
         currentOp = kv.WaitForAgreement(instance)
         if currentOp == nil {
            DPrintf("me %d gid %d tick 2 killed", kv.me, kv.gid)
            return
         }
     }
     DPrintf("me %d gid %d NEW CONFIG %d agreement reached JOB %d", kv.me, kv.gid, c.Num, instance)
     kv.proposed = c.Num
  }
}

// return Op decided on that instance
func (kv *ShardKV) WaitForAgreement(instance int) *common.ShardKVOp {
  to := 10 * time.Millisecond
  for !kv.dead {
    decided, v := kv.px.Status(instance)
    if decided {
       currentOp, ok := v.(common.ShardKVOp)
       if !ok {
          log.Printf("Type conversion failed")
          return nil
       }
       return &currentOp
    }
    time.Sleep(to)
    if to < 10 * time.Second {
      to *= 2
    }
  }
  return nil
}

// tell the server to shut itself down.
func (kv *ShardKV) kill() {
  kv.dead = true
  kv.l.Close()
  kv.px.Kill()
  kv.kvReqStore.Kill()
}

func (kv *ShardKV) StartBackground() {
  if kv.restart {
    log.Printf("*****")
    log.Printf("%d BACKGROUND THREAD STARTED  processed %d kv.px.Max() %v ", kv.uid, kv.processed, kv.px.Max())
    log.Printf("*****")
  }

  for !kv.dead {
    job := 1 + kv.processed
    to := 10 * time.Millisecond
    for !kv.dead {
      decided, v := kv.px.Status(job)
      if kv.restart {
//        log.Printf("%d querying job %d Decided %v", kv.uid, job, decided)
      }

      if decided {
  //       if !kv.restart { log.Printf("%d, %d", kv.uid, job)}
         op, ok := v.(common.ShardKVOp)
         if !ok {
            log.Printf("Type conversion failed")
            break
         }
         kv.mu.Lock()
         dup := false
         // filter dup
         if op.Cmd != "Reconfig" {
            _, hasPreviousReply := kv.replymap[key2shard(op.Key)][op.Sn]
            if hasPreviousReply {
               dup = true
            }
         } else if op.Cmd == "Reconfig" {
           if op.ConfigNum != kv.config.Num + 1 {
              DPrintf("me %d gid %d DUP RECONFIG configNum %d NOW %d", kv.me, kv.gid, op.ConfigNum, kv.config.Num)
              dup = true
           }
         }

         // end with filter dup
         if !dup {
            DPrintf("me %d gid %d Processing cmd %s sn %d configNum %d NOW %d", kv.me, kv.gid, op.Cmd, op.Sn, op.ConfigNum, kv.config.Num)
            reply := common.KVReply{Sn:op.Sn}
            if op.Cmd == "Put" || op.Cmd == "Get" || op.Cmd == "PutHash" {
                shard := key2shard(op.Key)
                if kv.checkServer(shard) {
                  if op.Cmd == "Put" {
                      kv.kvmap[shard][op.Key] = op.Value
                      kv.dirtyMap[shard][op.Key] = op.Value
                  } else if op.Cmd == "PutHash" {
                      previousValue := kv.kvmap[shard][op.Key]
                      h := common.Hash(previousValue + op.Value)
                      kv.kvmap[shard][op.Key] = strconv.Itoa(int(h))
                      kv.dirtyMap[shard] [op.Key] = strconv.Itoa(int(h))
                      reply.PreviousValue = previousValue
                  } else if op.Cmd == "Get" {
                      value := kv.kvmap[shard][op.Key]
                      reply.Value = value
                  }
                  reply.Err = common.OK
                  kv.replymap[shard][op.Sn] = &reply
                  kv.dirtyReqs[shard][op.Sn] = &reply
                }
            } // end if op.Cmd == "Put" || "Get" || "PutHash"
            if op.Cmd == "Reconfig" {
                kv.mu.Unlock()
                transferreply := common.TransferReply{}
                newConfig := common.Config{Num: op.ConfigNum, Shards: op.Shards, Groups: op.Groups}
                kv.requestShards(&newConfig, &transferreply)
                DPrintf("me %d gid %d DONE with requestshard for config %d", kv.me, kv.gid, op.ConfigNum)
                kv.mu.Lock()
                if kv.config.Num > 0 { kv.updateStates(&transferreply)}
                kv.config = &newConfig
            } // Reconfig
        } // end dup
        kv.processed = job
        kv.mu.Unlock()
        break
      } else {  // if not decided
        // try to propose nop
        if kv.px.Max() > job {
           if kv.IsOwner(job) {
             kv.px.Start(job, kv.px.NoOp)
           } else {
             kv.px.Revoke(job)
           }
           //kv.px.Start(job, Op{})
        }
      }
      time.Sleep(to)
      // Exponential backoff doesn't work quite well for TestLimp, coz the group becomes dead too quickly
      /**
      if to < 10 * time.Second {
        to *= 2
      }
      **/
     }
  }
}

//
// returns the next instance to suggest
//
func (kv *ShardKV) NextIndex() (int) {
  index := kv.px.Max() + 1
  for {
    if kv.IsOwner(int(index)) {
      break
    }
    index++
  }
  return index
}

//
// are we the owner?
//
func (kv *ShardKV) IsOwner(instance int) (bool) {
  return kv.px.IsOwner(instance)
}

func (kv *ShardKV) StartStore() {
  for !kv.dead{
    kv.mu.Lock()
    // encode
    cfg := new(bytes.Buffer)
    enc := gob.NewEncoder(cfg)
    enc.Encode(kv.config)
    kv.kvReqStore.BatchStore(kv.dirtyMap, kv.dirtyReqs, kv.processed, cfg.Bytes())

    for i := kv.lastStore; i < kv.processed; i++{
      kv.px.Done(i)
    }

    kv.lastStore = kv.processed
    kv.dirtyMap = make(map[int]map[string]string)

    for i := 0; i < common.NShards; i++{
        kv.dirtyMap[i] = make(map[string]string)
    }

    kv.dirtyReqs = make(map[int]map[int64]*common.KVReply)
    for i := 0; i < common.NShards; i++{
      kv.dirtyReqs[i] = make(map[int64]*common.KVReply)
    }
    kv.mu.Unlock()
    time.Sleep(time.Millisecond * 1000)
  }
}

func (kv *ShardKV) LoadFromDB() {
  fmt.Println("Loading from DB")
  for s := range(kv.kvmap){
    kvmap, err := kv.kvReqStore.KvReadAll(s)
    if(err == "DbOk"){
      kv.kvmap[s] = kvmap
    }
  }

  for s := range(kv.replymap){
    replymap, err := kv.kvReqStore.ReqReadAll(s)
    if(err == "DbOk"){
      kv.replymap[s] = replymap
    }
  }
  processed, err := kv.kvReqStore.MDataGetValue()
  if(err == "DbOk"){
    kv.processed = processed
  }
  cfg, err := kv.kvReqStore.ConfigGetValue()
  if(err == "DbOk"){
    // decode
    var config common.Config
    buf := bytes.NewBuffer(cfg)
    dec := gob.NewDecoder(buf)
    dec.Decode(&config)
    kv.config = &config
  }
}

func(kv *ShardKV) HasDB() bool{
  if _, err := os.Stat("./db/s"+ strconv.Itoa(kv.uid) + "_" + strconv.Itoa(int(kv.gid)) +"_kvstore.db"); os.IsNotExist(err) {
    fmt.Printf("no such file or directory: %s", "s"+ strconv.Itoa(kv.uid) + "_" + strconv.Itoa(int(kv.gid)) +"_kvstore.db")
    return false
  }
  return true
}

// for the benchmarking package
func(kv *ShardKV) BenchKill() {
  kv.kill()
}
func(kv *ShardKV) GetRPCCount() int {
  return kv.rpcCount
}
func(kv *ShardKV) ResetRPCCount() {
  kv.rpcCount = 0
}

//
// Start a shardkv server.
// gid is the ID of the server's replica group.
// shardmasters[] contains the ports of the
//   servers that implement the shardmaster.
// servers[] contains the ports of the servers
//   in this replica group.
// Me is the index of this server in servers[].
//
func StartServer(gid int64, shardmasters []string,
                 servers []string, uid int, me int, restart bool) *ShardKV {
  gob.Register(common.ShardKVOp{})
  gob.Register(common.Config{})
  gob.Register(common.TransferDBArgs{})
  gob.Register(common.TransferDBReply{})
  gob.Register(common.TransferArgs{})
  gob.Register(common.TransferReply{})
  gob.Register(common.QueryPXMaxArgs{})
  gob.Register(common.QueryPXMaxReply{})
  gob.Register(common.QueryKVProcessedArgs{})
  gob.Register(common.QueryKVProcessedReply{})
  gob.Register(common.MoveMaxArgs{})
  gob.Register(common.MoveMaxReply{})
  gob.Register(common.KVReply{})
  gob.Register(common.GetReply{})
  gob.Register(common.PutReply{})

  kv := new(ShardKV)
  kv.me = me
  kv.gid = gid
  kv.config = &common.Config{Num:0}
  kv.processed = -1
  kv.proposed = 0
  kv.uid = uid
  kv.servers = servers
  kv.restart = restart
  kv.deaf = false
  kv.rpcCount = 0
  for i := 0; i < common.NShards; i++ {
      kv.kvmap[i] = make(map[string]string)
  }
  for i := 0; i < common.NShards; i++ {
      kv.replymap[i] = make(map[int64]*common.KVReply)
  }
  kv.dirtyMap = make(map[int]map[string]string)
  for i := 0; i < common.NShards; i++{
      kv.dirtyMap[i] = make(map[string]string)
  }

  kv.dirtyReqs = make(map[int]map[int64]*common.KVReply)
  for i := 0; i < common.NShards; i++{
    kv.dirtyReqs[i] = make(map[int64]*common.KVReply)
  }

  kv.sm = shardmaster.MakeClerk(shardmasters)

  hasDB := kv.HasDB()
  if hasDB {
    //establish the DB
      log.Printf("%d Restarting without DISK loss", kv.uid)
    store, err := persistence.MakeKvReqStore("s"+ strconv.Itoa(kv.uid) + "_" + strconv.Itoa(int(kv.gid)), false)
    if( err == "DbOk"){
      kv.kvReqStore = store
    }
    kv.LoadFromDB()
  } else {
    if restart {
      // disk-loss recovery
      log.Printf("%d Restarting after DISK loss", kv.uid)
      // query each peer for their px.Max
			m := make([]int, len(kv.servers))
      kv.getPXMax(m)
      // take Max([]px.Max)
      hiMax := -1
      for _, v := range m {
        if v > hiMax {
          hiMax = v
        }
      }
      // send moveMax RPC to all peers
      kv.moveMax(hiMax+1)
      // query each peer for their kv.processed
      p := make([]int, len(kv.servers))
      for {
        kv.getKVProcessed(p)
        pass := true
		    for i, v := range p {
		      if v < hiMax && i != kv.me {
		        pass = false
		      }
		    }
        // when kv.processed > Max for all peers
        if pass {
          // initiate disk transfer
          break
        }
      }
      
      r := &common.TransferDBReply{}
      kv.getDB(r)
      kv.processed = r.Processed
      for mapkey, mapvalue := range r.KVMap {
          kv.kvmap[mapkey] = mapvalue
      }
      for mapkey, mapvalue := range r.ReplyMap {
          kv.replymap[mapkey] = mapvalue
      }
      kv.config = r.Config
      store, err := persistence.MakeKvReqStore("s"+ strconv.Itoa(kv.uid) + "_" + strconv.Itoa(int(kv.gid)), true)
      if( err == "DbOk"){
        kv.kvReqStore = store
      }
      cfg := new(bytes.Buffer)
      enc := gob.NewEncoder(cfg)
      enc.Encode(r.Config)
      kv.kvReqStore.BatchStore(r.KVMap, r.ReplyMap, r.Processed, cfg.Bytes())
    } else {
      // normal start
      log.Printf("%d Normal start", kv.uid)
      store, err := persistence.MakeKvReqStore("s"+ strconv.Itoa(kv.uid) + "_" + strconv.Itoa(int(kv.gid)), true)
      if( err == "DbOk"){
        kv.kvReqStore = store
      }
    }
  }

  // Your initialization code here.
  // Don't call Join().

  rpcs := rpc.NewServer()
  rpcs.Register(kv)

  kv.px = mencius.Make(common.ShardKVPaxos, servers, me, rpcs, kv.uid, true, restart)
  kv.lastTickMax = -1
  kv.lastTickProcessed = -1
  ticker := time.NewTicker(time.Second * 10)
  go func() {
    for _ = range ticker.C {
      if kv.dead {
        ticker.Stop()
        return
      }
      if kv.lastTickMax == kv.px.Max() && kv.lastTickProcessed == kv.processed {
        kv.upToDate = true
        //fmt.Printf("S%+v up to date\n",kv.me)
      } else {
        kv.upToDate = false
        kv.lastTickMax = kv.px.Max()
        kv.lastTickProcessed = kv.processed
      }
    }
  }()

  //start bg thread
  go kv.StartBackground()
  go kv.StartStore()

  os.Remove(servers[me])
  l, e := net.Listen("unix", servers[me]);
  if e != nil {
    log.Fatal("listen error: ", e);
  }
  kv.l = l

  // please do not change any of the following code,
  // or do anything to subvert it.

  go func() {
    for kv.dead == false {
      conn, err := kv.l.Accept()
      if err == nil && kv.dead == false && kv.deaf == false {
        if kv.unreliable && (rand.Int63() % 1000) < 100 {
          // discard the request.
          conn.Close()
        } else if kv.unreliable && (rand.Int63() % 1000) < 200 {
          // process the request but force discard of reply.
          c1 := conn.(*net.UnixConn)
          f, _ := c1.File()
          err := syscall.Shutdown(int(f.Fd()), syscall.SHUT_WR)
          if err != nil {
            fmt.Printf("shutdown: %v\n", err)
          }
          kv.rpcCount++
          go rpcs.ServeConn(conn)
        } else {
          kv.rpcCount++
          go rpcs.ServeConn(conn)
        }
      } else if err == nil {
        conn.Close()
      }
      if err != nil && kv.dead == false {
        fmt.Printf("ShardKV(%v) accept: %v\n", me, err.Error())
        kv.kill()
      }
    }
  }()

  go func() {
    for kv.dead == false {
      kv.tick()
      time.Sleep(250 * time.Millisecond)
    }
  }()

  return kv
}
