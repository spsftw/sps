package shardmaster

import "net"
import "fmt"
import "net/rpc"
import "log"
import "mencius"
import "sync"
import "os"
import "syscall"
import "encoding/gob"
import "math/rand"
import "time"
import "sort"
import "common"

//
// Lab4A using Mencius.

const Debug=0

func DPrintf(format string, a ...interface{}) (n int, err error) {
  if Debug > 0 {
    log.Printf(format, a...)
  }
  return
}

type ShardMaster struct {
  mu sync.Mutex
  l net.Listener
  me int
  dead bool // for testing
  unreliable bool // for testing
  px *mencius.Paxos

  configs []common.Config // indexed by config num
  processed int // processedInstance
  availableGIDs int64slice
  sn int
  replymap map[int64] bool   // a map which holds all requests number that it has seen. Probably need to optimize.
}

func (sm *ShardMaster) Join(args *common.JoinArgs, reply *common.JoinReply) error {
  DPrintf("%d Join GID %d ", sm.me, args.GID)
  sm.mu.Lock()
  defer sm.mu.Unlock()
  var instance int
  instance = sm.NextIndex()
  var op common.ShardMasterOp
  op = common.ShardMasterOp{Cmd:"Join", Sn: args.Sn, GID: args.GID, KVArgs: &common.KVArgs{JoinArgs:args}}
  sm.px.Start(instance, op)
  currentOp := sm.WaitForAgreement(instance)
  for !sm.dead && !(currentOp.Sn == op.Sn){
      instance = sm.NextIndex()
      sm.px.Start(instance, op)
      currentOp = sm.WaitForAgreement(instance)
  }
  // agreement reached
  return nil
}
func (sm *ShardMaster) Leave(args *common.LeaveArgs, reply *common.LeaveReply) error {
  DPrintf("%d Leave GID %d Sn: %d", sm.me, args.GID, args.Sn)
  sm.mu.Lock()
  defer sm.mu.Unlock()
  var instance int
  instance = sm.NextIndex()
  var op common.ShardMasterOp
  op = common.ShardMasterOp{Cmd:"Leave", Sn: args.Sn, GID: args.GID, KVArgs: &common.KVArgs{LeaveArgs:args}}
  sm.px.Start(instance, op)
  currentOp := sm.WaitForAgreement(instance)
  for !sm.dead && !(currentOp.Sn == op.Sn){
      instance = sm.NextIndex()
      sm.px.Start(instance, op)
      currentOp = sm.WaitForAgreement(instance)
  }
  // agreement reached
  return nil
}

func (sm *ShardMaster) Move(args *common.MoveArgs, reply *common.MoveReply) error {
  DPrintf("%d Move GID %d SHARD %d Sn: %d", sm.me, args.GID, args.Shard, args.Sn)
  sm.mu.Lock()
  defer sm.mu.Unlock()
  var instance int
  instance = sm.NextIndex()
  var op common.ShardMasterOp
  op = common.ShardMasterOp{Cmd:"Move", Sn: args.Sn, GID: args.GID, KVArgs: &common.KVArgs{MoveArgs:args}}
  sm.px.Start(instance, op)
  currentOp := sm.WaitForAgreement(instance)
  for !sm.dead && !(currentOp.Sn == op.Sn){
      instance = sm.NextIndex()
      sm.px.Start(instance, op)
      currentOp = sm.WaitForAgreement(instance)
  }
  // agreement reached
  return nil
}

func (sm *ShardMaster) Query(args *common.QueryArgs, reply *common.QueryReply) error {

  DPrintf("%d Query Sn: %d", sm.me, args.Sn)
  sm.mu.Lock()
  defer sm.mu.Unlock()
  var instance int
  instance = sm.NextIndex()
  var op common.ShardMasterOp
  op = common.ShardMasterOp{Cmd:"Query", Sn: args.Sn, KVArgs: &common.KVArgs{QueryArgs:args}}
  sm.px.Start(instance, op)
  currentOp := sm.WaitForAgreement(instance)
  for !sm.dead && !(currentOp.Sn == op.Sn){
      instance = sm.NextIndex()
      sm.px.Start(instance, op)
      currentOp = sm.WaitForAgreement(instance)
  }
  // agreement reached
  sm.mu.Unlock()
  for !sm.dead && sm.processed < instance {
      time.Sleep(10 * time.Millisecond)
  }
  sm.mu.Lock()
  if args.Num == -1 || args.Num >= len(sm.configs) {
     reply.Config = sm.configs[len(sm.configs)-1]
  } else {
     reply.Config = sm.configs[args.Num]
  }
  return nil
}

// return Op decided on that instance
func (sm *ShardMaster) WaitForAgreement(instance int) *common.ShardMasterOp {
  to := 10 * time.Millisecond
  for !sm.dead {
    decided, v := sm.px.Status(instance)
    if decided {
       currentOp, ok := v.(common.ShardMasterOp)
       if !ok {
          log.Printf("Type conversion failed")
          return nil
       }
       return &currentOp
    }
    time.Sleep(to)
    if to < 10 * time.Second {
      to *= 2
    }
  }
  return nil
}

func (sm *ShardMaster) ApplyJoin(args *common.JoinArgs) {
  DPrintf("%d Processing JOIN Sn: %d", sm.me, args.Sn)
  currentConfig := sm.configs[len(sm.configs)-1]
  dup := false // whether GID already exists
  for _, g := range sm.availableGIDs {
    if g == args.GID {
     dup = true
    }
  }
  if dup {
     return
  }

  var shards [common.NShards]int64
  groups := make (map [int64] []string)
  if len(sm.availableGIDs) > 0 {
    // count each gid owns how many shards
    shardcountmap := make (map[int64] int)
    for _, gid := range currentConfig.Shards {
       _, existed := shardcountmap[gid]
       if !existed {
          shardcountmap[gid] = 1
       } else {
          shardcountmap[gid] += 1
       }
    }
    var maxgid int64
    maxcount := 0
    for _, gid := range sm.availableGIDs {
       shardcount := shardcountmap[gid]
       if shardcount > maxcount {
          maxgid = gid
          maxcount = shardcount
       }
    }
    newcount := maxcount/2
    for i, gid := range currentConfig.Shards {
       if gid == maxgid && newcount > 0 {
          shards[i] = args.GID
          newcount--
       } else {
          shards[i] = currentConfig.Shards[i]
       }
    }

    for mapkey,mapvalue := range currentConfig.Groups {
       groups[mapkey] = mapvalue
    }
  } else {
    for i, _ := range currentConfig.Shards {
        shards[i] = args.GID
    }
  }
  groups[args.GID] = args.Servers
  sm.availableGIDs = append(sm.availableGIDs,args.GID)
  sort.Sort(sm.availableGIDs)

  newConfig := common.Config{len(sm.configs), shards, groups}
  sm.configs = append(sm.configs, newConfig)

  DPrintf("%d Join new shards %s", sm.me, shards)
}

func (sm *ShardMaster) ApplyLeave(args *common.LeaveArgs) {
  DPrintf("%d Processing LEAVE GID %d", sm.me, args.GID)
  currentConfig := sm.configs[len(sm.configs)-1]
  var newavailableGIDs int64slice // need to confirm
  existed := false // whether GID already exists
  for _, g := range sm.availableGIDs {
     if g == args.GID {
       existed = true
     } else {
       newavailableGIDs = append(newavailableGIDs,g)
     }
  }
  // newavailableGIDs should be sorted at this point
  if !existed {
     return
  }

  sm.availableGIDs = newavailableGIDs
  DPrintf("LEAVE availableGIDs %s",sm.availableGIDs)
  groups := make (map [int64] []string)
  var shards [common.NShards]int64
  // log.Printf("Leave shards %s", shards)
  // count how many shards each gid owns
  shardcountmap := make (map[int64] int)
  for _, gid := range currentConfig.Shards {
     _, existed := shardcountmap[gid]
     if !existed {
        shardcountmap[gid] = 1
     } else {
        shardcountmap[gid] += 1
     }
  }
  var leastgid int64
  leastcount := common.NShards
  for _, gid := range sm.availableGIDs {
     if gid != args.GID {
       shardcount := shardcountmap[gid]
       if shardcount < leastcount {
          leastgid = gid
          leastcount = shardcount
       }
     }
  }
  DPrintf("%d LEAVE leastgid %d leastcount %d", sm.me, leastgid, leastcount)
  for i, gid := range currentConfig.Shards {
     if gid == args.GID {
        shards[i] = leastgid
     } else {
        shards[i] = currentConfig.Shards[i]
     }
  }

  for mapkey,mapvalue := range currentConfig.Groups {
     if mapkey != args.GID {
        groups[mapkey] = mapvalue
     }
  }
  newConfig := common.Config{len(sm.configs), shards, groups}
  sm.configs = append(sm.configs, newConfig)
  DPrintf("%d Leave new shards %s", sm.me, shards)
}

func (sm *ShardMaster) ApplyMove(args *common.MoveArgs) {
  DPrintf("%d Processing MOVE", sm.me)
  currentConfig := sm.configs[len(sm.configs)-1]
  var shards [common.NShards]int64
  for i:=0; i < common.NShards; i++ {
    if i == args.Shard {
      shards[i] = args.GID
    } else {
      shards[i] = currentConfig.Shards[i]
    }
  }
  groups := make(map[int64][]string)
  for mapkey,mapvalue := range currentConfig.Groups {
    groups[mapkey] = mapvalue
  }
  newConfig := common.Config{len(sm.configs), shards, groups}
  sm.configs = append(sm.configs, newConfig)
}

type int64slice []int64
func (a int64slice) Len() int { return len(a) }
func (a int64slice) Swap(i, j int) { a[i], a[j] = a[j], a[i] }
func (a int64slice) Less(i, j int) bool { return a[i] < a[j] }


func (sm *ShardMaster) StartBackground() {
  for !sm.dead {
    job := 1 + sm.processed
    to := 10 * time.Millisecond
    for !sm.dead {
      decided, v := sm.px.Status(job)
      if decided {
         // DPrintf("%d Processing Job %d", sm.me, job)
         op, ok := v.(common.ShardMasterOp)
         if !ok {
            log.Printf("Type conversion failed")
            break
         }
         sm.mu.Lock()
         // filter requests that have already been processed before
         _, hasPreviousReply := sm.replymap[op.Sn]
         if !hasPreviousReply {
           if op.Cmd == "Join" {
              args := op.KVArgs.JoinArgs
              sm.ApplyJoin(args)
           } else if op.Cmd == "Leave" {
              args := op.KVArgs.LeaveArgs
              sm.ApplyLeave(args)
           } else if op.Cmd == "Move" {
              args := op.KVArgs.MoveArgs
              sm.ApplyMove(args)
           } else if op.Cmd == "Query" {
              // nothing to do
              // DPrintf("%d Processing QUERY Job %d ConfigNum %d", sm.me, job, args.Num)
           }
           sm.replymap[op.Sn] = true
         }
         sm.processed = job
         sm.px.Done(job)
         sm.mu.Unlock()
         //DPrintf("%d background releases lock", sm.me)
         break
      } else {
        // try to propose nop
        if sm.px.Max() > job {
           if sm.IsOwner(job) {
             sm.px.Start(job, common.ShardMasterOp{})
           } else {
             sm.px.Revoke(job)
           }
           //sm.px.Start(job, Op{})
        }
      }
      time.Sleep(to)
      if to < 10 * time.Second {
        to *= 2
      }
     }
  }
}


// please don't change this function.
func (sm *ShardMaster) Kill() {
  sm.dead = true
  sm.l.Close()
  sm.px.Kill()
}

//
// returns the next instance to suggest
//
func (sm *ShardMaster) NextIndex() (int) {
  index := sm.px.Max() + 1
  for {
    if sm.IsOwner(int(index)) {
      break
    }
    index++
  }
  return index
}

//
// are we the owner?
//
func (sm *ShardMaster) IsOwner(instance int) (bool) {
  return sm.px.IsOwner(instance)
}

//
// revoke set of instances for the specified owner 
//
/*func (sm *ShardMaster) RevokeSet(instance int) {
  // optimization 3: p revokes q for large block of
  // instances when suspecting server q has failed
  set := sm.px.NilInstances(sm.processed, instance, sm.beta)
  for _, v := range set {
    sm.px.Revoke(v)
  }
}*/

//
// servers[] contains the ports of the set of
// servers that will cooperate via Paxos to
// form the fault-tolerant shardmaster service.
// me is the index of the current server in servers[].
// 
func StartServer(servers []string, me int) *ShardMaster {
  gob.Register(common.ShardMasterOp{})

  sm := new(ShardMaster)
  sm.me = me

  sm.configs = make([]common.Config, 1)
  sm.configs[0].Groups = map[int64][]string{}
  sm.processed = -1
  sm.availableGIDs = int64slice{}
  sm.replymap = make(map[int64] bool)
  rpcs := rpc.NewServer()
  rpcs.Register(sm)

  //because shardmaster does not need to be persistent, the uid passed to mencius is false
  sm.px = mencius.Make(common.ShardMasterPaxos, servers, me, rpcs, 0, false, false /**shardmaster never fails **/)
  go sm.StartBackground()
  os.Remove(servers[me])
  l, e := net.Listen("unix", servers[me]);
  if e != nil {
    log.Fatal("listen error: ", e);
  }
  sm.l = l

  // please do not change any of the following code,
  // or do anything to subvert it.

  go func() {
    for sm.dead == false {
      conn, err := sm.l.Accept()
      if err == nil && sm.dead == false {
        if sm.unreliable && (rand.Int63() % 1000) < 100 {
          // discard the request.
          conn.Close()
        } else if sm.unreliable && (rand.Int63() % 1000) < 200 {
          // process the request but force discard of reply.
          c1 := conn.(*net.UnixConn)
          f, _ := c1.File()
          err := syscall.Shutdown(int(f.Fd()), syscall.SHUT_WR)
          if err != nil {
            fmt.Printf("shutdown: %v\n", err)
          }
          go rpcs.ServeConn(conn)
        } else {
          go rpcs.ServeConn(conn)
        }
      } else if err == nil {
        conn.Close()
      }
      if err != nil && sm.dead == false {
        fmt.Printf("ShardMaster(%v) accept: %v\n", me, err.Error())
        sm.Kill()
      }
    }
  }()

  return sm
}
