package shardmaster

// 
// Shardmaster clerk.
// Please don't change this file.
//
// These functions have been optimized to
// uniformly distribute load on Mencius servers.
//

import "net/rpc"
import "time"
import "fmt"
import "crypto/rand"
import "math/big"
import "sync"
import "common"


type Clerk struct {
  mu sync.Mutex
  servers []string // shardmaster replicas
  priority_index int // split workload
  Id int64 // unique id for each clerk
  Sn int64 // increasing sequence number for each request
}

func MakeClerk(servers []string) *Clerk {
  ck := new(Clerk)
  ck.servers = servers
  ck.priority_index = 0
  ck.Id = nrand()
  ck.Sn = ck.Id + 1
  return ck
}

func nrand() int64 {
  max := big.NewInt(int64(1) << 62)
  bigx, _ := rand.Int(rand.Reader, max)
  x := bigx.Int64()
  return x
}
//
// call() sends an RPC to the rpcname handler on server srv
// with arguments args, waits for the reply, and leaves the
// reply in reply. the reply argument should be a pointer
// to a reply structure.
//
// the return value is true if the server responded, and false
// if call() was not able to contact the server. in particular,
// the reply's contents are only valid if call() returned true.
//
// you should assume that call() will time out and return an
// error after a while if it doesn't get a reply from the server.
//
// please use call() to send all RPCs, in client.go and server.go.
// please don't change this function.
//
func call(srv string, rpcname string,
          args interface{}, reply interface{}) bool {
  c, errx := rpc.Dial("unix", srv)
  if errx != nil {
    return false
  }
  defer c.Close()

  err := c.Call(rpcname, args, reply)
  if err == nil {
    return true
  }

  fmt.Println(err)
  return false
}

func (ck *Clerk) Query(num int) common.Config {
  ck.mu.Lock()
  defer ck.mu.Unlock()
  iter := 0
  sn := ck.Sn + 1
  defer func() {ck.Sn = sn }()
  for {
    // try each known server.
    ind := ck.priority_index%len(ck.servers)
    ck.priority_index++
    srv := ck.servers[ind]
    args := &common.QueryArgs{ClientId: ck.Id, Sn: sn}
    args.Num = num
    var reply common.QueryReply
    ok := call(srv, "ShardMaster.Query", args, &reply)
    if ok {
      return reply.Config
    }
    iter++
    if iter%len(ck.servers) == 0 {
      time.Sleep(100 * time.Millisecond)
    }
  }
  return common.Config{}
}

func (ck *Clerk) Join(gid int64, servers []string) {
  ck.mu.Lock()
  defer ck.mu.Unlock()

  iter := 0
  sn := ck.Sn + 1
  defer func() {ck.Sn = sn }()
  for {
    // try each known server.
    ind := ck.priority_index%len(ck.servers)
    ck.priority_index++
    srv := ck.servers[ind]
    args := &common.JoinArgs{ClientId: ck.Id, Sn: sn}
    args.GID = gid
    args.Servers = servers
    var reply common.JoinReply
    ok := call(srv, "ShardMaster.Join", args, &reply)
    if ok {
      return
    }
    iter++
    if iter%len(ck.servers) == 0 {
      time.Sleep(100 * time.Millisecond)
    }
  }
}

func (ck *Clerk) Leave(gid int64) {
  ck.mu.Lock()
  defer ck.mu.Unlock()

  iter := 0
  sn := ck.Sn + 1
  defer func() {ck.Sn = sn }()
  for {
    // try each known server.
    ind := ck.priority_index%len(ck.servers)
    srv := ck.servers[ind]
    args := &common.LeaveArgs{ClientId: ck.Id, Sn: sn}
    args.GID = gid
    var reply common.LeaveReply
    ok := call(srv, "ShardMaster.Leave", args, &reply)
    if ok {
      return
    }
    ck.priority_index++
    iter++
    if iter%len(ck.servers) == 0 {
      time.Sleep(100 * time.Millisecond)
    }
  }
}

func (ck *Clerk) Move(shard int, gid int64) {
  ck.mu.Lock()
  defer ck.mu.Unlock()

  iter := 0
  sn := ck.Sn + 1
  defer func() {ck.Sn = sn }()
  for {
    // try each known server.
    ind := ck.priority_index%len(ck.servers)
    srv := ck.servers[ind]
    args := &common.MoveArgs{ClientId: ck.Id, Sn: sn}
    args.Shard = shard
    args.GID = gid
    var reply common.LeaveReply
    ok := call(srv, "ShardMaster.Move", args, &reply)
    if ok {
      return
    }
    ck.priority_index++
    iter++
    if iter%len(ck.servers) == 0 {
      time.Sleep(100 * time.Millisecond)
    }
  }
}
