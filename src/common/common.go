package common
import "hash/fnv"
import "crypto/rand"
import "math/big"

//
// Master shard server: assigns shards to replication groups.
//
// RPC interface:
// Join(gid, servers) -- replica group gid is joining, give it some shards.
// Leave(gid) -- replica group gid is retiring, hand off all its shards.
// Move(shard, gid) -- hand off one shard from current owner to gid.
// Query(num) -> fetch Config # num, or latest config if num==-1.
//
// A Config (configuration) describes a set of replica groups, and the
// replica group responsible for each shard. Configs are numbered. Config
// #0 is the initial configuration, with no groups and all shards
// assigned to group 0 (the invalid group).
//
// A GID is a replica group ID. GIDs must be uniqe and > 0.
// Once a GID joins, and leaves, it should never join again.
//
// Please don't change this file.
//

const NShards = 10

const (
   ShardKVPaxos = "ShardKVPaxos"
   ShardMasterPaxos = "ShardMasterPaxos"
   StringPaxos = "StringPaxos" // interface{} is string or integer
   UnknownPaxos = "UnknownPaxos"
)

type Config struct {
  Num int // config number
  Shards [NShards]int64 // gid
  Groups map[int64][]string // gid -> servers[]
}

type JoinArgs struct {
  GID int64       // unique replica group ID
  Servers []string // group server ports
  ClientId int64
  Sn int64
}

type JoinReply struct {
}

type LeaveArgs struct {
  GID int64
  ClientId int64
  Sn int64
}

type LeaveReply struct {
}

type MoveArgs struct {
  Shard int
  GID int64
  ClientId int64
  Sn int64
}

type MoveReply struct {
}

type QueryArgs struct {
  Num int // desired config number
  ClientId int64
  Sn int64
}

type QueryReply struct {
  Config Config
}


//
// Sharded key/value server.
// Lots of replica groups, each running op-at-a-time paxos.
// Shardmaster decides which group serves each shard.
// Shardmaster may change shard assignment from time to time.
//
// You will have to modify these definitions.
//

const (
  OK = "OK"
  ErrNoKey = "ErrNoKey"
  ErrWrongGroup = "ErrWrongGroup"
  ErrWait = "ErrWait"
)

type Err string

type PutArgs struct {
  // You'll have to add definitions here.
  Key string
  Value string
  DoHash bool  // For PutHash
  // You'll have to add definitions here.
  // Field names must start with capital letters,
  // otherwise RPC will break.
  ClientId int64
  Sn int64 // request number
}

type PutReply struct {
  Err Err
  PreviousValue string   // For PutHash
}

type GetArgs struct {
  Key string
  // You'll have to add definitions here.
  ClientId int64
  Sn int64 // request number
}

type GetReply struct {
  Err Err
  Value string
}

type TransferArgs struct {
  Shards [NShards]int
  ConfigNum int
}

type TransferReply struct {
  Err Err
  KVMap map[int] map[string]string
  ReplyMap map[int] map[int64]*KVReply
}

type TransferPaxosDBArgs struct {
}

type TransferPaxosDBReply struct {
  Err Err
  Instances map[int]MenciusState
  NeedToSkip map[int]map[int]bool
  Done int
  Dones []int
  Min int
  Max int
}

type TransferDBArgs struct {
}

type TransferDBReply struct {
  Err Err
  KVMap map[int] map[string]string
  ReplyMap map[int] map[int64]*KVReply
  Processed int
  Config *Config
}

type QueryPXMaxArgs struct {
}

type QueryPXMaxReply struct {
  Err Err
  Max int
}

type QueryKVProcessedArgs struct {
}

type QueryKVProcessedReply struct {
  Err Err
  Processed int
}

type MoveMaxArgs struct {
  MaxGlobal int
}

type MoveMaxReply struct {
  Err Err
}

type KVReply struct {
  Cmd string // Get, Put, PutHash
  Sn int64 // Serial number of request
  Err Err
  Value string // For GetReply
  PreviousValue string // For PutReply
}

func Hash(s string) uint32 {
  h := fnv.New32a()
  h.Write([]byte(s))
  return h.Sum32()
}


//
// Data structure used by the persistence package
//

const (
  DbOk = "DbOk"
  ErrDbDead = "ErrDbDead"
  ErrDbNoConnection = "ErrDbNoConnection"
  ErrDbCreate = "ErrDbCreate"
  ErrDbExec = "ErrDbExec"
  ErrDbNoKey = "ErrDbNoKey"
  ErrDbQuery = "ErrDbQuery"
  ErrDbBeginTx = "ErrDbBeginTx"
  ErrDbTxCommit = "ErrDbTxCommit"
  ErrDbGetCount = "ErrDbGetCount"
)

type DbError string

type ShardMasterOp struct {
  Cmd string
  GID int64
  KVArgs *KVArgs
  Sn int64
}
type KVArgs struct {
  JoinArgs *JoinArgs
  LeaveArgs *LeaveArgs
  MoveArgs *MoveArgs
  QueryArgs *QueryArgs
}

type ShardKVOp struct {
  Cmd string // Get, Put, PutHash, Reconfig
  Key string
  ClientId int64
  Sn int64 // request number
  Value string // For Put
  // Optional, for cmd Reconfig
  ConfigNum int
  Shards [NShards]int64 // gid
  Groups map[int64][]string // gid -> servers[]
}

type MenciusState struct {
  DecidedValue interface{}
  N_p int64
  N_a int64
  V_a interface{}
  Init bool // for setting n_a to -1
}

func Nrand() int64 {
  max := big.NewInt(int64(1) << 62)
  bigx, _ := rand.Int(rand.Reader, max)
  x := bigx.Int64()
  return x
}


